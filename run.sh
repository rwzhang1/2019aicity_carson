#!/bin/bash

python ../embed_triplet_net.py \
    --store_pth ..log/resnet50/emb_25000_800.pkl \
    --model ../log/resnet50/ep25000_trip_soft.pkl \
    --data_pth ../aic19-track2-reid/image_test/ \
    --data_list VehicleID/train_test_split_v1/test_list_800.txt \
    --model_name res50 \
    --num_class 13164 \
    --img_size 256

python ../embed_triplet_net.py \
    --store_pth ../res/soft_trip_res50_VehicleID/emb_10000_2400_v2.pkl \
    --model ../res/soft_trip_res50_VehicleID/10000model_trip_soft_res50_v2.pkl \
    --data_pth VehicleID/image/ \
    --data_list VehicleID/train_test_split_v1/test_list_2400.txt \
    --model_name res50 \
    --num_class 13164 \
    --img_size 256
    
python ../embed_triplet_net.py \
    --store_pth ../res/soft_trip_res50_VehicleID/emb_10000_1600_v2.pkl \
    --model ../res/soft_trip_res50_VehicleID/10000model_trip_soft_res50_v2.pkl \
    --data_pth VehicleID/image/ \
    --data_list VehicleID/train_test_split_v1/test_list_1600.txt \
    --model_name res50 \
    --num_class 13164 \
    --img_size 256
    
    #--data_pth /home/CORP/ryann.bai/dataset/VehicleID/image/ \
    #--data_list /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt \
    #--data_pth /home/CORP/ryann.bai/dataset/VeRi-776/image_test/ \
    #--data_list /home/CORP/ryann.bai/dataset/VeRi-776/name_test.txt \

###run test_cmc.sh
#!/bin/bash
python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_40000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_40000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_40000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_35000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_35000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_35000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt
    
    
python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_30000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_30000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_30000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt
    
python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_25000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt


python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_25000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_25000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt
    
    
python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_20000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_20000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_20000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt    
    
    
python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_15000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt


python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_15000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_15000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt
    
    
python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_10000_800_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_800.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_10000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../cmc_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_10000_2400_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/cmc_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_2400.txt              
    
    
#run map
    #!/bin/bash
    
python ../mAP_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_15000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/map_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt

python ../mAP_vehicleID.py \
    --embeddings ../res/soft_trip_res50_VehicleID/emb_10000_1600_v2.pkl \
    --repeat 2 \
    --save ../res/soft_trip_res50_VehicleID/map_resnet50.txt \
    --list_file /home/CORP/ryann.bai/dataset/VehicleID/train_test_split_v1/test_list_1600.txt#!/bin/bash

#train
python train_triplet_net.py \
       --model res50 \
       --model_name model_trip_soft_res50_v2.pkl \
       --batch_p 18 \
       --learning_rate 3e-4 \
       --train_iterations 50000 \
       --decay_start_iteration 25000\
       --num_class 333 \
       #--resume ../res/soft_trip_res50_VehicleID/25000model_trip_soft.pkl
python train_joint_aic_veri_v3.py --model resnet50 --n_layer 50 --train_iterations 15000 --checkpoint_frequency 1512 --resume 'from_veri' --ngpu 3 --decay_start_iteration 2500
