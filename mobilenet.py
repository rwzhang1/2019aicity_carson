# -*- coding: utf-8 -*-
# @Author: rein9
# @Last Modified time: 2019-04-30 02:04:57

import torch
import torch.nn as nn
from torch.nn.modules.utils import _single, _pair, _triple
import math
import torch.nn.functional as F
import torchvision.transforms as transforms
__all__ = ['mobilenet']


def nearby_int(n):
    return int(round(n))


def init_model(model):
    for m in model.modules():
        if isinstance(m, nn.Conv2d):
            n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
            m.weight.data.normal_(0, math.sqrt(2. / n))
        elif isinstance(m, nn.BatchNorm2d):
            m.weight.data.fill_(1)
            m.bias.data.zero_()


def weight_decay_config(value=1e-4, log=True):
    def regularize_layer(m):
        non_depthwise_conv = isinstance(m, nn.Conv2d) \
            and m.groups != m.in_channels
        return isinstance(m, nn.Linear) or non_depthwise_conv

    return {'name': 'WeightDecay',
            'value': value,
            'log': log,
            'filter': {'parameter_name': lambda n: not n.endswith('bias'),
                       'module': regularize_layer}
            }


class DepthwiseSeparableFusedConv2d(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size,
                stride=1, padding=0):
        super(DepthwiseSeparableFusedConv2d, self).__init__()
        self.components = nn.Sequential(
            nn.Conv2d(in_channels, in_channels, kernel_size,
                            stride=stride, padding=padding, groups=in_channels),
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True),

            nn.Conv2d(in_channels, out_channels, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.components(x)

class DepthwiseSeparableFusedConv2d_dropbn(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size,
                 stride=1, padding=0):
        super(DepthwiseSeparableFusedConv2d_dropbn, self).__init__()
        self.components = nn.Sequential(
            nn.Conv2d(in_channels, in_channels, kernel_size,
                      stride=stride, padding=padding, groups=in_channels),
            #nn.BatchNorm2d(in_channels),
            #nn.ReLU(inplace=True),

            nn.Conv2d(in_channels, out_channels, 1, bias=False),
            nn.Dropout(0.001),
            nn.BatchNorm2d(out_channels, momentum = 0.9, eps = 0.01 ),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        return self.components(x)
        
class DepthwiseSeparableFusedConv2d_nobn(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size,
                 stride=1, padding=0):
        super(DepthwiseSeparableFusedConv2d_nobn, self).__init__()
        self.components = nn.Sequential(
            nn.Conv2d(in_channels, in_channels, kernel_size,
                      stride=stride, padding=padding, groups=in_channels),
            #nn.BatchNorm2d(in_channels),
            #nn.ReLU(inplace=True),

            nn.Conv2d(in_channels, out_channels, 1, bias=False),
            nn.BatchNorm2d(out_channels, momentum = 0.9, eps = 0.01 ),
            nn.ReLU(inplace=True),
        )

    def forward(self, x):
        return self.components(x)

class MobileNet(nn.Module):

    def __init__(self, num_classes=1000, dims = 128, input_size=224, width=1., with_top = True, shallow=False, regime=None):
        super(MobileNet, self).__init__()
        num_classes = num_classes or 1000
        width = width or 1.
        layers = [
            nn.Conv2d(3, nearby_int(width * 32), kernel_size=3, stride=2, padding=1, bias=False),
            nn.BatchNorm2d(nearby_int(width * 32)),# update on 04/08
            nn.ReLU(inplace=True),

            #removed on 04/04 for mobilenet
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 32), nearby_int(width * 64),
                kernel_size=3, padding=1),
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 64), nearby_int(width * 128),
                kernel_size=3, stride=2, padding=1),
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 128), nearby_int(width * 128),
                kernel_size=3, padding=1),
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 128), nearby_int(width * 256),
                kernel_size=3, stride=2, padding=1),
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 256), nearby_int(width * 256),
                kernel_size=3, padding=1),
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 256), nearby_int(width * 512),
                kernel_size=3, stride=2, padding=1)
        ]
        if not shallow:
            # 5x 512->512 DW-separable convolutions
            layers += [
                DepthwiseSeparableFusedConv2d_nobn(
                    nearby_int(width * 512), nearby_int(width * 512),
                    kernel_size=3, padding=1),
                DepthwiseSeparableFusedConv2d_nobn(
                    nearby_int(width * 512), nearby_int(width * 512),
                    kernel_size=3, padding=1),
                DepthwiseSeparableFusedConv2d_nobn(
                    nearby_int(width * 512), nearby_int(width * 512),
                    kernel_size=3, padding=1),
                DepthwiseSeparableFusedConv2d_nobn(
                    nearby_int(width * 512), nearby_int(width * 512),
                    kernel_size=3, padding=1),
                DepthwiseSeparableFusedConv2d_nobn(
                    nearby_int(width * 512), nearby_int(width * 512),
                    kernel_size=3, padding=1),
            ]
        layers += [
            DepthwiseSeparableFusedConv2d_nobn(
                nearby_int(width * 512), nearby_int(width * 1024),
                kernel_size=3, stride=2, padding=1),
            # Paper specifies stride-2, but unchanged size.
            # Assume its a typo and use stride-1 convolution
            # 03/26: need to remove last layer of normalization 
            # 04/11 change to conv2d_dropbn
            DepthwiseSeparableFusedConv2d_dropbn(
                nearby_int(width * 1024), nearby_int(width * 1024),
                kernel_size=3, stride=1, padding=1)
        ]
        self.features = nn.Sequential(*layers)
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        output_channel = nearby_int(width * 1024)
        # 04/11 change to conv2d_dropbn
        # self.dropout = nn.Dropout(0.001)
        self.with_top = with_top
        if with_top:
            self.classifier = nn.Linear(output_channel, num_classes)
        self.embed = nn.Linear(output_channel, dims)
        self.output_dim = output_channel
        self._initialize_weights()
        nn.init.orthogonal_(self.embed.weight, gain=1)
        nn.init.constant_(self.embed.bias, 0)
        
        if regime == 'small':
            scale_lr = 4
            self.regime = [
                {'epoch': 0, 'optimizer': 'SGD',
                 'momentum': 0.9, 'lr': scale_lr * 1e-1, 'regularizer': weight_decay_config(1e-4)},
                {'epoch': 30, 'lr': scale_lr * 1e-2},
                {'epoch': 60, 'lr': scale_lr * 1e-3},
                {'epoch': 80, 'lr': scale_lr * 1e-4}
            ]
            self.data_regime = [
                {'epoch': 0, 'input_size': 128, 'batch_size': 512},
                {'epoch': 80, 'input_size': 224, 'batch_size': 128},
            ]
            self.data_eval_regime = [
                {'epoch': 0, 'input_size': 128, 'batch_size': 1024},
                {'epoch': 80, 'input_size': 224, 'batch_size': 512},
            ]
        else:
            self.regime = [
                {'epoch': 0, 'optimizer': 'Adam', 'lr': 5e-4,
                 'momentum': 0.9, 'regularizer': weight_decay_config(1e-3)},
                {'epoch': 160, 'lr': 4e-4},
                {'epoch': 300, 'lr': 2e-4},
                {'epoch': 500, 'lr': 8e-5},
                {'epoch': 680, 'lr': 5e-5, 'regularizer': weight_decay_config(5e-5)},
                {'epoch': 880, 'lr': 2e-5},
                {'epoch': 1080, 'lr': 8e-6},
                {'epoch': 1280, 'lr': 5e-6},                
            ]

            
    def forward(self, x):
        # print('x shape before feature', x.shape)
        x = self.features(x)
        # print('x shape after feature', x.shape)
        x = self.avg_pool(x)
        # print('x shape after avgpool ', x.shape)
        x = x.view(x.size(0), -1)
        # print('x shape before emb', x.shape)
        embs = self.embed(x)
        #embs = F.normalize(embs, p=2, dim=1)
        if self.with_top:
            # 04/11 change to conv2d_dropbn
            # x = self.dropout(x)
            # print('x shape after dropout ', x.shape)
            fc = self.classifier(x)
            return fc, x, embs
        else:
            return x, embs
            

    def _initialize_weights(self):
        
        def truncated_normal_(tensor, mean=0, std=1):
            size = tensor.shape
            tmp = tensor.new_empty(size + (4,)).normal_()
            valid = (tmp < 2) & (tmp > -2)
            ind = valid.max(-1, keepdim=True)[1]
            tensor.data.copy_(tmp.gather(-1, ind).squeeze(-1))
            tensor.data.mul_(std).add_(mean)##
            
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                truncated_normal_(m.weight, std=0.09)
                #updates made on 04/09                
                #print('initialize conv2d with truncated_normal', m.weight.data)
                #n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                #m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                # update 04/09
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight, a=1)
                nn.init.constant_(m.bias, 0)
                # update 04/09
                #n = m.weight.size(1)
                #m.weight.data.normal_(0, 0.01)
                #m.bias.data.zero_()
                
def mobilenet(**config):
    """MobileNet model architecture from the `"MobileNets:
    Efficient Convolutional Neural Networks for Mobile Vision Applications"
    <https://arxiv.org/abs/1704.04861>`_ paper.

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
    """
    dataset = config.pop('dataset', 'imagenet')
    assert dataset == 'imagenet'
    return MobileNet(**config)

if __name__ == '__main__':
    model = MobileNet()
    from gen_aic_veri import TripletImage_Dataset
    from gen_aic_veri import Get_train_DataLoader
    import torch.optim as optim
    import matplotlib.pyplot as plt
    import cv2
    
    # test the hooks 
    veri_text = 'ReID_CNN/database/VeRi_train_info.txt'
    dataset = TripletImage_Dataset(veri_text, crop=True, flip=True, jitter=True, imagenet_normalize=True, image_per_class_in_batch=1)
    train_loader = Get_train_DataLoader(dataset, batch_size=2, num_workers=1)
    print('len', len(dataset), 'classes', dataset.classes)
    print('train n_batch', len(train_loader))
    n = 123
    im = dataset[n]['img']
    print(dataset[n]['class'])
    # if len(im.shape) > 3:
        # for i in range(im.shape[0]):
            # cv2.imshow('erased', im[i].permute(1,2,0).numpy())
            # cv2.waitKey(10000)
    # else:
        # cv2.imshow('erased', im.permute(1,2,0).numpy())
        # cv2.waitKey(10000)
    print('im shape: ', im.shape)
    
    model_ckpt = 'ReID_CNN/ckpt_mobile_veritrip_lastdrop/model_mobilenet_1500.ckpt'
    model = MobileNet(num_classes=dataset.n_id)
    activation = {}
    def get_activation(name):
        def hook(model, input, output):
            activation[name] = output.detach()
        return hook
    
    def normalize_output(img):
        img = img - img.min()
        img = img / img.max()
        return img
    
    '''
    temporarily create data_parallel to load state dict
    ngpu = 3
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
    with torch.cuda.device(0):
        model = model.cuda()
    if (device.type == 'cuda') and (ngpu > 1):
        model = nn.DataParallel(model, list(range(ngpu)))
    model_dict = torch.load(model_ckpt)
    print(model_dict['model_state_dict'].keys())
    model.load_state_dict(model_dict['model_state_dict'])
    '''
    # original saved file with DataParallel
    state_dict = torch.load(model_ckpt)
    # create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict['model_state_dict'].items():
        name = k[7:] # remove `module.`
        new_state_dict[name] = v
    # load params
    model.load_state_dict(new_state_dict)
        
    model.features[-1].register_forward_hook(get_activation('conv_last'))
    model.features[0].register_forward_hook(get_activation('conv'))
    # Plot some images
    pred_class, feature, embs = model(im)
    
    act = activation['conv_last'].squeeze()
    print('act shape:', act.shape)
    fig, axarr = plt.subplots(4,4)
    for idx in range(min(16,act.size(0))):
        axarr[idx//4, idx%4].imshow(act[idx])
        fig.savefig('log/visual/mobilenet_conv_last.jpg')
    act = activation['conv'].squeeze()
    print('act shape:', act.shape)
    fig, axarr = plt.subplots(4,4)
    for idx in range(min(16,act.size(0))):
        axarr[idx//4, idx%4].imshow(act[idx])
        fig.savefig('log/visual/mobilenet_conv.jpg')

#    print(getattr(model, 'regime'))
#    in_ten = torch.randn(32, 3, 224, 224)
#    feat, embs, output = model(in_ten)
#    print(feat[0].size())
#    print(embs[0].size())
#    print(feat[2].size())