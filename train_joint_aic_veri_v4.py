#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 17:21:34 2019
Train aic/veri_ict/vid jointly
@author: rwzhang
"""

import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import torch.optim as optim
import sys,os,logging,time,itertools
import pandas as pd
import numpy as np
from sklearn.utils import shuffle
import models
from resnet50_fc import EmbedNetwork, EmbedBaseNet
from othernet_fc import embvgg_bn,EMBVGG
from EmbedVGGM import vggm,VGGM
from mobilenetv2 import  MobileNetV2

from gen_triplet import BatchHardTripletSelector,ClusterHardTripletSelector
from batch_sampler import BatchSampler,BatchSamplerWithCam
from utils.loss import TripletLoss, Soft_TripletLoss
from utils.optimizer import AdamOptimWrapper
from utils.logger import logger

from preprocess_data import AICData, convert_xml, VehicleID, VReID
import argparse, pickle,re
from tqdm import tqdm

# construct the argument parser and parse the arguments
#Usage:  python train_joint_aic_veri_v2.py --model resnet --n_layer 50 --train_iterations 25000 --checkpoint_frequency 5000 --resume 'from_veri' --ngpu 3
ap = argparse.ArgumentParser(description='Train a verification model')
ap.add_argument('--label_bin', dest='label_bin', default = "smallvggnet_lb.pickle", help="path to output label binarizer")
ap.add_argument('--multi_proc', dest='multi_proc', default=4, help= 'the number of workers to deploy for multiprocessing options')
ap.add_argument('--img_dir', dest='img_dir', help='img_dir', default='aic19-track2-reid/image_train/', type=str)
ap.add_argument('--img_list', dest='img_list', help='input xml file', default='aic19-track2-reid/train_label.xml', type=str)
ap.add_argument('--model', dest = 'model', help='model', default='resnet', type=str)
ap.add_argument('--n_layer', dest='n_layer', help='n_layer', default=50, type=int)
ap.add_argument('--stage5', dest='stage5', help='whether to add layer5 for resnet50 and resnet101', default=False, type=bool)
ap.add_argument('--save_path', dest='save_path', help='save_path', default='joint_aic_vid/', type=str)
ap.add_argument('--vid_img_dir', dest='vid_img_dir', help='VehicleID img_dir', default='/home/carson/2019AICity_carson/Track2/DATASETS/VehicleID/image/', type=str)
ap.add_argument('--vid_img_list', dest='vid_img_list', help='img_list', default='/home/carson/2019AICity_carson/Track2/DATASETS/VehicleID/train_test_split/train_list.txt', type=str)
ap.add_argument('--veri_img_dir', dest='veri_img_dir', help='VeRI img_dir', default='/home/carson/2019AICity_carson/Track2/DATASETS/VeRi/image_train/', type=str)
ap.add_argument('--veri_img_list', dest='veri_img_list', help='img_list', default='/home/carson/2019AICity_carson/Track2/ReID_CNN/database/VeRi_train_info.txt', type=str)

ap.add_argument('--input_size', dest='input_size', help = 'the input size to resize the original image to for training', type = int, default=224)
ap.add_argument('--normalize', dest = 'normalize', help = 'whether to normalize for training', type = bool, default=True)
ap.add_argument('--crop_scale',dest = 'crop_scale', type=float,default=1.0,help='The percentage of scale when cropping')
#orginally:--learning_rate 0.0001 -train_iterations 25000 --decay_start_iteration 15000
ap.add_argument('--learning_rate', dest = 'learning_rate', default=0.001, type=float,help='The initial value of the learning-rate, before it kicks in.')
ap.add_argument('--train_iterations', dest = 'train_iterations', default = 45000, type=int,help='Number of training iterations.')
ap.add_argument('--decay_start_iteration',dest ='decay_start_iteration', default = 1000, type=int, help='At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.')
ap.add_argument('--checkpoint_frequency', dest = 'checkpoint_frequency', default=10, type=int, help='Set this to 0 to disable intermediate storing.')
ap.add_argument('--batch_p', dest = 'batch_p', default=18, type=int, help='The number P used in the PK-batches')
ap.add_argument('--batch_k', dest = 'batch_k', default=4, type=int, help='The numberK used in the PK-batches')
ap.add_argument('--batch_c', dest = 'batch_c', default=2, type=int, help='The cam number used in the PK-batches')
ap.add_argument('--num_class', dest = 'num_class', default=333, type=int, help='The num_classes for classification')
ap.add_argument('--batch_hard', dest = 'batch_hard', action='store_true',help='whether to use batch_hard for triplet loss')
ap.add_argument('--margin', dest = 'margin', default='0', type=str, help='margin of triplet loss ("soft" or float)')
ap.add_argument('--resume', dest = 'resume', default='', type=str, help='The resume point for continuous training')
#ap.add_argument('-tr', '--train_val', default = 0.1, type= float, help='train/validataion split ratio')
ap.add_argument('--ngpu', dest = 'ngpu', default = 1, type=int, help='number of gpus to use')
args = ap.parse_args()
# =============================================================================
ngpu = args.ngpu
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
margin = args.margin if args.margin=='soft' else float(args.margin)
# =============================================================================
root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
model = args.model + str(args.n_layer)
save_path = root_dir + args.save_path + model + '/'
#dataset = args.dataset
#img_dir = args.img_dir
#img_list = args.img_list
label_bin = root_dir + args.label_bin
train_xml = root_dir + args.img_list
img_dir = root_dir + args.img_dir
vid_img_dir = args.vid_img_dir
vid_img_list = args.veri_img_list
veri_img_dir = args.veri_img_dir
veri_img_list = args.veri_img_list
train_csv = root_dir + "aic19-track2-reid/train_label.csv"
train_feat = root_dir + "aic19-track2-reid/train_feat.csv"
train_feat_pkl = root_dir + "aic19-track2-reid/train_feat.pkl"
train_track = root_dir + "aic19-track2-reid/train_track.txt"
if args.n_layer == 50:
    print("INFO: For resnet50, need to normaize to train")
    img_norm = True
else:
    img_norm = args.normalize
# =============================================================================
# construct the image generator for data augmentation
lb = pickle.loads(open(label_bin, "rb").read())
assert args.num_class == len(lb.classes_)
cam2img_df = convert_xml(train_xml)
#cam2img_df = shuffle(cam2img_df, random_state = 10086)

vid_img_dir = 'DATASETS/VehicleID/image/'
vid_img_list = 'DATASETS/VehicleID/train_test_split/train_list.txt'
# =============================================================================

# =============================================================================
def train():
    #setup
    torch.multiprocessing.set_sharing_strategy('file_system')
    if not os.path.exists(save_path): os.makedirs(save_path)
    #dataloader
    vid_selector = BatchHardTripletSelector()
    #aic_selector = ClusterHardTripletSelector()
    aic_ds = AICData(img_dir, cam2img_df, img_size = args.input_size, normalize = img_norm, num_crops = 1, crop_scale = args.crop_scale)
    vid_ds = VehicleID(vid_img_dir, vid_img_list, img_size=  args.input_size, is_train = True, normalize = img_norm, crop_scale = args.crop_scale)
    veri_ds = VReID(veri_img_dir, veri_img_list, img_size= args.input_size, is_train = True, normalize = img_norm, crop_scale = args.crop_scale)

    logger.info('Dataloading Done')
    vid_sampler = BatchSampler(vid_ds, args.batch_p, args.batch_k) #batch sample the dataset
    vid_dl = DataLoader(vid_ds, batch_sampler = vid_sampler, num_workers = args.multi_proc) # need to update the worker
    vid_diter = iter(vid_dl)
    aic_sampler = BatchSampler(aic_ds, args.batch_p, args.batch_k) #batch sample the dataset
    #aic_sampler = BatchSamplerWithCam(aic_ds, args.batch_p, args.batch_k, args.batch_c) #batch sample the dataset
    aic_dl = DataLoader(aic_ds, batch_sampler = aic_sampler, num_workers = args.multi_proc) # need to update the worker
    aic_diter = iter(aic_dl)
    veri_sampler = BatchSampler(veri_ds, args.batch_p, args.batch_k) #batch sample the dataset
    veri_dl = DataLoader(veri_ds, batch_sampler = veri_sampler, num_workers = args.multi_proc) # need to update the worker
    veri_diter = iter(veri_dl)
    print('vid_sampler.iter_num:{}, veri_sampler.iter_num:{}, aic_sampler.iter_num:{}'.format(vid_sampler.iter_num, veri_sampler.iter_num, aic_sampler.iter_num))
    print('vid_dl len:{}, veri_dl len:{}, aic_dl len:{}'.format(len(vid_dl), len(veri_dl), len(aic_dl)))

    #model and loss
    logger.info('setting up backnone models and loss')
    logger.info('Build Model from scratch')

    # Get Model
    # commented out on 03/12, this will help train for lower resolution images
    # base_net = models.FeatRes51Net(stage5 = args.stage5, n_layers=args.n_layer, pretrained=True)
    # base_net = EmbedBaseNet(pretrained_base=True)
    base_net = embvgg_bn(model_name = 'vgg16', pretrained=True, num_classes = 333, with_top = False)
    vid_id_net = models.NLayersFC(base_net.output_dim, vid_ds.n_id)
    aic19_id_net = models.NLayersFC(base_net.output_dim, aic_ds.n_id)
    cams_net = models.NLayersFC(base_net.output_dim, aic_ds.n_cid)
    veri_id_net = models.NLayersFC(base_net.output_dim, veri_ds.n_id)
    veri_color_net = models.NLayersFC(base_net.output_dim, veri_ds.n_color)
    veri_type_net = models.NLayersFC(base_net.output_dim, veri_ds.n_type)

    print('vid_ds_n_id:{},  aic_ds.n_vid:{},  aic_ds.n_cid:{}'.format(vid_ds.n_id, aic_ds.n_id, aic_ds.n_cid))
    print('veri_ds_n_id:{}, veri_ds.n_color:{},  veri_ds.n_type:{}'.format(veri_ds.n_id, veri_ds.n_color, veri_ds.n_type))
    net_dict = {'base_net':base_net,'vid_id_net':vid_id_net,'aic19_id_net':aic19_id_net,'cams_net':cams_net, 'veri_id_net':veri_id_net, 'veri_color_net':veri_color_net, 'veri_type_net': veri_type_net}

    cur_epoch = 0
    if args.resume == 'from_veri':
        resume_pt = 'Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl'
        logger.info('fine-turn from {}'.format(resume_pt))
    elif args.resume != '':
        resume_pt = args.resume
        state_dict = torch.load(resume_pt)
        if 'epoch' in resume_pt:
            cur_epoch = state_dict['epoch']
        else:
            m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.resume)
            cur_epoch = int(m.groupdict()['epoch'])
        logger.info('fine-turn from checkpoint: {}, epoch: {}'.format(args.resume, cur_epoch))
    if args.resume is not None and args.resume != '':
        state_dict = torch.load(resume_pt)
        if 'epoch' in state_dict:
            state_dict = state_dict['model_state_dict']
        #for key in list(state_dict.keys()):
        #    if key in set(list(['fc','embed','fc_head'])):
        #        del state_dict[key]
        base_net.load_state_dict(state_dict,strict=False)

    with torch.cuda.device(0):
        base_net = base_net.cuda()
        vid_id_net = vid_id_net.cuda()
        aic19_id_net = aic19_id_net.cuda()
        cams_net = cams_net.cuda()
        veri_id_net = veri_id_net.cuda()
        veri_color_net = veri_color_net.cuda()
        veri_type_net = veri_type_net.cuda()

        #net = nn.DataParallel(net)
    if (device.type == 'cuda') and (ngpu > 1):
        base_net = nn.DataParallel(base_net, list(range(ngpu)))
        vid_id_net = nn.DataParallel(vid_id_net, list(range(ngpu)))
        aic19_id_net = nn.DataParallel(aic19_id_net, list(range(ngpu)))
        cams_net = nn.DataParallel(cams_net, list(range(ngpu)))
        veri_id_net = nn.DataParallel(veri_id_net, list(range(ngpu)))
        veri_color_net = nn.DataParallel(veri_color_net, list(range(ngpu)))
        veri_type_net = nn.DataParallel(veri_type_net, list(range(ngpu)))

    # define Triplet loss
    triplet_loss = TripletLoss(margin=None).cuda()
    hardtrip_loss = Soft_TripletLoss(margin=margin, batch_hard=args.batch_hard)
    #all get the softmax categorical cross entropy loss
    softmax_criterion = torch.nn.CrossEntropyLoss()

    #optimizer
    logger.info('create optimizer')
    optim_base = AdamOptimWrapper(base_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration*3, t1 = args.train_iterations)
    optim_aic19_id = AdamOptimWrapper(aic19_id_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_cams = AdamOptimWrapper(cams_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_vid_id = AdamOptimWrapper(vid_id_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_veri_id = AdamOptimWrapper(veri_id_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_veri_color = AdamOptimWrapper(veri_color_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_veri_type = AdamOptimWrapper(veri_type_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)

#    optim_base = optim.Adam(base_net.parameters(), lr=args.learning_rate)
#    optim_aic19_id = optim.Adam(aic19_id_net.parameters(), lr=args.learning_rate)
#    optim_cams = optim.Adam(cams_net.parameters(), lr=args.learning_rate)
#    optim_vid_id = optim.Adam(vid_id_net.parameters(), lr=args.learning_rate)
#    optim_veri_id = optim.Adam(veri_id_net.parameters(), lr=args.learning_rate)
#    optim_veri_color = optim.Adam(veri_color_net.parameters(), lr=args.learning_rate)
#    optim_veri_type = optim.Adam(veri_type_net.parameters(), lr=args.learning_rate)

    #train
    logger.info('start training...')
    vid_loss_trip_avg = []
    vid_loss_trip_feat_avg = []
    vid_loss_id_avg = []
    aic_loss_trip_avg = []
    aic_loss_trip_feat_avg = []
    aic_loss_vid_avg = []
    aic_loss_cid_avg = []
    veri_loss_trip_avg = []
    veri_loss_trip_feat_avg = []
    veri_loss_id_avg = []
    veri_loss_color_avg = []
    veri_loss_type_avg = []
    
    visited_vid = set()
    visited_aic_vid = set()
    visited_aic_cam = set()
    visited_veri = set()
    e = cur_epoch
    t_start = time.time()
    
    epoch_size = (max(len(veri_dl), len(vid_dl), len(aic_dl))+1)//min(len(veri_dl), len(vid_dl), len(aic_dl))+1
    n_epochs = args.train_iterations // epoch_size // min(len(veri_dl), len(vid_dl), len(aic_dl))
    for e in range(cur_epoch, n_epochs):
        logger.info('epoch start/total: {}/{}\n'.format(cur_epoch, n_epochs))

        pbar = tqdm(total=epoch_size,ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))
        
        for i in range(epoch_size):
            logger.info('epoch info: {}, iteration: {}\n'.format(e, i))
            epoch_loss = 0
# =============================================================================
#           train veri dataset
            for n in range(veri_sampler.iter_num+1):#veri_sampler.iter_num or args.batch_p
                count = e*epoch_size+i*veri_sampler.iter_num
                try:
                    veri_imgs, veri_lbs, veri_color, veri_type, _ = next(veri_diter)
                except StopIteration:
                    veri_diter = iter(veri_dl)
                    veri_imgs, veri_lbs, veri_color, veri_type,_ = next(veri_diter)
                # print('veri_dataset: epoch: {}, labels: {}'.format(count+n, np.unique(veri_lbs)))
                for veri_lb in veri_lbs:
                    visited_veri.add(veri_lb.item())
                
                with torch.cuda.device(0):
                    veri_imgs = veri_imgs.cuda(non_blocking=True)
                    veri_lbs = veri_lbs.cuda(non_blocking=True)
                    veri_color = veri_color.cuda(non_blocking=True)
                    veri_type = veri_type.cuda(non_blocking=True)
                    
                #aply inference to generate embeddings from fc
                base_net.zero_grad()
                veri_id_net.zero_grad()
                veri_color_net.zero_grad()
                veri_type_net.zero_grad()
                veri_feat, veri_embds = base_net(veri_imgs)
                #with torch.cuda.device(0):
                #    veri_feat = veri_feat.cuda(non_blocking=True)
                pred_lbs = veri_id_net(veri_feat)
                pred_color = veri_color_net(veri_feat)
                pred_type = veri_type_net(veri_feat)
                veri_anchor, veri_positives, veri_negatives = vid_selector(veri_embds, veri_lbs)
                print('veri_dataset: veri_feat.mean: {}, anchor.shape: {}\n'.format(veri_feat.mean(), veri_embds.shape))
                veri_loss_trip = hardtrip_loss(veri_embds, veri_lbs)
                veri_feat_anchor, veri_feat_pos, veri_feat_neg = vid_selector(veri_feat, veri_color)
                # print('veri_dataset: veri_feat.mean: {}, anchor.shape: {}, post: {}, neg: {}\n'.format(veri_feat.mean(), veri_feat_anchor.mean(), veri_feat_pos.mean(), veri_feat_neg.mean()))
                veri_loss_trip_feat = triplet_loss(veri_feat_anchor, veri_feat_pos, veri_feat_neg)
                
                veri_loss_id = softmax_criterion(pred_lbs, veri_lbs)
                veri_loss_color = softmax_criterion(pred_color, veri_color)
                veri_loss_type = softmax_criterion(pred_type, veri_type)
                # print('veri debug @232 pred_lbs: {}, veri_lbs: {}'.format(pred_lbs, veri_lbs))
                veri_loss_all = veri_loss_id + veri_loss_color + veri_loss_type + veri_loss_trip.mean() + veri_loss_trip_feat
                epoch_loss += veri_loss_all

                # optim_base.zero_grad()
                # optim_veri_id.zero_grad()
                veri_loss_all.backward()
                optim_base.step()
                optim_veri_id.step()
                optim_veri_color.step()
                optim_veri_type.step()

                veri_loss_trip_avg.append(veri_loss_trip.mean().detach().cpu().numpy())#calculate on the cpu
                veri_loss_trip_feat_avg.append(veri_loss_trip_feat.detach().cpu().numpy())#calculate on the cpu
                veri_loss_id_avg.append(veri_loss_id.detach().cpu().numpy())
                veri_loss_color_avg.append(veri_loss_color.detach().cpu().numpy())
                veri_loss_type_avg.append(veri_loss_type.detach().cpu().numpy())
                print('veri_dataset: epoch: {}, loss_veri_triplet: {}, veri_loss_trip_feat: {}, loss_id_veri: {}, loss_color_veri: {}, loss_type_veri: {}, veri_loss_all: {}\n'.format(e*epoch_size+i*veri_sampler.iter_num+n, 
                      veri_loss_trip.mean(), veri_loss_trip_feat, veri_loss_id, veri_loss_color, veri_loss_type, veri_loss_all))

            veri_loss_trip_avg = sum(veri_loss_trip_avg) / len(veri_loss_trip_avg)
            veri_loss_trip_feat_avg = sum(veri_loss_trip_feat_avg) / len(veri_loss_trip_feat_avg)
            veri_loss_id_avg = sum(veri_loss_id_avg) / len(veri_loss_id_avg)
            veri_loss_color_avg = sum(veri_loss_color_avg) / len(veri_loss_color_avg)
            veri_loss_type_avg = sum(veri_loss_type_avg) / len(veri_loss_type_avg)
            now = time.time()
            time_interval = now - t_start
            # print('visited veri:{}'.format(visited_veri))
            logger.info('veri_dataset: epoch info: {}, iteration: {}, currently_visited_labels: {}'.format(e, i, len(visited_veri)))
            logger.info('veri_dataset: loss_all_veri: {}, base_net_lr: {:4f}, time: {:3f}\n'.format(veri_loss_all, optim_base.lr, time_interval))
            logger.info('veri_dataset: loss_veri_triplet: {},veri_loss_trip_feat: {}, loss_id_veri: {}'.format(veri_loss_trip_avg, veri_loss_trip_feat_avg, veri_loss_id_avg))
            logger.info('veri_dataset: veri_loss_color: {}, veri_loss_type: {}'.format(veri_loss_color_avg, veri_loss_type_avg))
            veri_loss_trip_avg = []
            veri_loss_trip_feat_avg = []
            veri_loss_id_avg = []
            veri_loss_color_avg = []
            veri_loss_type_avg = []
            t_start = max(time.time(), now) # check to see if we have a pause
            
# =============================================================================
#           train vid net first
#           print('[INFO] initial train to reach a decent loss')
            # for n in range(vid_sampler.iter_num):#vid_sampler.iter_num or args.batch_p
            for n in range(veri_sampler.iter_num+1):#vid_sampler.iter_num or args.batch_p
                count = e*epoch_size+i*veri_sampler.iter_num
                try:
                    vid_imgs, vid_lbs, _, _ = next(vid_diter)
                except StopIteration:
                    vid_diter = iter(vid_dl)
                    vid_imgs,vid_lbs,_,_ = next(vid_diter)
                for vid_lb in vid_lbs:
                    visited_vid.add(vid_lb.item())
                
                with torch.cuda.device(0):
                    vid_imgs = vid_imgs.cuda(non_blocking=True)
                    vid_lbs = vid_lbs.cuda(non_blocking=True)

                #aply inference to generate embeddings from fc
                base_net.zero_grad()
                vid_id_net.zero_grad()
                vid_feat, vid_embds = base_net(vid_imgs)
                #with torch.cuda.device(0):
                #    vid_feat = vid_feat.cuda(non_blocking=True)
                vid_cls = vid_id_net(vid_feat)
                vid_anchor, vid_positives, vid_negatives = vid_selector(vid_embds, vid_lbs)
                vid_loss_trip = hardtrip_loss(vid_embds, vid_lbs)
                vid_feat_anchor, vid_feat_pos, vid_feat_neg = vid_selector(vid_feat, vid_lbs)
                vid_loss_trip_feat = triplet_loss(vid_feat_anchor, vid_feat_pos, vid_feat_neg)
                vid_loss_id = softmax_criterion(vid_cls, vid_lbs)
                # print('VID debug @232 vid_cls: {}, vid_lbs: {}'.format(vid_cls, vid_lbs))
                vid_loss_all = 0.5*vid_loss_id + vid_loss_trip.mean() + vid_loss_trip_feat
                epoch_loss += vid_loss_all
                # optim_base.zero_grad()
                # optim_vid_id.zero_grad()
                vid_loss_all.backward()
                optim_base.step()
                optim_vid_id.step()

                vid_loss_trip_avg.append(vid_loss_trip.mean().detach().cpu().numpy())#calculate on the cpu
                vid_loss_trip_feat_avg.append(vid_loss_trip_feat.detach().cpu().numpy())#calculate on the cpu
                vid_loss_id_avg.append(vid_loss_id.detach().cpu().numpy())
                print('VID_dataset: epoch: {}, loss_vid_triplet: {}, vid_loss_trip_feat: {}, loss_id_vid: {}, vid_loss_all: {}\n'.format(count+n, 
                      vid_loss_trip.mean(), vid_loss_trip_feat, vid_loss_id, vid_loss_all))

            vid_loss_trip_avg = sum(vid_loss_trip_avg) / len(vid_loss_trip_avg)
            vid_loss_trip_feat_avg = sum(vid_loss_trip_feat_avg) / len(vid_loss_trip_feat_avg)
            vid_loss_id_avg = sum(vid_loss_id_avg) / len(vid_loss_id_avg)
            now = time.time()
            time_interval = now - t_start
            # print('visited vid:{}'.format(visited_vid))
            logger.info('VID_dataset: epoch info: {}, iteration: {}, currently_visited_labels: {}'.format(e, i, len(visited_vid)))
            logger.info('VID_dataset: loss_all_vid: {}, base_net_lr: {:4f}, time: {:3f}\n'.format(vid_loss_all, optim_base.lr, time_interval))
            logger.info('VID_dataset: loss_vid_triplet: {},vid_loss_trip_feat: {}, loss_id_vid: {}'.format(vid_loss_trip_avg, vid_loss_trip_feat_avg, vid_loss_id_avg))
            vid_loss_trip_avg = []
            vid_loss_trip_feat_avg = []
            vid_loss_id_avg = []
            t_start = max(time.time(), now) # check to see if we have a pause
# =============================================================================
#           train aic19 dataset
            for n in range(aic_sampler.iter_num+1):
                count = e*epoch_size+i*aic_sampler.iter_num
                # print('AIC_dataset: epoch: {}\n'.format(count+n))
                try:
                    imgs, lbs, cams, _ = next(aic_diter)
                except StopIteration:
                    aic_diter = iter(aic_dl)
                    imgs, lbs, cams,_ = next(aic_diter)
                for lb in lbs:
                    visited_aic_vid.add(lb.item())
                for cam in cams:
                    visited_aic_cam.add(cam.item())

                with torch.cuda.device(0):
                    b_imgs = imgs.cuda(non_blocking=True)
                    lbs = lbs.cuda(non_blocking=True)
                    cams = cams.cuda(non_blocking=True)
                base_net.zero_grad()
                aic19_id_net.zero_grad()
                cams_net.zero_grad()
                aic_feat, embds = base_net(b_imgs)
                # with torch.cuda.device(0):
                    # aic_feat = aic_feat.cuda(non_blocking=True)
                cls_lbs = aic19_id_net(aic_feat)
                cls_cams = cams_net(aic_feat)
                #generate the triplet
                #feat_anchor, feat_pos, feat_neg = aic_selector(aic_feat, lbs, cams)
                feat_anchor, feat_pos, feat_neg = vid_selector(aic_feat, lbs)
                aic_loss_trip_feat = triplet_loss(feat_anchor, feat_pos, feat_neg)
                anchor, positives, negatives = vid_selector(embds, lbs)
                #anchor, positives, negatives = aic_selector(embds, lbs, cams)
                aic_loss_trip = hardtrip_loss(embds, lbs)
                aic_loss_vid = softmax_criterion(cls_lbs, lbs)
                aic_loss_cid = softmax_criterion(cls_cams, cams)
                # loss_all = 0.5*loss_id + 0.5* loss_cid + loss_triplet
                loss_all = aic_loss_vid + aic_loss_cid + aic_loss_trip.mean() + aic_loss_trip_feat
                epoch_loss += loss_all

                optim_cams.zero_grad()
                loss_all.backward()
                optim_base.step()
                optim_aic19_id.step()
                optim_cams.step()

                #detach the current tensor from current graph. The result will never require gradient.
                aic_loss_trip_avg.append(aic_loss_trip.mean().detach().cpu().numpy())#calculate on the cpu
                aic_loss_trip_feat_avg.append(aic_loss_trip_feat.detach().cpu().numpy())#calculate on the cpu
                aic_loss_vid_avg.append(aic_loss_vid.detach().cpu().numpy())
                aic_loss_cid_avg.append(aic_loss_cid.detach().cpu().numpy())
                print('AIC_dataset: epoch: {}, loss_vid_triplet: {}, vid_loss_trip_feat: {}, loss_vid: {}, loss_cid: {}, vid_loss_all: {}\n'.format(count+n, aic_loss_trip.mean(), aic_loss_trip_feat, aic_loss_vid, aic_loss_cid, loss_all))

            aic_loss_trip_avg = sum(aic_loss_trip_avg) / len(aic_loss_trip_avg)
            aic_loss_trip_feat_avg = sum(aic_loss_trip_feat_avg) / len(aic_loss_trip_feat_avg)
            aic_loss_vid_avg = sum(aic_loss_vid_avg) / len(aic_loss_vid_avg)
            aic_loss_cid_avg = sum(aic_loss_cid_avg) / len(aic_loss_cid_avg)
            t_end = time.time()
            time_interval = t_end - t_start
            logger.info('AIC19_dataset: epoch info: {}, iteration: {}, loss_all: {}, base_net_lr: {:4f}, time: {:3f}\n'.format(e, i, loss_all, optim_base.lr, time_interval))
            logger.info('AIC19_dataset: currently_visited_labels: {}, currently_visited_cams:{}'.format(len(visited_aic_vid), len(visited_aic_cam)))
            logger.info('AIC19_dataset: loss_vid: {}, loss_cid: {}'.format(aic_loss_vid_avg, aic_loss_cid_avg))
            logger.info('AIC19_dataset: loss_triplet: {}, loss_triplet_feat: {}'.format(aic_loss_trip_avg, aic_loss_trip_feat_avg))
            aic_loss_trip_avg = []
            aic_loss_trip_feat_avg = []
            aic_loss_vid_avg = []
            aic_loss_cid_avg = []
            t_start = max(time.time(), t_end) # check to see if we have a pause
        
            pbar.update(1)
            pbar.set_postfix({'loss':'%f'%(epoch_loss/(i+1))})
        pbar.close()
# =============================================================================             
        if e % args.checkpoint_frequency == 0 and e != 0:
            logger.info('Saving the intermediate training status')
            for net_name, net in net_dict.items():
                save_model = save_path + 'ep{}_model_{}_{}.pkl'.format(e, model, net_name)
                print(save_model)
                ver = 2 #verbose = 2
                while os.path.exists(save_model):
                    logger.info('Model Exists, Version Update!!!')
                    save_model = save_model + '_v' + str(ver)
                    ver = ver + 1
                torch.save({'epoch': e, 'model_state_dict': net.state_dict()}, save_model)
# =============================================================================
        base_net.eval()
        vid_id_net.eval()
        aic19_id_net.eval()
        cams_net.eval()
        veri_id_net.eval()
        veri_color_net.eval()
        veri_type_net.eval()

        base_net.train()
        vid_id_net.train()
        aic19_id_net.train()
        cams_net.train()
        veri_id_net.train()
        veri_color_net.train()
        veri_type_net.train()

    ## dump
    logger.info('saving trained model')
    for net_name, net in net_dict.items():
        save_model = save_path + 'ep{}_model_{}_{}.pkl'.format(e, model, net_name)
        print(save_model)
        ver = 2 #verbose = 2
        while os.path.exists(save_model):
            logger.info('Model Exists, Version Update!!!')
            save_model = save_model + '_v' + str(ver)
            ver = ver + 1
        torch.save({'epoch': e, 'model_state_dict': net.state_dict()}, save_model)
    logger.info('Finished at {}'.format(time.strftime("%D_%H:%M:%S")))

if __name__ == '__main__':
    train()
