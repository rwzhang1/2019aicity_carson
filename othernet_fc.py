# -*- coding: utf-8 -*-
"""
Created on Sun Jan 27 03:46:40 2019

@author: Huawei
"""
import torch
import torch.nn as nn
import torchvision
import torchvision.models as models
import torch.utils.model_zoo as model_zoo
import math

__all__ = [
    'VGG', 'vgg11', 'vgg11_bn', 'vgg13', 'vgg13_bn', 'vgg16', 'vgg16_bn',
    'vgg19_bn', 'vgg19',
]


model_urls = {
    'vgg11': 'https://download.pytorch.org/models/vgg11-bbd30ac9.pth',
    'vgg13': 'https://download.pytorch.org/models/vgg13-c768596a.pth',
    'vgg16': 'https://download.pytorch.org/models/vgg16-397923af.pth',
    'vgg19': 'https://download.pytorch.org/models/vgg19-dcbb9e9d.pth',
    'vgg11_bn': 'https://download.pytorch.org/models/vgg11_bn-6002323d.pth',
    'vgg13_bn': 'https://download.pytorch.org/models/vgg13_bn-abd245e5.pth',
    'vgg16_bn': 'https://download.pytorch.org/models/vgg16_bn-6c64b313.pth',
    'vgg19_bn': 'https://download.pytorch.org/models/vgg19_bn-c79401a0.pth',
}
model_cfg = {
    'vgg11': 'A',
    'vgg13': 'B',
    'vgg16': 'D',
    'vgg19': 'E',
    'vgg11_bn': 'A',
    'vgg13_bn': 'B',
    'vgg16_bn': 'D',
    'vgg19_bn': 'E',
        }
cfg = {
    'A': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'D': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'E': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}


class VGG(nn.Module):
    def __init__(self, features, num_classes=1000, init_weights=True):
        super(VGG, self).__init__()
        self.features = features
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        self.classifier = nn.Sequential(
            nn.Linear(512 * 7 * 7, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(4096, num_classes),
        )
        if init_weights:
            self._initialize_weights()

    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)
                
class VGGBASE(nn.Module):
    def __init__(self, model_name='vgg19', num_classes=1000, dims = 128, pretrained=True, init_weights = False,with_top=False, **kwargs):
        super(VGGBASE,self).__init__()
        if model_name == 'vgg16':
            old_model = models.vgg16(pretrained=pretrained)
        elif model_name == 'vgg19':
            old_model = models.vgg19(pretrained=pretrained)
        self.features = old_model.features
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        #removing last layer from the classifier
        self.classifier = nn.Sequential(old_model.classifier[:-1])
        self.fc_head = DenseReLU(in_feats = 4096, out_feats = 2048)
        self.fc = nn.Linear(in_features = 2048, out_features = num_classes)
        self.embed = nn.Linear(in_features = 2048, out_features = dims)
        self.with_top = with_top
        self.output_dim = self.fc.in_features
        
        if init_weights:
            self._initialize_weights()
        else:
            for key,module in self._modules.items():                    
                if key.find('embed') != -1:
                    print(key)
                    nn.init.kaiming_normal_(self.embed.weight, a=1)
                    nn.init.constant_(self.embed.bias, 0)
                if key.find('fc') != -1:
                    print(key)
                    nn.init.normal_(self.fc.weight, 0, 0.01)
                    nn.init.constant_(self.fc.bias, 0)
                
    def forward(self, x):        
        # print('x shape before feature', x.shape)
        print(self._modules.items())
        # print('after flattening', x.shape)
        # print('x shape before feature', x.shape)
        x = self.features(x)
        # print('x shape after feature', x.shape)
        x = self.avgpool(x)
        # print('before flattening', x.shape)
        x = x.view(x.size(0), -1)
        # print('after flattening', x.shape)
        x = self.classifier(x)
        x = self.fc_head(x)
        embs = self.embed(x)
        fc = self.fc(x)
        if self.with_top:
            return fc, x, embs
        else:
            return x, embs

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)
                
        
class EMBVGG(nn.Module):
    def __init__(self, features, num_classes=1000, dims = 128, init_weights = True, with_top=False, *args, **kwargs):
        super(EMBVGG, self).__init__()
        self.features = features
        self.conv_1x1_bn = CONV1X1_BN(512, 256)
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        self.classifier = nn.Sequential(
            # nn.Linear(512 * 7 * 7, 4096),
            nn.Linear(256 * 7 * 7, 2048),
            nn.ReLU(True),
            nn.Dropout(),
            # nn.Linear(4096, 4096),
            # nn.ReLU(True),
            # nn.Dropout(),
            # nn.Linear(4096, num_classes),
       )
        #self.fc_head = DenseReLU(in_feats = 4096, out_feats = 2048)
        self.fc = nn.Linear(in_features = 2048, out_features = num_classes)
        self.embed = nn.Linear(in_features = 2048, out_features = dims)
        self.with_top = with_top
        self.output_dim = self.fc.in_features
        if init_weights:
            self._initialize_weights()
        else:
            for key,module in self._modules.items():                    
                if key.find('embed') != -1:
                    print(key)
                    nn.init.kaiming_normal_(self.embed.weight, a=1)
                    nn.init.constant_(self.embed.bias, 0)
                if key.find('fc_head') != -1:
                    print(key)
                    nn.init.normal_(self.fc_head.weight, 0, 0.01)
                    nn.init.constant_(self.fc_head.bias, 0)
                elif key.find('fc') != -1:
                    print(key)
                    nn.init.normal_(self.fc.weight, 0, 0.01)
                    nn.init.constant_(self.fc.bias, 0)
                
    def forward(self, x):        
        # print('x shape before feature', x.shape)
        x = self.features(x)
        # print('x shape after feature', x.shape)
        x = self.conv_1x1_bn(x)
        x = self.avgpool(x)
        # print('before flattening', x.shape)
        x = x.view(x.size(0), -1)
        # print('after flattening', x.shape)
        x = self.classifier(x)
        #x = self.fc_head(x)
        embs = self.embed(x)
        fc = self.fc(x)
        if self.with_top:
            return fc, x, embs
        else:
            return x, embs

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)
                
class CONV1X1_BN(nn.Module):
    def __init__(self, in_features, out_features, *args, **kwargs):
        super(CONV1X1_BN, self).__init__(*args, **kwargs)
        self.conv = nn.Conv2d(in_features, out_features, 1, 1, 0, bias=False)
        # self.bn = nn.BatchNorm2d(out_features)
        self.relu = nn.ReLU6(inplace=True)
        self.out_features = out_features
        self._initialize_weights()
        
    def forward(self, x):
        x = self.conv(x)
        # x = self.bn(x)
        x = self.relu(x)
        return x
        
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                print('conv',m)
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

class DenseReLU(nn.Module):
    def __init__(self, in_feats, out_feats, *args, **kwargs):
        super(DenseReLU, self).__init__(*args, **kwargs)
        self.dense = nn.Linear(in_features = in_feats, out_features = out_feats)
        self.relu = nn.ReLU(inplace = True)
        self.dropout = nn.Dropout(p=0.5),
        self.in_features = in_feats
        self.out_features= out_feats
        self._initialize_weights()
        
    def forward(self, x):
        x = self.dense(x)
        x = self.relu(x)
        return x
        
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

def make_layers(cfg, batch_norm=False):
    layers = []
    in_channels = 3
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)

class DenseNormReLU(nn.Module):
    def __init__(self, in_feats, out_feats, *args, **kwargs):
        super(DenseNormReLU, self).__init__(*args, **kwargs)
        self.dense = nn.Linear(in_features = in_feats, out_features = out_feats)
        self.bn = nn.BatchNorm1d(out_feats)
        self.relu = nn.ReLU(inplace = True)

    def forward(self, x):
        x = self.dense(x)
        x = self.bn(x)
        x = self.relu(x)
        return x

def embvgg_bn(model_name = 'vgg16', pretrained = False,  batch_norm = False, **kwargs):
    if pretrained:
        kwargs['init_weights'] = False
    model = EMBVGG(make_layers(cfg[model_cfg[model_name]], batch_norm=batch_norm), **kwargs)
    if pretrained:
        model.load_state_dict(model_zoo.load_url(model_urls[model_name]), strict= False)
    return model    
    
if __name__ == "__main__":
    embed_net = embvgg_bn(model_name = 'vgg16', pretrained=False, num_classes = 333)
    in_ten = torch.randn(32, 3, 224, 224)
    print(embed_net)
    out, fc = embed_net(in_ten)
    print(out.shape)
    print(fc.shape)
