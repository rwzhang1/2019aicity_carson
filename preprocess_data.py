# -*- coding: utf-8 -*-
"""
Created on Sat Jan 12 23:29:39 2019

@author: Huawei
"""

# =============================================================================
# import libs
# =============================================================================
import matplotlib
# set the matplotlib backend so figures can be saved in the background
matplotlib.use("Agg")

# import the necessary packages
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from PIL import Image, ImageFont, ImageDraw, ImageEnhance
from utils.utils import get_random_data,letterbox_image,rotate,resize
from lxml import etree
from imutils import paths
from utils.debugger import Debugger

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import argparse, pickle, csv, cv2, re
import os, time
# =============================================================================
def convert_xml(train_xml):
    '''
    Parse input_xml and return a dataframe
    '''
    tree = etree.parse(train_xml)
    root = tree.getroot()
    train_items = [] 
    for obj in root.findall('./Items/Item'):
        #order img, vid, cami
        row_obj = obj.values()
        train_items.append(row_obj)
    train_items = sorted(train_items, key = lambda x:(x[2],x[1]))
    df = pd.DataFrame(columns=obj.keys(),data = train_items)
    df.sort_values(by=['vehicleID', 'cameraID'], inplace=True)
    return df

# =============================================================================
def convert_trainlabel(train_csv):
    '''
    Parse train_label.csv and return the dataframe of vid, imageName combo
    @return: total 478 vehicles, 1 to 323 have cameraID assigned, 324 to 478 does not have cameraID
    '''
    df_csv = pd.read_csv(train_csv, header = None, names = ['vehicleID', 'imageName'])
    df_csv.sort_values(by=['vehicleID'], inplace=True)
    df_csv['vehicleID'] = df_csv['vehicleID'].apply(lambda x: format(x,'04d'))
    return df_csv

# =============================================================================
def convert_traintrack(train_track, cam2img):
    '''
    @train_track.txt, each row shows vehicles from the same camera
    @cam2img_df: the output from convert_xml
    @vid2img_df: the output from train_lable.csv for vehicleID assignment
    @output: generate the validataion data for predicting cameraID
    '''
    test_camera = []
    visited = set()
    with open(train_track, 'r') as f:
        lines = f.readlines()
    for line in lines:
        list_imgs = line.split()#same vid and same cameraID
        if all([imgName in cam2img for imgName in list_imgs]):
            for imgName in list_imgs:
                visited.add(imgName)
            continue
        else:
            test_camera.append(list_imgs)
    return test_camera, visited

# =============================================================================
'''
##Do some cross check between train_label.csv and train_label.xml
cam2img_df = convert_xml(train_xml)
vid2img_df = convert_trainlabel(train_csv)
img2vid = sorted(vid2img_df.vehicleID.unique())
img2cam = sorted(cam2img_df.vehicleID.unique())
vid2img = sorted(vid2img_df.imageName.unique())
cam2img = sorted(cam2img_df.imageName.unique())
expand the imageName to full path:
cam2img_df['imageName'] = cam2img_df['imageName'].apply(lambda x:train_img_path+x)
vid2img_df['imageName'] = vid2img_df['imageName'].apply(lambda x:train_img_path+x)

##label binarizer
lb = LabelBinarizer()
vid_bin = lb.fit_transform(img2cam)
#f = open(args["label_bin"], "wb")
f = open(label_bin, "wb")
f.write(pickle.dumps(lb))
    
test_camera, visited = convert_traintrack(train_track, cam2img)
assert img2vid == img2cam and vid2img == cam2img and not test_camera, 'Warning: train_label.xml and train_lable.csv mismatch'
'''
# =============================================================================     
    

# =============================================================================
'''
# Data Preparation for Pytorch
'''
import torch 
import torchvision
import torchvision.transforms as transforms
from torch.utils.data import Dataset,DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from utils.random_erasing import RandomErasing
from sklearn.utils import shuffle
#from gen_aic_veri import Get_test_DataLoader, Get_train_DataLoader, Get_val_DataLoader, Remap_Label, Check_Min_Sample_Per_Class, Jitter_Transform_to_Tensor

_IMAGENET_STATS = {'mean': [0.485, 0.456, 0.406],
                   'std': [0.229, 0.224, 0.225]}

_IMAGENET_PCA = {
    'eigval': torch.Tensor([0.2175, 0.0188, 0.0045]),
    'eigvec': torch.Tensor([
        [-0.5675,  0.7192,  0.4009],
        [-0.5808, -0.0045, -0.8140],
        [-0.5836, -0.6948,  0.4203],
    ])
}
# =============================================================================
class AICData(Dataset):
    '''
    A wrapper to build the desired dataset to feed into training model
    '''
    def __init__(self, data_path, train_df, img_size, normalize=True, num_crops = 1, crop_scale = 0.6, *args, **kwargs):
        '''
        train_df: dataframe for training data, keys: 'ImageName', 'VehicleID', 'CameraID'
        '''
        super(AICData, self).__init__(*args, **kwargs)
        self.data_path = data_path      
        self.train_df = train_df
        self.tuple_df = train_df.set_index(['vehicleID', 'cameraID'])

        self.lb_ids = self.train_df['vehicleID'].tolist()
        self.lb_cams = self.train_df['cameraID'].tolist()
        self.img_list = self.train_df['imageName'].tolist()
        self.lb_ids_uniq = sorted(self.train_df.vehicleID.unique())
        self.cam_ids_uniq = sorted(self.train_df.cameraID.unique())
        self.n_id = len(self.lb_ids_uniq)
        self.n_cid = len(self.cam_ids_uniq)
        # useful for sampler
        self.lb_img_dict = dict()
        self.cam_img_dict = dict()
        self.lb_array = np.array(self.lb_ids)
        self.cams_array = np.array(self.lb_cams)
        self.img_array = np.array(self.img_list)
        self.img_size = img_size
        for lb in self.lb_ids_uniq:
            idx = np.where(self.lb_array == lb)[0]
            self.lb_img_dict.update({lb: idx})

        for clb in self.cam_ids_uniq:
            idx = np.where(self.cams_array == clb)[0]
            self.cam_img_dict.update({clb: idx})
# =============================================================================
        if normalize:
            convert_tensor = transforms.Compose([transforms.RandomHorizontalFlip(),
                                                 transforms.RandomRotation(45),
                                                 transforms.ToTensor(),
                                                 transforms.Normalize(**_IMAGENET_STATS)])
        else:
            convert_tensor = transforms.Compose([transforms.RandomHorizontalFlip(),
                                                 transforms.RandomRotation(45),
                                                 transforms.ToTensor(),])
        scale = np.random.rand()*(1.-crop_scale)+crop_scale
        img_size = int(img_size * scale)
        if num_crops == 1:
            t_list = [transforms.RandomResizedCrop(img_size, scale=(0.7, 1.0), interpolation=2),
                    #transforms.RandomCrop((img_size, img_size)),
                    convert_tensor]
        else:
            if num_crops == 5:
                t_list = [transforms.FiveCrop((img_size, img_size))]
            elif num_crops == 10:
                t_list = [transforms.TenCrop((img_size, img_size))]
            # returns a 4D tensor
            t_list.append(transforms.Lambda(lambda crops:
                                            torch.stack([convert_tensor(crop) for crop in crops])))
            
        re_val = np.random.randint(0,255, size=(3,))/255.
        t_list.append(RandomErasing(0.5, mean=re_val))
        #tran_PIL.append(Lighting(2.0, _IMAGENET_PCA['eigval'], _IMAGENET_PCA['eigvec']))
        self.trans = transforms.Compose(t_list)

    def __len__(self):
        return len(self.train_df)
    
    def __getitem__(self, idx):
        imgName, vid_idx, cid_idx = self.train_df.iloc[idx]
        orig_img = Image.open(self.data_path + '/' + imgName)
        scale = np.random.rand()*0 + 1
        #rng_size = min(max(orig_img.size), self.img_size)
        #orig_img = orig_img.resize((int(rng_size*scale), int(rng_size*scale)))
        #orig_img = transforms.Compose([transforms.RandomCrop((rng_size, rng_size))])(orig_img)
        orig_img = orig_img.resize((int(self.img_size*scale), int(self.img_size*scale)))
        #orig_img = transforms.Compose([transforms.Resize((self.img_size, self.img_size))])(orig_img)
        #orig_img = transforms.Compose([transforms.RandomCrop((self.img_size, self.img_size))])(orig_img)
        img = self.trans(orig_img)
        m = re.match(r'c(\d+)', cid_idx)
        # print("cam to index", vid_idx, self.cam_ids_uniq.index(cid_idx))
        # img = img.view(-1,img.shape[-3],img.shape[-2],img.shape[-1])
        return img, self.lb_ids_uniq.index(vid_idx), self.cam_ids_uniq.index(cid_idx), imgName
# =============================================================================

# =============================================================================
class AICTestData(Dataset):
    '''
    A wrapper to build the desired dataset to feed into test model
    '''
    def __init__(self, data_path, test_txt, img_size, duplicates = 3, normalize=True, num_crops = 10, crop_scale = 1.0, *args, **kwargs):
        '''
        test_txt: dataframe for training data, keys: 'ImageName', 'VehicleID', 'CameraID'
        '''
        super(AICTestData, self).__init__(*args, **kwargs)
        self.data_path = data_path      
        self.imgs = os.listdir(data_path)
        self.test_txt = test_txt
        self.lb_img_dict = dict() # fake an vehicleID for lb_img_dict
        self.img_size = img_size
        with open(test_txt, 'r') as f:
            buffer = f.readlines()
        for i, imgList in enumerate(buffer):
            for n in imgList.strip().split(' '):
                imgId = n.split('.')[0]
                imgId = '{0:06d}'.format(int(imgId))
                if n.split('.')[0] not in self.lb_img_dict:
                    self.lb_img_dict[imgId] = None
                self.lb_img_dict[imgId] = i
# =============================================================================
#JSON: key: self.track_id values: list of images belong to the same track
#          for i, imgList in enumerate(buffer):
#              if i not in self.lb_img_dict:
#                  self.lb_img_dict[i] = set()
#              for n in imgList.strip().split(' '):
#                  self.lb_img_dict[i].add(n)
#          self.test_df = pd.DataFrame.from_dict(self.lb_img_dict)
# =============================================================================
        scale = np.random.rand()*(1.-crop_scale)+crop_scale
        img_size = int(img_size * scale)
        tran_PIL = [# transforms.Resize((int(img_size*scale), int(img_size*scale))),
                    transforms.ColorJitter(brightness=2.0),
                    transforms.RandomHorizontalFlip(),
                    transforms.RandomRotation(30),
                        ]
        if normalize:
            convert_tensor = transforms.Compose(tran_PIL + [transforms.ToTensor(),
                                 transforms.Normalize(**_IMAGENET_STATS)])
        else:
            convert_tensor = transforms.Compose(tran_PIL + [transforms.ToTensor(),])
            
        if num_crops == 1:
            t_list = [
                # transforms.CenterCrop((img_size, img_size)),
                transforms.RandomCrop((img_size, img_size)),
                convert_tensor
            ]
        else:
            if num_crops == 5:
                t_list = [transforms.FiveCrop((img_size, img_size))]
            elif num_crops == 10:
                t_list = [transforms.TenCrop((img_size, img_size))]
            # returns a 4D tensor
            t_list.append(transforms.Lambda(lambda crops:
                                            torch.stack([convert_tensor(crop) for crop in crops])))
                
        t_list.append(Lighting(2.2, _IMAGENET_PCA['eigval'], _IMAGENET_PCA['eigvec']))
        self.trans= multi_transform(transforms.Compose(t_list), duplicates)
# =============================================================================
        outtest_txt = re.sub('test','augtest', test_txt)
        lb_img_df = pd.DataFrame.from_dict(self.lb_img_dict, orient='index')
        lb_img_df.to_csv(outtest_txt, sep = ',') 
    def __len__(self):
        return len(self.imgs)
    
    def __getitem__(self, idx):
        imgName = self.imgs[idx]
        orig_img = Image.open(self.data_path + '/' + imgName)
        #rng = np.random.randint(70,224)
        #orig_img = orig_img.resize((rng,rng))
        scale = np.random.rand()*0.3 + 1.0
        #rng_size = min(max(orig_img.size), self.img_size)
        #orig_img = orig_img.resize((int(rng_size*scale), int(rng_size*scale)))
        #orig_img = transforms.Compose([transforms.RandomCrop((rng_size, rng_size))])(orig_img)
        orig_img = orig_img.resize((int(self.img_size*scale), int(self.img_size*scale)))
        #orig_img = transforms.Compose([transforms.RandomCrop((self.img_size, self.img_size)),])(orig_img)
        img = self.trans(orig_img)
        img = img.view(-1,img.shape[-3],img.shape[-2],img.shape[-1])
        print('debug 285', img.shape)
        imgId = imgName.split('.')[0]
        imgId = '{0:06d}'.format(int(imgId))
        return img, self.lb_img_dict[imgId], self.lb_img_dict[imgId], imgName
# =============================================================================
# =============================================================================
class AICQueryData(Dataset):
    '''
    Generate Dataset from query image dir
    '''
    def __init__(self, data_path, img_size, duplicates = 10, normalize = False, num_crops = 1, crop_scale = 0.6, *args, **kwargs):
        '''
        test_txt: dataframe for training data, keys: 'ImageName', 'VehicleID', 'CameraID'
        '''
        super(AICQueryData, self).__init__(*args, **kwargs)
        self.data_path = data_path      
        self.imgs = os.listdir(data_path)
        self.img_size = img_size
# =============================================================================
#         
        scale = np.random.rand()*(1. - crop_scale) + crop_scale
        img_size = int(img_size * scale)
        tran_PIL = [# transforms.Resize((int(img_size*scale), int(img_size*scale))),
                # transforms.ColorJitter(brightness=2.0, contrast=0., saturation=0., hue=0.),
                transforms.ColorJitter(brightness=2.0),
                transforms.RandomHorizontalFlip(),
                # transforms.RandomRotation(30),
                ]
        if normalize:
            convert_tensor = transforms.Compose(tran_PIL + [transforms.ToTensor(),
                                 transforms.Normalize(**_IMAGENET_STATS)])
        else:
            convert_tensor = transforms.Compose(tran_PIL + [transforms.ToTensor(),])
        
        if num_crops == 1:
            t_list = [
                #transforms.CenterCrop((img_size, img_size)),
                transforms.RandomCrop((img_size, img_size)),
                convert_tensor
            ]
        else:
            if num_crops == 5:
                t_list = [transforms.FiveCrop((img_size, img_size))]
            elif num_crops == 10:
                t_list = [transforms.TenCrop((img_size, img_size))]
            # returns a 4D tensor
            t_list.append(transforms.Lambda(lambda crops:
                                            torch.stack([convert_tensor(crop) for crop in crops])))
        #t_list.append(Lighting(2.8, _IMAGENET_PCA['eigval'], _IMAGENET_PCA['eigvec']))
        self.trans= multi_transform(transforms.Compose(t_list), duplicates)        
# =============================================================================
        
    def __len__(self):
        return len(self.imgs)
    
    def __getitem__(self, idx):
        imgName = self.imgs[idx]
        orig_img = Image.open(self.data_path + '/' + imgName)
        #rng = np.random.randint(70,224)
        #orig_img = orig_img.resize((rng,rng))
        scale = np.random.rand()*0.3 + 1.0
        #rng_size = min(max(orig_img.size), self.img_size)
        #orig_img = orig_img.resize((int(rng_size*scale), int(rng_size*scale)))
        #orig_img = transforms.Compose([transforms.RandomCrop((rng_size, rng_size))])(orig_img)
        orig_img = orig_img.resize((int(self.img_size*scale), int(self.img_size*scale)))
        # orig_img = transforms.Compose([transforms.RandomCrop((self.img_size, self.img_size))])(orig_img)
        img = self.trans(orig_img)
        print(img.shape)
        img = img.view(-1,img.shape[-3],img.shape[-2],img.shape[-1])
        return img, imgName
    
# =============================================================================
class VehicleID(Dataset):
    '''
    a wrapper of VehicleID dataset
    '''
    def __init__(self, data_path, train_file, img_size, is_train = True, normalize=True, crop_scale = 0.6, *args, **kwargs):
        super(VehicleID, self).__init__(*args, **kwargs)
        self.is_train = is_train
        self.data_path = data_path
        self.check = os.listdir(data_path)
        self.check = [el for el in self.check if os.path.splitext(el)[1] == '.jpg']
        self.dict = {}
        f = open(train_file, 'r')
        self.train_file = f.readlines()
        self.imgs = []
        max_class = 0
        self.label_idx ={}
        self.lb_ids_uniq=set()
        now = 0
        for line in self.train_file:
            line=line.strip()
            if(len(line.split(' ')) ==2):
                car_name,car_class = line.split(' ')[0],int(line.split(' ')[1])
            elif(len(line.split(' ')) ==1 and not is_train):
                car_name,car_class = line.split(' ')[0], -1
            else:
                print('dataset wrong')
                continue
            #if car_name not in self.check:
            #      logger.warning("%s do not exists"%(car_name))
            #      continue
            if(len(car_name.split('.')) == 1):
                car_name = car_name + '.jpg'
            if car_name not in self.dict:
                self.dict[car_name] = car_class
                max_class = max(max_class, int(car_class))
                self.lb_ids_uniq.add(car_class)
                self.imgs.append(car_name)
    
        self.lb_ids = [self.dict[el] for el in self.imgs]
        self.lb_cams = [-1 for el in self.imgs]
        self.imgs = [os.path.join(data_path, el) for el in self.imgs]
        self.img_size = img_size
        scale = np.random.rand()*(1. - crop_scale) + crop_scale
        img_size = int(img_size * scale)

        if is_train:
            tran_PIL = [#transforms.Resize((img_size+50, img_size+50)),
                        #transforms.RandomCrop((img_size, img_size)),
                        transforms.RandomResizedCrop(img_size, scale=(0.7, 1.0), interpolation=2),
                        #transforms.ColorJitter(brightness=0.2, contrast=0, saturation=0, hue=0),
                        transforms.RandomRotation(20),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor()]
            # if tencrop:
            #   tran_PIL.append(transforms.TenCrop((img_size, img_size)))
            #   tran_PIL.append(transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])))
            if normalize:
                tran_PIL.append(transforms.Normalize((0.486, 0.459, 0.408), (0.229, 0.224, 0.225)))
            
            re_val = np.random.randint(0,255, size=(3,))/255.
            tran_PIL.append(RandomErasing(0.5, mean=re_val))
            self.trans = transforms.Compose(tran_PIL)
        else:
            self.trans_tuple = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.486, 0.459, 0.408), (0.229, 0.224, 0.225))
                ])
            self.Lambda = transforms.Lambda(
                lambda crops: [self.trans_tuple(crop) for crop in crops])
            self.trans = transforms.Compose([
                transforms.Resize((img_size, img_size)),
                transforms.TenCrop((img_size, img_size)),
                self.Lambda,
            ])

        # useful for sampler
        self.lb_img_dict = dict()
        # self.lb_ids_uniq = set(self.lb_ids)
        lb_array = np.array(self.lb_ids)
        for lb in self.lb_ids_uniq:
            idx = np.where(lb_array == lb)[0]
            self.lb_img_dict.update({lb: idx})

        # create car_class to index lookup table so that we can feed into pytorch to train
        for idx, car_class in enumerate(self.lb_ids_uniq):
            if car_class not in self.label_idx:
                self.label_idx[car_class] = idx
                
        self.n_id = len(self.lb_ids_uniq)
        
    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        img = Image.open(self.imgs[idx])
# =============================================================================
#         #0312: decide not to resize twice 
#         #scale = np.random.rand()*0. + 1. 
#         #img = img.resize((int(self.img_size*scale), int(self.img_size*scale)))     
# =============================================================================
        img = self.trans(img)
        if self.label_idx[self.lb_ids[idx]] >= len(self.lb_ids_uniq):
            print("label out of bound")
        # rng = np.random.randint(70,224)
        # img = img.resize((rng,rng))
        # return img, self.lb_ids[idx], self.lb_cams[idx], self.imgs[idx]
        return img, self.label_idx[self.lb_ids[idx]], self.lb_cams[idx], self.imgs[idx]
    
# =============================================================================
class VReID(Dataset):
    '''
    a wrapper of VehicleID dataset
    '''
    def __init__(self, data_path, train_file, img_size, is_train = True, normalize=True, crop_scale=0.6, *args, **kwargs):
        super(VReID, self).__init__(*args, **kwargs)
        self.is_train = is_train
        self.data_path = data_path
        self.imgs = []
        self.lb_ids = []
        self.dict = {}
        self.label_idx = {}
        self.lb_ids_uniq = {}
        self.color_list = []
        self.type_list = []
        self.lb_ids_uniq = set()
        max_class = 0
        
        file = open(train_file,'r')
        for i,row in enumerate(file):
            if i==0: continue
            line = row.strip().split(' ')
            self.imgs.append(line[0])
            car_class, color_class, type_class = int(line[1])-1, int(line[2])-1, int(line[3])-1
            self.lb_ids.append(car_class)
            self.color_list.append(color_class)
            self.type_list.append(type_class)
            car_name = line[0][-10:]
            if car_name not in self.dict:
                self.dict[car_name] = car_class
                max_class = max(max_class, int(car_class))
                self.lb_ids_uniq.add(car_class)    
                
        # create car_class to index lookup table so that we can feed into pytorch to train
        for idx, car_class in enumerate(self.lb_ids_uniq):
            if car_class not in self.label_idx:
                self.label_idx[car_class] = idx                
        file.close()

        self.img_size = img_size
        scale = np.random.rand()*(1. - crop_scale) + crop_scale
        img_size = int(img_size * scale)
        if is_train:
            tran_PIL = [#transforms.Resize((img_size+50, img_size+50)),
                        #transforms.RandomCrop((img_size, img_size)),
                        transforms.RandomResizedCrop(img_size, scale=(0.7, 1.0), interpolation=2),
                        #transforms.ColorJitter(brightness=0.2, contrast=0, saturation=0, hue=0),
                        transforms.RandomRotation(20),
                        transforms.RandomHorizontalFlip(),
                        transforms.ToTensor()]
            # if tencrop:
            #   tran_PIL.append(transforms.TenCrop((img_size, img_size)))
            #   tran_PIL.append(transforms.Lambda(lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])))
            if normalize:
                tran_PIL.append(transforms.Normalize((0.486, 0.459, 0.408), (0.229, 0.224, 0.225)))
            # re_val = np.random.randint(0,255, size=(3,))/255.
            # tran_PIL.append(RandomErasing(0.5, mean=re_val))
            self.trans = transforms.Compose(tran_PIL)
        else:
            self.trans_tuple = transforms.Compose([
                transforms.ToTensor(),
                transforms.Normalize((0.486, 0.459, 0.408), (0.229, 0.224, 0.225))
                ])
            self.Lambda = transforms.Lambda(
                lambda crops: [self.trans_tuple(crop) for crop in crops])
            self.trans = transforms.Compose([
                transforms.Resize((img_size, img_size)),
                transforms.TenCrop((img_size, img_size)),
                self.Lambda,
            ])

        # useful for sampler
        self.lb_img_dict = dict()
        # self.lb_ids_uniq = set(self.lb_ids)
        lb_array = np.array(self.lb_ids)
        for lb in self.lb_ids_uniq:
            idx = np.where(lb_array == lb)[0]
            self.lb_img_dict.update({lb: idx})
            
        self.n_id = len(self.lb_ids_uniq)
        self.n_color = len(set(self.color_list))
        self.n_type = len(set(self.type_list))
        
    def __len__(self):
        return len(self.imgs)

    def __getitem__(self, idx):
        img = Image.open(self.imgs[idx])
# =============================================================================
#         #0312: decide not to resize twice 
#         #scale = np.random.rand()*0+1 
#         #img = img.resize((int(self.img_size*scale), int(self.img_size*scale)))
#         # rng = np.random.randint(70,224)
#         # img = img.resize((rng,rng))
# =============================================================================
        img = self.trans(img)
        # return img, self.lb_ids[idx], self.lb_cams[idx], self.imgs[idx]
        return img, self.label_idx[self.lb_ids[idx]], self.color_list[idx], self.type_list[idx], self.imgs[idx]
    
#=============================================================================
def multi_transform(transform_fn, duplicates=1, dim=0):
    """preforms multiple transforms, useful to implement inference time augmentation or
     "batch augmentation" from https://openreview.net/forum?id=H1V4QhAqYQ&noteId=BylUSs_3Y7
    """
    if duplicates > 1:
        return transforms.Lambda(lambda x: torch.stack([transform_fn(x) for _ in range(duplicates)], dim=dim))
    else:
        return transform_fn
# =============================================================================
class Lighting(object):
    """Lighting noise(AlexNet - style PCA - based noise)"""

    def __init__(self, alphastd, eigval, eigvec):
        self.alphastd = alphastd
        self.eigval = eigval
        self.eigvec = eigvec

    def __call__(self, img):
        if self.alphastd == 0:
            return img

        alpha = img.new().resize_(3).normal_(0, self.alphastd)
        rgb = self.eigvec.type_as(img).clone()\
            .mul(alpha.view(1, 3).expand(3, 3))\
            .mul(self.eigval.view(1, 3).expand(3, 3))\
            .sum(1).squeeze()

        return img.add(rgb.view(3, 1, 1).expand_as(img))
# =============================================================================
if __name__ == "__main__":
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    train_xml = root_dir + 'aic19-track2-reid/train_label.xml'
    train_img_path = root_dir + 'aic19-track2-reid/image_train/'
    test_csv = root_dir + "aic19-track2-reid/test_track_id.txt"
    test_img_path = root_dir + 'aic19-track2-reid/image_test/'
    cam2img_df = convert_xml(train_xml)
    cam2img_df = shuffle(cam2img_df, random_state = 10086)
    num_crops =5
    ds = AICData(train_img_path, cam2img_df, 64, normalize=False, num_crops = 5, crop_scale=1.0)
    ds_vid = VehicleID('DATASETS/VehicleID/image/', 'DATASETS/VehicleID/train_test_split/train_list.txt',  256, is_train = True, normalize=False, crop_scale=1.0)
    ds_veri = VReID('DATASETS/VeRi/image_train/', 'ReID_CNN/database/VeRi_train_info.txt', 64, is_train = True, normalize=False, crop_scale=1.0)
    #tuple_df = ds.tuple_df
    cams_per_lbl = []
    imgs_per_lblcam = []
    #for lb in ds.lb_ids_uniq:
    #   cams_in_lbl = tuple_df.loc[lb].index.unique()
    #    cams_per_lbl.append(len(cams_in_lbl))
    #    for camID in cams_in_lbl:
    #        imgs_per_lblcam.append(len(tuple_df.loc[(lb, camID), 'imageName']))
    tencrop = num_crops > 1
    ds_t = AICTestData(test_img_path, test_csv, 64, normalize = False, num_crops = 5, crop_scale=1.0)
    #print(min(cams_per_lbl))
    #print(min(imgs_per_lblcam))
    n = 1786
    #n = np.random.randint(0,1000)
    print(n)
    #im, vid, cid, im_name = ds_t[n]
    im, vid, cid, im_name = ds_vid[n]
    #im, vid, cid,_, im_name = ds_veri[n]
    print(im.max())
    print(im.min())
    print('imgname: {}, vid: {}, cid: {}'.format(im_name, vid, cid))
#     ran_er = RandomErasing()
#     im = ran_er(im) 
    print('debug 368', len(im.shape), im.shape[0])
    if len(im.shape) > 3:
        for i in range(im.shape[0]):
            cv2.imshow('erased', im[i].permute(1,2,0).numpy())
            cv2.waitKey(10000)
    else:
        cv2.imshow('erased', im.permute(1,2,0).numpy())
        cv2.waitKey(10000)
# =============================================================================
# Test Veri
# =============================================================================
#    veri_info = root_dir + 'ReID_CNN/database/VeRi_train_info.txt'
#    veri_img_dir = root_dir + 'DATASETS/VeRi/image_train/'
#    veri_ds = VReID(veri_img_dir, veri_info, img_size= 224, is_train = True)
#    print(veri_ds.imgs)
#    print(min(cams_per_lbl))
#    print(min(imgs_per_lblcam))
#    n = np.random.randint(0,1000)
#    # n = 1200
#    im, vid, cid, tid, im_name = veri_ds[n]
##     print(im.shape)
#    print(im.max())
#    print(im.min())
#    print('imgname: {}, vid: {}, cid: {}, tid: {}'.format(im_name, vid, cid, tid))
#    cv2.imshow('erased', im.permute(1,2,0).numpy())
#    cv2.waitKey(0)
