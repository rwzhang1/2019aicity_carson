import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.utils.data import Dataset,DataLoader
from gen_aic_veri import AIC19_Dataset,VehicleID_Dataset
from gen_aic_veri import Get_train_DataLoader, Get_val_DataLoader
from utils.loss import Soft_TripletLoss as TripletLoss
from ReID_CNN.logger import Logger
from utils.optimizer import AdamOptimWrapper
from batch_sampler import BatchSampler

import models
import sys, argparse, os, re
from tqdm import tqdm
import numpy as np
import pandas as pd
cudnn.benchmark=True

def train_joint(args, vid_dataset, aic19_dataset, train_vid_dataloader, val_vid_dataloader,
                train_aic19_dataloader, val_aic19_dataloader,
                base_net, vid_id_net, aic19_id_net, cams_net):

    optimizer_base = AdamOptimWrapper(base_net.parameters(), lr=args.lr, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optimizer_vid = AdamOptimWrapper(vid_id_net.parameters(), lr=args.lr, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optimizer_aic19 = AdamOptimWrapper(aic19_id_net.parameters(), lr=args.lr, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optimizer_cid = AdamOptimWrapper(cams_net.parameters(),lr=args.lr, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    criterion_triplet = TripletLoss(margin=margin, batch_hard=args.batch_hard)
    criterion_ce = nn.CrossEntropyLoss()
    logger = Logger(os.path.join(args.save_model_dir,'train'))
    val_logger = Logger(os.path.join(args.save_model_dir,'val'))
    
    epoch_size = min(len(train_vid_dataloader), len(train_aic19_dataloader))
    #epoch_size = min(vid_dataset.n_train, aic19_dataset.n_train)
    for e in range(args.n_epochs):
        logger.append_epoch(e)
        pbar = tqdm(total=epoch_size,ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))
        # VeRi dataset
        epoch_loss = 0
        for i, samples in enumerate(train_vid_dataloader):
            if i==1: break
            print('samples keys: {}\n'.format(samples.keys()))
            print('samples size{}\n'.format(samples['img'].size()))
            imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                       samples['img'].size(2), 
                                       samples['img'].size(3),
                                       samples['img'].size(4))
            print('total sample in batch: {}\n'.format(imgs.shape))
            classes = samples['id'].view(-1)
            with torch.cuda.device(0):
                b_img = imgs.cuda(non_blocking=True)
                classes = classes.cuda(non_blocking=True)
            base_net.zero_grad()
            vid_id_net.zero_grad()
            #forward
            pred_feat, pred_emb = base_net(b_img)
            print('pred_feat: {}, emb: {}\n'.format(pred_feat,pred_emb))
            pred_id = vid_id_net(pred_feat)
            print('pred_id.shape: {}\n'.format(pred_id.shape))
            b_loss_triplet = criterion_triplet(pred_feat, classes)
            b_loss_triplet_embs = criterion_triplet(pred_emb, classes)
            loss_id = criterion_ce(pred_id, classes)
            loss = b_loss_triplet.mean() + b_loss_triplet_embs.mean() + 0.5*loss_id
            print('VID_triplet_loss: {}, emm_triplet_loss: {}, id_loss: {}\n'.format(b_loss_triplet.mean(), b_loss_triplet_embs.mean(), loss_id))
            epoch_loss += loss.data
#           epoch_loss += loss.data[0]
            # backward
            loss.backward()
            optimizer_base.step()
            optimizer_vid.step()

            logger.logg({'loss_vid_triplet': b_loss_triplet.data.mean(),
                         'loss_vid_triplet_max': b_loss_triplet.data.max(),
                        'loss_vid_triplet_embs': b_loss_triplet_embs.data.mean(),
                        'loss_vid_triplet_embs_max': b_loss_triplet_embs.data.max(),
                        'loss_vid_id': loss_id})

        # AIC dataset
        epoch_loss = 0
        for i, samples in enumerate(train_aic19_dataloader):
            if i==1: break
            imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                       samples['img'].size(2), 
                                       samples['img'].size(3),
                                       samples['img'].size(4))
            vid = samples['vid'].view(-1)
            cid = samples['cid'].view(-1)
            #check to see if there are multiple samples of the same cid
            #method1: s = pd.Series(cid)
            #s[s.duplicated()]))
            u, i = np.unique(cid, return_inverse=True)
            if len (u[np.bincount(i) > 1]) < 1:
                print('not enough cid samples for cid triplet')
                break
            with torch.cuda.device(0):
                b_img = imgs.cuda(non_blocking=True)
                vid = vid.cuda(non_blocking=True)
                cid = cid.cuda(non_blocking=True)
            base_net.zero_grad()
            aic19_id_net.zero_grad()
            cams_net.zero_grad()
            #forward
            pred_feat, pred_emb = base_net(b_img)
            pred_vid = aic19_id_net(pred_feat)
            pred_cid = cams_net(pred_feat)
            aicloss_triplet = criterion_triplet(pred_feat, vid)
            aicloss_triplet_cid = criterion_triplet(pred_feat, cid)
            aicloss_triplet_embs = criterion_triplet(pred_emb, vid)
            loss_vid = criterion_ce(pred_vid, vid)
            loss_cid = criterion_ce(pred_cid, cid)
            loss = aicloss_triplet.mean() + aicloss_triplet_embs.mean() + 0.5*aicloss_triplet_cid.mean() + 0.5*loss_vid + 0.2*loss_cid
            print('AIC19_triplet_loss: {}, emm_triplet_loss: {}, triplet_loss_cid: {}, vid_loss: {}, cid_loss: {}\n'.format(aicloss_triplet.mean(),\
                  aicloss_triplet_embs.mean(), aicloss_triplet_cid.mean(), loss_vid, loss_cid))
#                epoch_loss += loss.data[0]
            epoch_loss += loss.data
            # backward
            loss.backward()
            optimizer_base.step()
            optimizer_aic19.step()
            optimizer_cid.step()

            logger.logg({'loss_aic19_triplet': aicloss_triplet.data.mean(),
                        'loss_aic19_triplet_embs':aicloss_triplet_embs.data.mean(),
                        'loss_aic19_triplet_cid': aicloss_triplet_cid.data.mean(),
                        'loss_aic19_vid': loss_vid,
                        'loss_aic19_cid': loss_cid})
        pbar.update(epoch_size)
        pbar.close()
        logger.write_log()
        if e % args.save_every_n_epoch == 0:
            torch.save(base_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_base.ckpt'%(e)))
            torch.save(vid_id_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_vid_id.ckpt'%(e)))
            torch.save(aic19_id_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_aic19_id.ckpt'%(e)))
            torch.save(cams_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_cams.ckpt'%(e)))

        print('start validation')
        val_logger.append_epoch(e)
        base_net.eval()
        vid_id_net.eval()
        aic19_id_net.eval()
        cams_net.eval()

        # VehicleID
        correct = []
        for i,sample in enumerate(val_vid_dataloader):
            imgs = sample['img'].view(sample['img'].size(0)*sample['img'].size(1),
                                       sample['img'].size(2), 
                                       sample['img'].size(3),
                                       sample['img'].size(4))
            if imgs.size(0) < args.batch_size:
                break
            id = sample['id'].view(sample['id'].size(0)*sample['id'].size(1))
            
            with torch.no_grad():
                img = Variable(imgs).cuda()
                gt = Variable(id).cuda()
            #print('debugging @155 form sample index: {}, gt: {}'.format(i, gt))
            feat, _ = base_net(img)
            pred = vid_id_net(feat)
            _, pred_cls = torch.max(pred,dim=1)
            #print('predicted_cls for vid dataset: {}\n'.format(pred_cls))
            correct.append(pred_cls.data==gt.data)
#            print('correct prediction for vid dataset: {}\n'.format(correct))
        acc = torch.cat(correct).float().mean()
        print('VehicleID val acc: %.3f' % acc)
        val_logger.logg({'vid_id_acc':acc})

        correct_vid = []
        correct_cid = []
        for i,sample in enumerate(val_aic19_dataloader):
            imgs = sample['img'].view(sample['img'].size(0)*sample['img'].size(1),
                                       sample['img'].size(2), 
                                       sample['img'].size(3),
                                       sample['img'].size(4))
            vid = sample['vid'].view(sample['vid'].size(0)*sample['vid'].size(1))
            cid = sample['cid'].view(sample['cid'].size(0)*sample['cid'].size(1))
            if imgs.size(0) < args.batch_size:
                assert i > 0, 'error@182: {} \n'.format(imgs.shape)
                break
            with torch.no_grad():
                img = Variable(imgs).cuda()
                gt_vid = Variable(vid).cuda()
                gt_cid = Variable(cid).cuda()
            feat, _ = base_net(img)
            pred = aic19_id_net(feat)
            _, pred_vid = torch.max(pred,dim=1)
            pred_c = cams_net(feat)
            _, pred_cid = torch.max(pred_c,dim=1)
            #print('predicted_cls for aic19 dataset: {} and gt is: {}\n'.format(pred_vid, gt_vid))
            #print('predicted_cls for aic19 dataset: {} and gt is: {}\n'.format(pred_cid, gt_cid))
            correct_vid.append(pred_vid.data==gt_vid.data)
            correct_cid.append(pred_cid.data==gt_cid.data)
            #print('correct_vid: {}, correct_cid: {}'.format(correct_vid, correct_cid))
        acc_vid = torch.cat(correct_vid).float().mean()
        acc_cid = torch.cat(correct_cid).float().mean()
        print('AIC19 ID val acc: id: %.3f, cid: %.3f'%(acc_vid,acc_cid))
        val_logger.logg({'aic19_id_acc':acc_vid,'aic19_cid_acc':acc_cid})

        base_net.train()
        vid_id_net.train()
        aic19_id_net.train()
        cams_net.train()

if __name__ == '__main__':
    ## Parse arg
    parser = argparse.ArgumentParser(description='Train Re-ID net', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#    parser.add_argument('--vid_txt', default='/home/carson/2019AICity_carson/Track2/ReID_CNN/database/VehicleID_train_info.txt', help='txt for VeRi dataset')
#    parser.add_argument('--aic19_txt', default='/home/carson/2019AICity_carson/Track2/ReID_CNN/database/AIC19_train_info.txt', help='txt for AIC19 dataset')
    parser.add_argument('--vid_txt', default='ReID_CNN/database/VehicleID_train_info.txt', help='txt for VeRi dataset')
    parser.add_argument('--aic19_txt', default='ReID_CNN/database/AIC19_train_info.txt', help='txt for AIC19 dataset')
    parser.add_argument('--crop',type=bool,default=True,help='Whether crop the images')
    parser.add_argument('--flip',type=bool,default=True,help='Whether randomly flip the image')
    parser.add_argument('--jitter',type=int,default=0,help='Whether randomly jitter the image')
    parser.add_argument('--pretrain',type=bool,default=True,help='Whether use pretrained model')
    parser.add_argument('--lr',type=float,default=0.001,help='learning rate')
    parser.add_argument('--batch_size',type=int,default=72,help='batch size number')
    parser.add_argument('--n_epochs',type=int,default=5000,help='number of training epochs')
    parser.add_argument('--load_ckpt',default='/home/carson/2019AICity_carson/Track2/Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl',help='path to load ckpt')
    parser.add_argument('--save_model_dir',default=None,help='path to save model')
    parser.add_argument('--n_layer',type=int,default=50,help='number of Resnet layers')
    parser.add_argument('--margin',type=str,default='0',help='margin of triplet loss ("soft" or float)')
    parser.add_argument('--class_in_batch',type=int,default=18,help='# of class in a batch for triplet training')
    parser.add_argument('--image_per_class_in_batch',type=int,default=4,help='# of images of each class in a batch for triplet training')
    parser.add_argument('--batch_hard',action='store_true',help='whether to use batch_hard for triplet loss')
    parser.add_argument('--save_every_n_epoch',type=int,default=50,help='save model every n epoch')
    parser.add_argument('--class_w',type=float,help='weighting of classification loss when triplet training')
    parser.add_argument('--multi_proc',type=int, default=4,help='num of workers')
    parser.add_argument('--train_iterations', default=5000, type=int,help='Number of training iterations.')
    parser.add_argument('--decay_start_iteration', default=500, type=int, help='At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.')
    parser.add_argument('--ngpu',default = 1, type=int, help='number of gpus to use')
    args = parser.parse_args()
    margin = args.margin if args.margin=='soft' else float(args.margin)
    assert args.class_in_batch*args.image_per_class_in_batch == args.batch_size, \
           'batch_size need to equal class_in_batch*image_per_class_in_batch'

    # Get Dataset & DataLoader    
    vid_dataset = VehicleID_Dataset(args.vid_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.01,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
#    train_veri_dataloader = Get_train_DataLoader(vid_dataset,  batch_size=args.class_in_batch)
    print('vid_dataset total sample number: {}, n_train: {}, n_val: {}'.format(vid_dataset.n_id, vid_dataset.n_train, vid_dataset.n_val))
    train_vid_dataloader = Get_train_DataLoader(vid_dataset, batch_size=args.class_in_batch)
    val_vid_dataloader = Get_val_DataLoader(vid_dataset,batch_size=args.class_in_batch)

    aic19_dataset = AIC19_Dataset(args.aic19_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.3,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
    print('aic19_dataset total sample number: {}, n_train: {}, n_val: {}'.format(aic19_dataset.n_id, aic19_dataset.n_train, aic19_dataset.n_val))
    train_aic19_dataloader = Get_train_DataLoader(aic19_dataset, batch_size=args.class_in_batch)
    val_aic19_dataloader = Get_val_DataLoader(aic19_dataset, batch_size=args.class_in_batch)

    # Get Model
    base_net = models.FeatRes51Net(n_layers=args.n_layer, pretrained=args.pretrain)
    vid_id_net = models.NLayersFC(base_net.output_dim, vid_dataset.n_id)
    aic19_id_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_id)
    cams_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_cid)
    ngpu = args.ngpu
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

    if args.load_ckpt == 'from_veri':
        resume_pt = 'Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl'
        print('Resume From ckpt', resume_pt)
        
    elif args.load_ckpt != '':
        m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.resume)
        cur_epoch = int(m.groupdict()['epoch'])
        print('fine-turn from checkpoint: {}, epoch: {}'.format(args.resume, cur_epoch))
        resume_pt = args.resume
        
    if args.load_ckpt is not None:
        state_dict = torch.load(resume_pt)
        base_net.load_state_dict(state_dict, strict = False)

    with torch.cuda.device(0):
        base_net = base_net.cuda()
        vid_id_net = vid_id_net.cuda()
        aic19_id_net = aic19_id_net.cuda()
        cams_net = cams_net.cuda()
        
    #net = nn.DataParallel(net)
    if (device.type == 'cuda') and (ngpu > 1):
        base_net = nn.DataParallel(base_net, list(range(ngpu)))
        vid_id_net = nn.DataParallel(vid_id_net, list(range(ngpu)))
        aic19_id_net = nn.DataParallel(aic19_id_net, list(range(ngpu)))
        cams_net = nn.DataParallel(cams_net, list(range(ngpu)))
    
    if args.save_model_dir !=  None:
        os.system('mkdir -p %s' % os.path.join(args.save_model_dir))

    # Train
    train_joint(args, vid_dataset, aic19_dataset, train_vid_dataloader, val_vid_dataloader, train_aic19_dataloader, val_aic19_dataloader, base_net, vid_id_net, aic19_id_net, cams_net)

