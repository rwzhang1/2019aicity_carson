#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 17:21:34 2019

@author: rwzhang
"""

import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import sys,os,logging,time,itertools
import pandas as pd
import numpy as np
from sklearn.utils import shuffle
import models
from resnet50_fc import EmbedNetwork
from othernet_fc import embvgg_bn,EMBVGG
from EmbedVGGM import vggm,VGGM

from utils.loss import TripletLoss
from gen_triplet import BatchHardTripletSelector,ClusterHardTripletSelector
from batch_sampler import BatchSampler,BatchSamplerWithCam
from utils.optimizer import AdamOptimWrapper
from utils.logger import logger
from preprocess_data import AICData, convert_xml, VehicleID
import argparse, pickle,re
# construct the argument parser and parse the arguments
#Usage:  python train_joint_aic_veri_v2.py --model resnet50 --n_layer 50 --train_iterations 25000 --checkpoint_frequency 5000 --resume 'from_veri' --ngpu 3
ap = argparse.ArgumentParser(description='Train a verification model')
ap.add_argument('--label_bin', default = "smallvggnet_lb.pickle", help="path to output label binarizer")
ap.add_argument('--multi_proc', default=4, help= 'the number of workers to deploy for multiprocessing options')
ap.add_argument('--img_dir', help='img_dir', default='aic19-track2-reid/image_train/', type=str)
ap.add_argument('--img_list', help='input xml file', default='aic19-track2-reid/train_label.xml', type=str)
ap.add_argument('--model', help='model', default='resnet50', type=str)
ap.add_argument('--n_layer', help='n_layer', default=50, type=int)
ap.add_argument('--save_path', help='save_path', default='joint_aic_vid/', type=str)
ap.add_argument('--vid_img_dir', dest='vid_img_dir', help='VehicleID img_dir', default='/home/carson/2019AICity_carson/Track2/DATASETS/VehicleID/image/', type=str)
ap.add_argument('--vid_img_list', dest='vid_img_list', help='img_list', default='/home/carson/2019AICity_carson/Track2/DATASETS/VehicleID/train_test_split/train_list.txt', type=str)

ap.add_argument('--input_size', help = 'the input size to resize the original image to for training', type = int, default=224)
#orginally:--learning_rate 0.0001 -train_iterations 25000 --decay_start_iteration 15000
ap.add_argument('--learning_rate', default=0.001, type=float,help='The initial value of the learning-rate, before it kicks in.')
ap.add_argument('--train_iterations', default=25000, type=int,help='Number of training iterations.')
ap.add_argument('--decay_start_iteration', default=5000, type=int, help='At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.')
ap.add_argument('--checkpoint_frequency', default=1512, type=int, help='Set this to 0 to disable intermediate storing.')
ap.add_argument('--batch_p', default=18, type=int, help='The number P used in the PK-batches')
ap.add_argument('--batch_k', default=4, type=int, help='The numberK used in the PK-batches')
ap.add_argument('--batch_c', default=2, type=int, help='The cam number used in the PK-batches')
ap.add_argument('--num_class', default=333, type=int, help='The num_classes for classification')
ap.add_argument('--resume', default='', type=str, help='The resume point for continuous training')
#ap.add_argument('-tr', '--train_val', default = 0.1, type= float, help='train/validataion split ratio')
ap.add_argument('--ngpu',default = 1, type=int, help='number of gpus to use')
args = ap.parse_args()
# =============================================================================
ngpu = args.ngpu
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
# =============================================================================
root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
save_path = root_dir + args.save_path + args.model + '/'
#dataset = args.dataset
#img_dir = args.img_dir
#img_list = args.img_list
model = args.model
label_bin = root_dir + args.label_bin
train_xml = root_dir + args.img_list
img_dir = root_dir + args.img_dir
vid_img_dir = args.vid_img_dir
vid_img_list = args.vid_img_list
train_csv = root_dir + "aic19-track2-reid/train_label.csv"
train_feat = root_dir + "aic19-track2-reid/train_feat.csv"
train_feat_pkl = root_dir + "aic19-track2-reid/train_feat.pkl"
train_track = root_dir + "aic19-track2-reid/train_track.txt"
# =============================================================================
# =============================================================================
# construct the image generator for data augmentation
lb = pickle.loads(open(label_bin, "rb").read())
assert args.num_class == len(lb.classes_)
cam2img_df = convert_xml(train_xml)
#cam2img_df = shuffle(cam2img_df, random_state = 10086)

vid_img_dir = 'DATASETS/VehicleID/image/'
vid_img_list = 'DATASETS/VehicleID/train_test_split/train_list.txt'
# =============================================================================

# =============================================================================
def train():
    #setup
    torch.multiprocessing.set_sharing_strategy('file_system')
    if not os.path.exists(save_path): os.makedirs(save_path)
    #dataloader
    vid_selector = BatchHardTripletSelector()
    aic_selector = ClusterHardTripletSelector()
    aic_ds = AICData(img_dir, cam2img_df, img_size = args.input_size, is_train = True)
    vid_ds = VehicleID(vid_img_dir, vid_img_list, img_size= args.input_size, is_train = True)

    logger.info('Dataloading Done')
    vid_sampler = BatchSampler(vid_ds, args.batch_p, args.batch_k) #batch sample the dataset
    vid_dl = DataLoader(vid_ds, batch_sampler = vid_sampler, num_workers = args.multi_proc) # need to update the worker
    vid_diter = iter(vid_dl)
    aic_sampler = BatchSamplerWithCam(aic_ds, args.batch_p, args.batch_k, args.batch_c) #batch sample the dataset
    aic_dl = DataLoader(aic_ds, batch_sampler = aic_sampler, num_workers = args.multi_proc) # need to update the worker
    aic_diter = iter(aic_dl) #mode like yield

    #model and loss
    logger.info('setting up backnone models and loss')
    logger.info('Build Model from scratch')

    # Get Model
    base_net = models.FeatRes51Net(n_layers=args.n_layer, pretrained=True)
    vid_id_net = models.NLayersFC(base_net.output_dim, vid_ds.n_id)
    aic19_id_net = models.NLayersFC(base_net.output_dim, aic_ds.n_id)
    cams_net = models.NLayersFC(base_net.output_dim, aic_ds.n_cid)
    print('vid_ds_n_id:{},  aic_ds.n_vid:{},  aic_ds.n_cid:{}'.format( vid_ds.n_id, aic_ds.n_id, aic_ds.n_cid))
    net_dict = {'base_net':base_net,'vid_id_net':vid_id_net,'aic19_id_net':aic19_id_net,'cams_net':cams_net}

    cur_epoch = 0
    if args.resume == 'from_veri':
        resume_pt = 'Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl'
        logger.info('fine-turn from {}'.format(resume_pt))
    elif args.resume != '':
        m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.resume)
        cur_epoch = int(m.groupdict()['epoch'])
        print(cur_epoch)
        logger.info('fine-turn from checkpoint: {}, epoch: {}'.format(args.resume, cur_epoch))
        resume_pt = args.resume
    if args.resume is not None:
        state_dict = torch.load(resume_pt)
        base_net.load_state_dict(state_dict, strict = False)

    with torch.cuda.device(0):
        base_net = base_net.cuda()
        vid_id_net = vid_id_net.cuda()
        aic19_id_net = aic19_id_net.cuda()
        cams_net = cams_net.cuda()

    #net = nn.DataParallel(net)
    if (device.type == 'cuda') and (ngpu > 1):
        base_net = nn.DataParallel(base_net, list(range(ngpu)))
        vid_id_net = nn.DataParallel(vid_id_net, list(range(ngpu)))
        aic19_id_net = nn.DataParallel(aic19_id_net, list(range(ngpu)))
        cams_net = nn.DataParallel(cams_net, list(range(ngpu)))

    # define Triplet loss
    triplet_loss = TripletLoss(margin=None).cuda()
    #all get the softmax categorical cross entropy loss
    softmax_criterion = torch.nn.CrossEntropyLoss()

    #optimizer
    logger.info('create optimizer')
    optim_base = AdamOptimWrapper(base_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_aic19_id = AdamOptimWrapper(aic19_id_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_cams = AdamOptimWrapper(cams_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)
    optim_vid_id = AdamOptimWrapper(vid_id_net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)

    #train
    logger.info('start training...')
    vid_loss_trip_avg = []
    vid_loss_trip_feat_avg = []
    vid_loss_id_avg = []
    aic_loss_trip_avg = []
    aic_loss_trip_feat_avg = []
    aic_loss_vid_avg = []
    aic_loss_cid_avg = []
    visited_vid = set()
    visited_aic_vid = set()
    visited_aic_cam = set()
    count = cur_epoch
    t_start = time.time()
    while True:
#        print('[INFO] initial train to reach a decent loss')
        for n in range(args.batch_p):#vid_sampler.iter_num == args.batch_p
            try:
                vid_imgs, vid_lbs, _, _ = next(vid_diter)
            except StopIteration:
                vid_diter = iter(vid_dl)
                vid_imgs,vid_lbs,_,_ = next(vid_diter)
#            logger.info('VID_dataset: epoch: {}, labels: {}'.format(n+count, np.unique(vid_lbs)))
            for vid_lb in vid_lbs:
                visited_vid.add(vid_lb.item())
            
            with torch.cuda.device(0):
                vid_imgs = vid_imgs.cuda(non_blocking=True)
                vid_lbs = vid_lbs.cuda(non_blocking=True)
# =============================================================================
            #train vid net first
            #aply inference to generate embeddings from fc
            base_net.zero_grad()
            vid_id_net.zero_grad()
            vid_feat, vid_embds = base_net(vid_imgs)
            #with torch.cuda.device(0):
            #    vid_feat = vid_feat.cuda(non_blocking=True)
            vid_cls = vid_id_net(vid_feat)
            vid_anchor, vid_positives, vid_negatives = vid_selector(vid_embds, vid_lbs)
            print('VID_dataset: vid_feat.mean: {}, anchor.shape: {}, post: {}, neg: {}\n'.format(vid_feat.mean(), vid_anchor.mean(), vid_positives.mean(), vid_negatives.mean()))
            vid_loss_trip = triplet_loss(vid_anchor, vid_positives, vid_negatives)
            vid_feat_anchor, vid_feat_pos, vid_feat_neg = vid_selector(vid_feat, vid_lbs)
            vid_loss_trip_feat = triplet_loss(vid_feat_anchor, vid_feat_pos, vid_feat_neg)
            vid_loss_id = softmax_criterion(vid_cls, vid_lbs)
#            print('VID debug @232 vid_cls: {}, vid_lbs: {}'.format(vid_cls, vid_lbs))
            vid_loss_all = 0.5*vid_loss_id + vid_loss_trip + vid_loss_trip_feat
#            optim_base.zero_grad()
#            optim_vid_id.zero_grad()
            vid_loss_all.backward()
            optim_base.step()
            optim_vid_id.step()

            vid_loss_trip_avg.append(vid_loss_trip.detach().cpu().numpy())#calculate on the cpu
            vid_loss_trip_feat_avg.append(vid_loss_trip_feat.detach().cpu().numpy())#calculate on the cpu
            vid_loss_id_avg.append(vid_loss_id.detach().cpu().numpy())
            print('VID_dataset: epoch: {}, loss_vid_triplet: {}, vid_loss_trip_feat: {}, loss_id_vid: {}, vid_loss_all: {}\n'.format(n+count, 
                  vid_loss_trip, vid_loss_trip_feat, vid_loss_id, vid_loss_all))

            count += 1
            if count % args.batch_p == 0 and count != 0:
                vid_loss_trip_avg = sum(vid_loss_trip_avg) / len(vid_loss_trip_avg)
                vid_loss_trip_feat_avg = sum(vid_loss_trip_feat_avg) / len(vid_loss_trip_feat_avg)
                vid_loss_id_avg = sum(vid_loss_id_avg) / len(vid_loss_id_avg)
                now = time.time()
                time_interval = now - t_start
#                print('visited vid:{}'.format(visited_vid))
                logger.info('VID_dataset: epoch: {}, currently_visited_labels: {}'.format(count, len(visited_vid)))
                logger.info('VID_dataset: iter: {}, loss_all_vid: {}, base_net_lr: {:4f}, time: {:3f}\n'.format(count, vid_loss_all, optim_base.lr, time_interval))
                logger.info('VID_dataset: loss_vid_triplet: {},vid_loss_trip_feat: {}, loss_id_vid: {}'.format(vid_loss_trip_avg, vid_loss_trip_feat_avg, vid_loss_id_avg))
                vid_loss_trip_avg = []
                vid_loss_trip_feat_avg = []
                vid_loss_id_avg = []
                t_start = max(time.time(), now) # check to see if we have a pause
# =============================================================================
# train aic19 dataset

        for e in range(args.batch_p):
#            print('AIC_dataset: epoch: {}\n'.format(e+count))
            try:
                imgs, lbs, cams, _ = next(aic_diter)
            except StopIteration:
                aic_diter = iter(aic_dl)
                imgs, lbs, cams,_ = next(aic_diter)
            for lb in lbs:
                visited_aic_vid.add(lb.item())
            for cam in cams:
                visited_aic_cam.add(cam.item())

            with torch.cuda.device(0):
                b_imgs = imgs.cuda(non_blocking=True)
                lbs = lbs.cuda(non_blocking=True)
                cams = cams.cuda(non_blocking=True)
            base_net.zero_grad()
            aic19_id_net.zero_grad()
            cams_net.zero_grad()
            aic_feat, embds = base_net(b_imgs)
#            with torch.cuda.device(0):
#                aic_feat = aic_feat.cuda(non_blocking=True)
            cls_lbs = aic19_id_net(aic_feat)
            cls_cams = cams_net(aic_feat)
            #generate the triplet
#            feat_anchor, feat_pos, feat_neg = aic_selector(aic_feat, lbs, cams)
            feat_anchor, feat_pos, feat_neg = vid_selector(aic_feat, lbs)
            aic_loss_trip_feat = triplet_loss(feat_anchor, feat_pos, feat_neg)
            anchor, positives, negatives = aic_selector(embds, lbs, cams)
            aic_loss_trip = triplet_loss(anchor, positives, negatives)
            aic_loss_vid = softmax_criterion(cls_lbs, lbs)
            aic_loss_cid = softmax_criterion(cls_cams, cams)
#            loss_all = 0.5*loss_id + 0.5* loss_cid + loss_triplet
            loss_all = 0.3*aic_loss_vid + 0.3*aic_loss_cid + aic_loss_trip + aic_loss_trip_feat
#            optim_base.zero_grad()
#            optim_aic19_id.zero_grad()
            optim_cams.zero_grad()
            loss_all.backward()
#            loss_triplet.backward()
            optim_base.step()
            optim_aic19_id.step()
            optim_cams.step()

            #detach the current tensor from current graph. The result will never require gradient.
            aic_loss_trip_avg.append(aic_loss_trip.detach().cpu().numpy())#calculate on the cpu
            aic_loss_trip_feat_avg.append(aic_loss_trip_feat.detach().cpu().numpy())#calculate on the cpu
            aic_loss_vid_avg.append(aic_loss_vid.detach().cpu().numpy())
            aic_loss_cid_avg.append(aic_loss_cid.detach().cpu().numpy())
            print('AIC_dataset: epoch: {}, loss_vid_triplet: {}, vid_loss_trip_feat: {}, loss_vid: {}, loss_cid: {}, vid_loss_all: {}\n'.format(e+count, \
                  aic_loss_trip, aic_loss_trip_feat, aic_loss_vid, aic_loss_cid, loss_all))

            count += 1
            if count % args.batch_p == 0 and count != 0:
                aic_loss_trip_avg = sum(aic_loss_trip_avg) / len(aic_loss_trip_avg)
                aic_loss_trip_feat_avg = sum(aic_loss_trip_feat_avg) / len(aic_loss_trip_feat_avg)
                aic_loss_vid_avg = sum(aic_loss_vid_avg) / len(aic_loss_vid_avg)
                aic_loss_cid_avg = sum(aic_loss_cid_avg) / len(aic_loss_cid_avg)
                t_end = time.time()
                time_interval = t_end - t_start
                logger.info('AIC19_dataset: iter: {}, loss_all: {}, base_net_lr: {:4f}, time: {:3f}\n'.format(count, loss_all, optim_base.lr, time_interval))
                logger.info('AIC19_dataset: epoch: {}, currently_visited_labels: {}, currently_visited_cams:{}'.format(count, len(visited_aic_vid), len(visited_aic_cam)))
                logger.info('AIC19_dataset: loss_vid: {}, loss_cid: {}'.format(aic_loss_vid_avg, aic_loss_cid_avg))
                logger.info('AIC19_dataset: loss_triplet: {}, loss_triplet_feat: {}'.format(aic_loss_trip_avg, aic_loss_trip_feat_avg))
                aic_loss_trip_avg = []
                aic_loss_trip_feat_avg = []
                aic_loss_vid_avg = []
                aic_loss_cid_avg = []
                t_start = max(time.time(), t_end) # check to see if we have a pause
# =============================================================================
        if count % args.checkpoint_frequency == 0 and count != 0:
            logger.info('Saving the intermediate training status')
            for net_name, net in net_dict.items():
                save_model = save_path + 'ep{}_model_{}_{}.pkl'.format(count, args.model, net_name)
                print(save_model)
                ver = 2 #verbose = 2
                while os.path.exists(save_model):
                    logger.info('Model Exists, Version Update!!!')
                    save_model = save_model + '_v' + str(ver)
                    ver = ver + 1
                torch.save({'epoch': count, 'model_state_dict': net.state_dict()}, save_model)
#            torch.save(base_net.state_dict(),os.path.join(save_path,'model_%d_base.ckpt'%(count)))
#            torch.save(vid_id_net.state_dict(),os.path.join(save_path,'model_%d_vid_id.ckpt'%(count)))
#            torch.save(aic19_id_net.state_dict(),os.path.join(save_path,'model_%d_aic19_id.ckpt'%(count)))
#            torch.save(cams_net.state_dict(),os.path.join(save_path,'model_%d_cams.ckpt'%(count)))
#            torch.save(emb_net.state_dict(),os.path.join(save_path,'model_%d_emb.ckpt'%(count)))
        if count // args.train_iterations == 1 or count == args.train_iterations: break
# =============================================================================
        base_net.eval()
        vid_id_net.eval()
        aic19_id_net.eval()
        cams_net.eval()

        base_net.train()
        vid_id_net.train()
        aic19_id_net.train()
        cams_net.train()
    ## dump
    logger.info('saving trained model')
    for net_name, net in net_dict.items():
        save_model = save_path + 'ep{}_model_{}_{}.pkl'.format(count, args.model, net_name)
        print(save_model)
        ver = 2 #verbose = 2
        while os.path.exists(save_model):
            logger.info('Model Exists, Version Update!!!')
            save_model = save_model + '_v' + str(ver)
            ver = ver + 1
        torch.save({'epoch': count, 'model_state_dict': net.state_dict()}, save_model)
    logger.info('Finished at {}'.format(time.strftime("%D_%H:%M:%S")))

if __name__ == '__main__':
    train()

