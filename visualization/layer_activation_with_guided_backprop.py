"""
Created on Thu Oct 26 11:23:47 2017

@author: Utku Ozbulak - github.com/utkuozbulak
"""
import torch
from torch.nn import ReLU

from misc_functions import (get_example_params,
                            convert_to_grayscale,
                            save_gradient_images,
                            get_positive_negative_saliency)


class GuidedBackprop():
    """
       Produces gradients generated with guided back propagation from the given image
    """
    def __init__(self, model):
        self.model = model
        self.gradients = None
        self.forward_relu_outputs = []
        # Put model in evaluation mode
        self.model.eval()
        self.update_relus()
        self.hook_layers()

    def hook_layers(self):
        def hook_function(module, grad_in, grad_out):
            self.gradients = grad_in[0]
        # Register hook to the first layer
        first_layer = list(self.model.features._modules.items())[0][1]
        first_layer.register_backward_hook(hook_function)

    def update_relus(self):
        """
            Updates relu activation functions so that
                1- stores output in forward pass
                2- imputes zero for gradient values that are less than zero
        """
        def relu_backward_hook_function(module, grad_in, grad_out):
            """
            If there is a negative gradient, change it to zero
            """
            # Get last forward output
            corresponding_forward_output = self.forward_relu_outputs[-1]
            corresponding_forward_output[corresponding_forward_output > 0] = 1
            modified_grad_out = corresponding_forward_output * torch.clamp(grad_in[0], min=0.0)
            del self.forward_relu_outputs[-1]  # Remove last forward output
            return (modified_grad_out,)

        def relu_forward_hook_function(module, ten_in, ten_out):
            """
            Store results of forward pass
            """
            self.forward_relu_outputs.append(ten_out)

        # Loop through layers, hook up ReLUs
        for pos, module in self.model.features._modules.items():
            if isinstance(module, ReLU):
                module.register_backward_hook(relu_backward_hook_function)
                module.register_forward_hook(relu_forward_hook_function)

    def generate_gradients(self, input_image, target_class, cnn_layer, filter_pos):
        self.model.zero_grad()
        # Forward pass
        x = input_image
        for index, layer in enumerate(self.model.features):
            # Forward pass layer by layer
            # x is not used after this point because it is only needed to trigger
            # the forward hook function
            x = layer(x)
            # Only need to forward until the selected layer is reached
            if index == cnn_layer:
                # (forward hook function triggered)
                break
        conv_output = torch.sum(torch.abs(x[0, filter_pos]))
        # Backward pass
        conv_output.backward()
        # Convert Pytorch variable to numpy array
        # [0] to get rid of the first channel (1,3,224,224)
        gradients_as_arr = self.gradients.data.numpy()[0]
        return gradients_as_arr


if __name__ == '__main__':
    cnn_layer = 10
    filter_pos = 5
    target_example = 2  # Spider
    # (original_image, prep_img, target_class, file_name_to_export, pretrained_model) = get_example_params(target_example)
    
    import sys
    import torch.optim as optim
    import matplotlib.pyplot as plt
    import cv2
    sys.path.append('..')
    from gen_aic_veri import TripletImage_Dataset
    from gen_aic_veri import Get_train_DataLoader
    from mobilenetv2 import MobileNetV2
    
    # test the hooks 
    veri_text = '../ReID_CNN/database/VeRi_train_info.txt'
    dataset = TripletImage_Dataset(veri_text, crop=True, flip=True, jitter=True, imagenet_normalize=True, image_per_class_in_batch=1)
    train_loader = Get_train_DataLoader(dataset, batch_size=2, num_workers=1)
    print('len', len(dataset), 'classes', dataset.classes)
    print('train n_batch', len(train_loader))
    n = 123
    img_path = dataset.imgs
    file_name_to_export = img_path[img_path.rfind('/')+1:img_path.rfind('.')]

    im = dataset[n]['img']
    print('im class:', dataset[n]['class'])
    # if len(im.shape) > 3:
        # for i in range(im.shape[0]):
            # cv2.imshow('erased', im[i].permute(1,2,0).numpy())
            # cv2.waitKey(10000)
    # else:
        # cv2.imshow('erased', im.permute(1,2,0).numpy())
        # cv2.waitKey(10000)
    print('im shape: ', im.shape)
    
    model_ckpt = '../ReID_CNN/ckpt_mobile2_veritrip_lastdrop/model_mobilenetv2_1500.ckpt'
    model = MobileNetV2(num_classes=dataset.n_id)
        # original saved file with DataParallel
    state_dict = torch.load(model_ckpt)
    # create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict['model_state_dict'].items():
        name = k[7:] # remove `module.`
        new_state_dict[name] = v
    # load params
    model.load_state_dict(new_state_dict)
    
    # File export name
    file_name_to_export = file_name_to_export + '_layer' + str(cnn_layer) + '_filter' + str(filter_pos)
    # Guided backprop
    GBP = GuidedBackprop(model)
    # Get gradients
    guided_grads = GBP.generate_gradients(prep_img, target_class, cnn_layer, filter_pos)
    # Save colored gradients
    save_gradient_images(guided_grads, file_name_to_export + '_Guided_BP_color')
    # Convert to grayscale
    grayscale_guided_grads = convert_to_grayscale(guided_grads)
    # Save grayscale gradients
    save_gradient_images(grayscale_guided_grads, file_name_to_export + '_Guided_BP_gray')
    # Positive and negative saliency maps
    pos_sal, neg_sal = get_positive_negative_saliency(guided_grads)
    save_gradient_images(pos_sal, file_name_to_export + '_pos_sal')
    save_gradient_images(neg_sal, file_name_to_export + '_neg_sal')
    print('Layer Guided backprop completed')
