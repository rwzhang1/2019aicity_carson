import torch
import torch.nn as nn
from torch.autograd import Variable
import torchvision.models as models
from torchvision import transforms
import torch.nn.functional as F

class FeatureResNet(nn.Module):
    def __init__(self,n_layers=50,pretrained=True):
        super(FeatureResNet,self).__init__()
        if n_layers == 101:
            old_model= models.resnet101(pretrained=pretrained)
        elif n_layers == 50:
            old_model= models.resnet50(pretrained=pretrained)
        elif n_layers == 34:
            old_model= models.resnet34(pretrained=pretrained)
        elif n_layers == 18:
            old_model= models.resnet18(pretrained=pretrained)
        else:
            raise NotImplementedError('resnet%s is not found'%(n_layers))

        for name,modules in old_model._modules.items():
            if name.find('fc') == -1:
                self.add_module(name,modules)
        self.output_dim = old_model.fc.in_features
        self.pretrained = pretrained
    def forward(self,x):
        for name,module in self._modules.items():
            x = module(x)
#            x = nn.parallel.data_parallel(module, x)
        return x.view(x.size(0), -1)

class ResNet(nn.Module):
    def __init__(self,n_id,n_layers=50,pretrained=True):
        super(ResNet,self).__init__()
        if n_layers == 101:
            old_model= models.resnet101(pretrained=pretrained)
        elif n_layers == 50:
            old_model= models.resnet50(pretrained=pretrained)
        elif n_layers == 34:
            old_model= models.resnet34(pretrained=pretrained)
        elif n_layers == 18:
            old_model= models.resnet18(pretrained=pretrained)
        else:
            raise NotImplementedError('resnet%s is not found'%(n_layers))

        for name,modules in old_model._modules.items():
            self.add_module(name,modules)
        self.fc = nn.Linear(self.fc.in_features,n_id)
        #########
        self.pretrained = pretrained
    def forward(self,x):
        for name,module in self._modules.items():
            if name != 'fc':
                x = module(x)
        out = self.fc(x.view(x.size(0),-1))
        return out, x.view(x.size(0), -1)

class FeatRes51Net(nn.Module):
    def __init__(self, stage5=False, num_classes=333, emb_dims=128, n_layers=50, pretrained=True, bn_last=False, with_top=True):
        super(FeatRes51Net,self).__init__()
        if n_layers == 101:
            old_model= models.resnet101(pretrained=pretrained)
        elif n_layers == 50:
            old_model= models.resnet50(pretrained=pretrained)
        elif n_layers == 34:
            old_model= models.resnet34(pretrained=pretrained)
        elif n_layers == 18:
            old_model= models.resnet18(pretrained=pretrained)
        else:
            raise NotImplementedError('resnet%s is not found'%(n_layers))

        for name,modules in old_model._modules.items():
            if name != 'fc' and name != 'avgpool':
                self.add_module(name,modules)
        self.stage5 = stage5
        self.with_top = with_top
        self.bn_last = bn_last
        if stage5:
            #self.layer5 = create_layer(2048, 512, 3, stride=1)
            self.layer5 = CrossOver(2048, 256)
            self.output_dim = self.layer5.output_dim
        else:
            self.output_dim =  old_model.fc.in_features
        if bn_last:
            self.fc_head = DenseNormReLU(in_feats=self.output_dim, out_feats=1024)
        else:
            self.dropout = nn.Dropout(0.001)
            self.fc_head = nn.Linear(in_features=self.output_dim, out_features=1024)
        if with_top:
            self.fc = nn.Linear(in_features=self.output_dim, out_features=num_classes)
        self.embed = nn.Linear(in_features=1024, out_features=emb_dims)
        
        for el in self.fc_head.children():
            if isinstance(el, nn.Linear):
                nn.init.kaiming_normal_(el.weight, a=1)
                nn.init.constant_(el.bias, 0)

        nn.init.kaiming_normal_(self.embed.weight, a=1)
        #  nn.init.xavier_normal_(self.embed.weight, gain=1)
        nn.init.constant_(self.embed.bias, 0)
        
        #########
        self.pretrained = pretrained
    def forward(self,x):
        for name,module in self._modules.items():
            if name.find('fc') == -1 and name.find('fc_head') == -1 and name.find('embed') == -1:
                x = module(x)
        x = F.avg_pool2d(x, x.size()[2:])
        x = x.contiguous().view(-1, self.output_dim)#2048 features from original resnet
        if not self.bn_last:
            x = self.dropout(x)
        x_hidden = self.fc_head(x)
        embs = self.embed(x_hidden)
        if self.with_top:
            fc = self.fc(x)
            return fc, x, embs
        else:
            return x, embs
            
class CrossOver(nn.Module):
    def __init__(self, in_chan, mid_chan, stride=1, stride_at_1x1=False, *args, **kwargs):
        super(CrossOver, self).__init__(*args, **kwargs)

        out_chan = 4 * mid_chan
        self.conv1_up = nn.Conv2d(in_chan, mid_chan, kernel_size=(1, 7), stride=1, bias=False)
        self.bn1_up = nn.BatchNorm2d(mid_chan)
        self.conv2_up = nn.Conv2d(mid_chan, out_chan, kernel_size=(7, 1), stride=1, bias=False)
        self.bn2_up = nn.BatchNorm2d(out_chan)

        self.conv1_down = nn.Conv2d(in_chan, mid_chan, kernel_size=(7, 1), stride=1, bias=False)
        self.bn1_down = nn.BatchNorm2d(mid_chan)
        self.conv2_down = nn.Conv2d(mid_chan, out_chan, kernel_size=(1, 7), stride=1, bias=False)
        self.bn2_down = nn.BatchNorm2d(out_chan) 
        
        self.relu = nn.ReLU(inplace=True)
        self.output_dim = out_chan


    def forward(self, x):
        print('input shape to crossover:', x.shape)
        out_up = self.conv1_up(x)
        print('out_up conv1 output:', out_up.shape)
        out_up = self.bn1_up(out_up)
        print('out_up bn1 output:', out_up.shape)
        out_up = self.relu(out_up)
        out_up = self.conv2_up(out_up)
        print('out_up conv2 output:', out_up.shape)
        out_up = self.bn2_up(out_up)
        print('out_up bn2 output:', out_up.shape)
        
        out_down = self.conv1_down(x)
        print('out_down conv1 output:', out_down.shape)
        out_down = self.bn1_down(out_down)
        print('out_down bn1 output:', out_down.shape)
        out_down = self.relu(out_down)
        out_down = self.conv2_down(out_down)
        print('out_down conv2 output:', out_down.shape)
        out_down = self.bn2_down(out_down)
        print('out_down bn2 output:', out_down.shape)
        
        out = out_up + out_down
        out = self.relu(out)

        return out
    
class Bottleneck(nn.Module):
    def __init__(self, in_chan, mid_chan, stride=1, stride_at_1x1=False, *args, **kwargs):
        super(Bottleneck, self).__init__(*args, **kwargs)

        stride1x1, stride3x3 = (stride, 1) if stride_at_1x1 else (1, stride)

        out_chan = 4 * mid_chan
        self.conv1 = nn.Conv2d(in_chan, mid_chan, kernel_size=1, stride=stride1x1,
                bias=False)
        self.bn1 = nn.BatchNorm2d(mid_chan)
        self.conv2 = nn.Conv2d(mid_chan, mid_chan, kernel_size=3, stride=stride3x3,
                padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(mid_chan)
        self.conv3 = nn.Conv2d(mid_chan, out_chan, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(out_chan)
        self.relu = nn.ReLU(inplace=True)

        self.downsample = None
        if in_chan != out_chan or stride != 1:
            self.downsample = nn.Sequential(
                nn.Conv2d(in_chan, out_chan, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(out_chan))

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample == None:
            residual = x
        else:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out
    
def create_layer(in_chan, mid_chan, b_num, stride):
    out_chan = mid_chan * 4
    blocks = [Bottleneck(in_chan, mid_chan, stride=stride),]
    for i in range(1, b_num):
        blocks.append(Bottleneck(out_chan, mid_chan, stride=1))
    return nn.Sequential(*blocks)

class DenseNormReLU(nn.Module):
    def __init__(self, in_features, out_feats, *args, **kwargs):
        super(DenseNormReLU, self).__init__(*args, **kwargs)
        self.dense = nn.Linear(in_features = in_features, out_features = out_features)
        self.bn = nn.BatchNorm1d(out_features)
        self.relu = nn.ReLU(inplace = True)
        self.in_features = in_features
        self.out_features = out_features

    def forward(self, x):
        x = self.dense(x)
        x = self.bn(x)
        x = self.relu(x)
        return x

class NLayersFC(nn.Module):
    def __init__(self, in_dim, out_dim, hidden_dim=1, n_layers=0):
        super(NLayersFC, self).__init__()
        if n_layers == 0:
            model = [nn.Linear(in_dim, out_dim)]
        else:
            model = []
            model += [nn.Linear(in_dim, hidden_dim),
                      nn.ReLU(True)]
            for i in range(n_layers-1):
                model += [nn.Linear(hidden_dim, hidden_dim),
                          nn.ReLU(True)]
            model += [nn.Linear(hidden_dim, out_dim)]
        self.model = nn.Sequential(*model)

    def forward(self, x):
        return self.model(x)

class ICT_ResNet(nn.Module):
    def __init__(self,n_id,n_color,n_type,n_layers=50,pretrained=True):
        super(ICT_ResNet,self).__init__()
        if n_layers == 101:
            old_model= models.resnet101(pretrained=pretrained)
        elif n_layers == 50:
            old_model= models.resnet50(pretrained=pretrained)
        elif n_layers == 34:
            old_model= models.resnet34(pretrained=pretrained)
        elif n_layers == 18:
            old_model= models.resnet18(pretrained=pretrained)
        else:
            raise NotImplementedError('resnet%s is not found'%(n_layers))

        for name,modules in old_model._modules.items():
            self.add_module(name,modules)
        self.fc = nn.Linear(self.fc.in_features,n_id)
        self.fc_c = nn.Linear(self.fc.in_features,n_color)
        self.fc_t = nn.Linear(self.fc.in_features,n_type)
        #########
        self.pretrained = pretrained
    def forward(self,x):
        for name,module in self._modules.items():
            if name.find('fc')==-1:
                x = module(x)
        x = x.view(x.size(0),-1)
        x_i = self.fc(x)
        x_c = self.fc_c(x)
        x_t = self.fc_t(x)
        return x_i,x_c,x_t

class TripletNet(nn.Module):
    def __init__(self, net):
        super(TripletNet, self).__init__()
        self.net = net

    def forward(self, x, y, z):
        pred_x, feat_x = self.net(x)
        pred_y, feat_y = self.net(y)
        pred_z, feat_z = self.net(z)
        dist_pos = F.pairwise_distance(feat_x, feat_y, 2)
        dist_neg = F.pairwise_distance(feat_x, feat_z, 2)
        return dist_pos, dist_neg, pred_x, pred_y, pred_z

if __name__ == '__main__':
    netM = FeatRes51Net(stage5=False, n_layers=50, pretrained=True).cuda()
    print(netM)
    output = netM(Variable(torch.ones(32,3,256,256).cuda()/2.))
    print(output[0].size())
    print(output[1].size())
