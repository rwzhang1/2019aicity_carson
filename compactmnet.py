#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 15:58:42 2019

@author: rwzhang
New implementation for Compact-MNet
"""
import torch
import torch.nn as nn
from torch.nn.modules.utils import _single, _pair, _triple
import math
import torch.nn.functional as F
import torchvision.transforms as transforms
__all__ = ['compact_mnet']

def weight_decay_config(value=1e-4, log=True):
    def regularize_layer(m):
        non_depthwise_conv = isinstance(m, nn.Conv2d) \
            and m.groups != m.in_channels
        return isinstance(m, nn.Linear) or non_depthwise_conv

    return {'name': 'WeightDecay',
            'value': value,
            'log': log,
            'filter': {'parameter_name': lambda n: not n.endswith('bias'),
                       'module': regularize_layer}
            }
            
class CompactMNet(nn.Module):
    def __init__(self, num_classes=1000, dims = 128, with_top = True, regime=None):
        super(CompactMNet, self).__init__()

        def conv_bn(inp, oup, stride):
            return nn.Sequential(
                nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
                nn.BatchNorm2d(oup),
                nn.ReLU(inplace=True)
            )

        def conv_dw(inp, oup, stride):
            return nn.Sequential(
                nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
                nn.BatchNorm2d(inp),
                nn.ReLU(inplace=True),
    
                nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
                nn.ReLU(inplace=True),
            )

        def conv_dw_nobn(inp, oup, stride):
            '''
            Removing depth wise bn layer from original conv_dw module
            '''
            return nn.Sequential(
                nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
                #nn.BatchNorm2d(inp),
                #nn.ReLU(inplace=True),
    
                nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
                nn.ReLU(inplace=True),
            )

        def conv_dw_dropbn(inp, oup, stride):
            '''
            removing depth wise bn, added one drop out layer before bn
            '''
            return nn.Sequential(
                nn.Conv2d(inp, inp, 3, stride, 1, groups=inp, bias=False),
                #nn.BatchNorm2d(inp),
                #nn.ReLU(inplace=True),
    
                nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
                nn.Dropout(0.001),
                nn.BatchNorm2d(oup),
                nn.ReLU(inplace=True),
            )
        self.features = nn.Sequential(
            conv_bn(  3,  32, 2), 
            conv_dw_nobn( 32,  64, 2), # original: conv_dw( 32,  64, 1),			
            conv_dw_nobn( 64, 128, 2),
            conv_dw_nobn(128, 128, 1),
            conv_dw_nobn(128, 256, 2),
            conv_dw_nobn(256, 256, 1),
            conv_dw_nobn(256, 512, 2),
            conv_dw_nobn(512, 512, 1),
            conv_dw_nobn(512, 512, 1),
            conv_dw_nobn(512, 512, 1),
            conv_dw_nobn(512, 512, 1),
            conv_dw_nobn(512, 512, 1),
            conv_dw_dropbn(512, 1024, 1), # original: conv_dw(512, 1024, 2),
            #conv_dw(1024, 1024, 1),
        )

        self.avg_pool = nn.AdaptiveAvgPool2d(1) # original: nn.AvgPool2d(7)
        output_channel = 1024
        self.with_top = with_top
        if with_top:
            self.classifier = nn.Linear(output_channel, num_classes)
        self.embed = nn.Linear(output_channel, dims)
        self.output_dim = output_channel
        self._initialize_weights()
        nn.init.orthogonal_(self.embed.weight, gain=1)
        nn.init.constant_(self.embed.bias, 0)
        
        if regime == 'small':
            scale_lr = 4
            self.regime = [
                {'epoch': 0, 'optimizer': 'SGD',
                 'momentum': 0.9, 'lr': scale_lr * 1e-1, 'regularizer': weight_decay_config(1e-4)},
                {'epoch': 30, 'lr': scale_lr * 1e-2},
                {'epoch': 60, 'lr': scale_lr * 1e-3},
                {'epoch': 80, 'lr': scale_lr * 1e-4}
            ]
            self.data_regime = [
                {'epoch': 0, 'input_size': 128, 'batch_size': 512},
                {'epoch': 80, 'input_size': 224, 'batch_size': 128},
            ]
            self.data_eval_regime = [
                {'epoch': 0, 'input_size': 128, 'batch_size': 1024},
                {'epoch': 80, 'input_size': 224, 'batch_size': 512},
            ]
        else:
            self.regime = [
                {'epoch': 0, 'optimizer': 'Adam', 'lr': 5e-4,
                 'momentum': 0.9, 'regularizer': weight_decay_config(1e-3)},
                {'epoch': 160, 'lr': 4e-4},
                {'epoch': 300, 'lr': 2e-4},
                {'epoch': 500, 'lr': 8e-5},
                {'epoch': 680, 'lr': 5e-5, 'regularizer': weight_decay_config(5e-5)},
                {'epoch': 880, 'lr': 2e-5},
                {'epoch': 1080, 'lr': 8e-6},
                {'epoch': 1280, 'lr': 5e-6},                
            ]

            
    def forward(self, x):
        # print('x shape before feature', x.shape)
        x = self.features(x)
        # print('x shape after feature', x.shape)
        x = self.avg_pool(x)
        # print('x shape after avgpool ', x.shape)
        x = x.view(x.size(0), -1)
        # print('x shape before emb', x.shape)
        embs = self.embed(x)
        #embs = F.normalize(embs, p=2, dim=1)
        if self.with_top:
            # 04/11 change to conv2d_dropbn
            # x = self.dropout(x)
            # print('x shape after dropout ', x.shape)
            fc = self.classifier(x)
            return fc, x, embs
        else:
            return x, embs
            

    def _initialize_weights(self):
        
        def truncated_normal_(tensor, mean=0, std=1):
            size = tensor.shape
            tmp = tensor.new_empty(size + (4,)).normal_()
            valid = (tmp < 2) & (tmp > -2)
            ind = valid.max(-1, keepdim=True)[1]
            tensor.data.copy_(tmp.gather(-1, ind).squeeze(-1))
            tensor.data.mul_(std).add_(mean)##
            
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                truncated_normal_(m.weight, std=0.09)
                #updates made on 04/09                
                #print('initialize conv2d with truncated_normal', m.weight.data)
                #n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                #m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                # update 04/09
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight, a=1)
                nn.init.constant_(m.bias, 0)
                # update 04/09
                #n = m.weight.size(1)
                #m.weight.data.normal_(0, 0.01)
                #m.bias.data.zero_()

if __name__ == '__main__':
    model = CompactMNet()

    print(getattr(model, 'regime'))
    in_ten = torch.randn(32, 3, 224, 224)
    feat, embs, output = model(in_ten)
    print(feat[0].size())
    print(embs[0].size())
    print(feat[2].size())