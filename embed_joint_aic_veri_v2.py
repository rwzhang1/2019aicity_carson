#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 16:32:53 2019

@author: rwzhang
"""

import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import sys,os,logging,time,itertools
import pandas as pd
from sklearn.utils import shuffle

from preprocess_data import AICTestData, convert_xml, AICQueryData
import numpy as np
import argparse, pickle, re
import models
import cv2

torch.multiprocessing.set_sharing_strategy('file_system')
#Help: python embed_joint_aic_veri_v2.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet50' --num_class 333 --img_size 224 --resume 'from_veri'
#Help: python embed_joint_aic_veri_v2.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet50' --num_class 333 --img_size 224 --resume log/resnet50/ep25000_model_soft_resnet.pkl
#Help: python embed_joint_aic_veri_v2.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'vggm' --num_class 333 --img_size 221 --resume log/vggm/ep25000_model_vggm.pkl
#Help: python embed_joint_aic_veri_v2.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'vgg16_bn' --num_class 333 --img_size 224 --resume log/vgg16_bn/ep25000_model_vgg16_bn.pkl
#Query: python embed_joint_aic_veri_v2.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet50' --num_class 333 --img_size 224 --resume 'from_veri'
def parse_args():
    ap = argparse.ArgumentParser('Running EMbeddings on the Test/Query set')
    ap.add_argument('--mode', help='test or query', required=True, type=str)
    ap.add_argument('--img_dir', help='img_dir', default='aic19-track2-reid/image_test/', type=str)
    ap.add_argument('--img_list', help=' the raw images list for test', default='aic19-track2-reid/test_track_id.txt', type=str)
    ap.add_argument('--model', help='model', default='featnet', type=str)
    ap.add_argument('--n_layer', help='n_layer', default=50, type=int)
    ap.add_argument('--save_path', help='path to save the embeddings', default='joint_aic_vid/', type=str)
    ap.add_argument('--resume', default='joint_aic_vid/2018_ntu/ep880_model_base.ckpt', type=str, help='The trained model to be loaded for eval')
    ap.add_argument('--num_class', default=333, type=int, help='The numberK used in the PK-batches')
    ap.add_argument('--img_size', default=224, type=int, help='The imageSize used')
    ap.add_argument('--batch_size', default=24, type=int, help='The batch size used')
    ap.add_argument('--ngpu', default=1, type=int, help='number of GPUs to be engaged')

    return ap.parse_args()

def embed(args):
    #path init, load arguments
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    emb_path = root_dir + args.save_path + 'emb{}_{}{}'.format(args.mode, args.model, args.n_layer) + '/'
    img_list = root_dir + args.img_list
    img_dir = root_dir + args.img_dir
    emb_name = 'emb_'+ args.model +'.pkl'
    ngpu = args.ngpu
    input_size = args.img_size
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

    ## logging
    if not os.path.exists(emb_path): os.makedirs(emb_path)
    FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
    logger = logging.getLogger(__name__)

    ## restore model
    logger.info('restoring model')
    if args.model == 'featnet':
        # this only contains the feature 
        net = models.FeatureResNet(n_layers=args.n_layer, pretrained=True)
    else:
        net = models.FeatRes51Net(n_layers=args.n_layer, pretrained=True)

    cur_epoch = 0
    if args.resume == 'from_veri':
        resume_pt = 'Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl'
        logger.info('fine-tune from {}'.format(resume_pt))
    elif args.resume != '':
        m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.resume)
        cur_epoch = int(m.groupdict()['epoch'])
        print(cur_epoch)
        emb_name = 'tencrop_emb{}_{}_{}{}.pkl'.format(args.mode, cur_epoch, args.model, args.n_layer)
        logger.info('fine-tune from checkpoint: {}, epoch: {}'.format(args.resume, cur_epoch))
        resume_pt = args.resume
        
    with torch.cuda.device(0):
        net = net.cuda()
    #net = nn.DataParallel(net)
    if (device.type == 'cuda') and (ngpu > 1): net = nn.DataParallel(net, list(range(ngpu)))
    if args.resume is not None:
        state_dict = torch.load(resume_pt)
        net.load_state_dict(state_dict, strict = False)

    net.eval()

    ## load gallery dataset
    if args.mode == 'test':
    #    ds = AICTestData(img_dir, img_list, img_size=args.img_size, is_train = False)
        ds = AICTestData(img_dir, img_list, img_size=input_size, is_train = False, tencrop = False)
    else:
        ds = AICQueryData(img_dir, img_size=input_size, is_train = False, tencrop = False)

    print('labls: {}\n'.format(len(ds)))
    ## embedding samples
    logger.info('start embedding')
    features = []
    embeddings = []
    label_ids = []
    label_cams = []
    img_names = []
    batch_img = []
    visited_images=set()
    for it in range(len(ds)):
        print('\r=======>  processing iter {} / {}\n'.format(it, len(ds)),
                end = '', flush = True)
        if args.mode == 'test':
            im, lb_id, lb_cam, img_name = ds[it]
            visited_images.add(img_name)
            label_ids.append(lb_id)
            label_cams.append(lb_cam)
            img_names.append(img_name)
            batch_img.append(im)
        else:
            im, img_name = ds[it]
            img_names.append(img_name)
            batch_img.append(im)
        if (it+1)%args.batch_size == 0:
            print('total number of images in batch {} is: {}'.format(it, len(batch_img)))
            with torch.cuda.device(0):
                batch_img = torch.stack(batch_img).cuda(non_blocking=True)
            if args.model == 'featnet':
                feat = net(batch_img)
                features.append(feat.detach().cpu().numpy())
            else:
                feat, embd = net(batch_img)
                features.append(feat.detach().cpu().numpy())
                embeddings.append(embd.detach().cpu().numpy())
                logger.info('total images visited when finished iteration {} is: {}'.format(it, len(visited_images)))
            batch_img = []
    if len(batch_img) > 0:
        print('total number of images in batch {} is: {}'.format(it, len(batch_img)))
        with torch.cuda.device(0):
            batch_img = torch.stack(batch_img).cuda(non_blocking=True)
        if args.model == 'featnet':
            feat = net(batch_img)
            print('normed feat: {}\n'.format(feat.shape))
            features.append(feat.detach().cpu().numpy())
        else:
            feat, embd = net(batch_img)
            features.append(feat.detach().cpu().numpy())
            embeddings.append(embd.detach().cpu().numpy())        
        batch_img = []
    logger.info('total images visited when finished iteration {} is: {}'.format(it, len(visited_images)))
    print(len(embeddings))
    print('  ...   completed')
    print('features: {}, {}\n'.format(len(features), len(features[0])))
    features = np.vstack(features)
#    features = torch.cat(features, dim = 0)
    img_names = np.hstack(img_names)
    print('features: {}, {}\n'.format(len(features), len(features[0])))
    
    ## dump results
    logger.info('dump embeddings')
    if args.mode == 'test':
        label_ids = np.hstack(label_ids)
        label_cams = np.hstack(label_cams)
        print('total labels: {}'.format(len(label_ids)))
        if args.model == 'featnet':
            embd_res = {'features': features, 'label_ids': label_ids, 'label_cams': label_cams, 'img_names':img_names}
        else:
            embeddings = np.vstack(embeddings)
#            embeddings = torch.cat(embeddings, dim = 0)
            embd_res = {'features': features, 'embeddings': embeddings, 'label_ids': label_ids, 'label_cams': label_cams, 'img_names':img_names}
    else:
        if args.model == 'featnet':
            embd_res = {'features': features, 'img_names':img_names}
        else:
            embeddings = np.vstack(embeddings)
#            embeddings = torch.cat(embeddings, dim = 0)
            embd_res = {'features': features, 'embeddings': embeddings, 'img_names':img_names}
            
    emb_file = emb_path + '/' + emb_name
    with open(emb_file, 'wb') as fw:
        pickle.dump(embd_res, fw)
    logger.info('embedding finished')

if __name__ == '__main__':
    args = parse_args()
    embed(args)
