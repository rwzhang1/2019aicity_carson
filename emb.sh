# python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ \
# --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 \
# --img_size 224 --nettype 'embnet'  --base_ckpt 'joint_aic_vid/resnet50/ep39_model_resnet50_base_net.pkl' \
# --aic19_id_ckpt 'joint_aic_vid/resnet50/ep39_model_resnet50_aic19_id_net.pkl' \
# --color_base 'joint_aic_vid/resnet18/ep50_model_resnet18_base_net.pkl' \
# --color_ckpt 'joint_aic_vid/resnet18/ep50_model_resnet18_veri_color_net.pkl' \
# --n_color 10 --gencolor True --tencrop True --batch_size 4 
# 
# python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ \
# --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet'  \
# --base_ckpt 'joint_aic_vid/resnet50/ep39_model_resnet50_base_net.pkl' \
# --aic19_id_ckpt 'joint_aic_vid/resnet50/ep39_model_resnet50_aic19_id_net.pkl' \
# --color_base 'joint_aic_vid/resnet18/ep50_model_resnet18_base_net.pkl' \
# --color_ckpt 'joint_aic_vid/resnet18/ep50_model_resnet18_veri_color_net.pkl' \
# --n_color 10 --gencolor True --tencrop True  --batch_size 4
# 
# python gen_ranking.py --feat_opt 'feat' \
# --test_embs 'joint_aic_vid/embtest_resnet50/tencrop_embtest_39_color50_embnet.pkl' \
# --query_embs 'joint_aic_vid/embquery_resnet50/tencrop_embquery_39_color50_embnet.pkl'

#python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet'  --base_ckpt 'joint_aic_vid/resnet50/ep25_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50/ep25_model_resnet50_aic19_id_net.pkl' --color_base 'joint_aic_vid/joint_aic_ckpt/ep150_model_resnet18_base.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep150_model_resnet18_color.ckpt' --gencolor True
#python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet'  --base_ckpt 'joint_aic_vid/resnet50/ep25_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50/ep25_model_resnet50_aic19_id_net.pkl' --color_base 'joint_aic_vid/joint_aic_ckpt/ep150_model_resnet18_base.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep150_model_resnet18_color.ckpt' --gencolor True
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_25_color150_embnet.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_25_color150_embnet.pkl'
 
#python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet'  --base_ckpt 'joint_aic_vid/resnet50/ep7560_model_resnet50_base_net.pkl'  --color_base '' --color_ckpt 'joint_aic_vid/resnet50/ep7560_model_resnet50_veri_color_net.pkl' --gencolor True
#python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50/ep7560_model_resnet50_base_net.pkl'  --color_base '' --color_ckpt 'joint_aic_vid/resnet50/ep7560_model_resnet50_veri_color_net.pkl' --gencolor True
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_7560_color7560_embnet.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_7560_color7560_embnet.pkl' --gencolor True 
####5
# python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl' --color_base 'joint_aic_vid/joint_aic_ckpt/ep750_model_base_resnet50.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep750_model_color_resnet50.ckpt' --gencolor True
# python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl'  --color_base 'joint_aic_vid/joint_aic_ckpt/ep750_model_base_resnet50.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep750_model_color_resnet50.ckpt' --gencolor True
# python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_45000_color750_embnet.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_45000_color750_embnet.pkl' --gencolor True 
####4
#python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl' --color_base 'joint_aic_vid/joint_aic_ckpt/ep850_model_base_resnet34.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep850_model_color_resnet34.ckpt' --gencolor True
#python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl'  --color_base 'joint_aic_vid/joint_aic_ckpt/ep850_model_base_resnet34.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep850_model_color_resnet34.ckpt' --gencolor True
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_45000_color850_embnet.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_45000_color850_embnet.pkl' --gencolor True 
####3
#python embed_joint_aic_veri_tencrop.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl' 
#python embed_joint_aic_veri_tencrop.py --mode 'test' --img_list aic19-track2-reid/test_track_id.txt --img_dir aic19-track2-reid/image_test/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl' 
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/tencrop_embtest_45000_resnet50.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/tencrop_embquery_45000_resnet50.pkl' --model 'resnet'
####2
#python embed_joint_aic_veri_v2.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl' 
#python embed_joint_aic_veri_v2.py --mode 'test' --img_list aic19-track2-reid/test_track_id.txt --img_dir aic19-track2-reid/image_test/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/resnet50/ep45000_model_resnet50_base_net.pkl' 
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_45000_resnet50.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_45000_resnet50.pkl' --model 'resnet'
####1
#python embed_joint_aic_veri_v2.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/resnet50/ep24192_model_resnet50_base_net.pkl' 
#python embed_joint_aic_veri_v2.py --mode 'test' --img_list aic19-track2-reid/test_track_id.txt --img_dir aic19-track2-reid/image_test/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/resnet50/ep24192_model_resnet50_base_net.pkl' 
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_24192_resnet50.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_24192_resnet50.pkl' --model 'resnet'

####0
#python embed_joint_aic_veri_v2.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/joint_aic_ckpt/ep4650_model_base.ckpt' 
#python embed_joint_aic_veri_v2.py --mode 'test' --img_list aic19-track2-reid/test_track_id.txt --img_dir aic19-track2-reid/image_test/ --model 'resnet' --num_class 333 --img_size 224 --resume 'joint_aic_vid/joint_aic_ckpt/ep4650_model_base.ckpt' 
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/embtest_4650_resnet50.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/embquery_4650_resnet50.pkl' --model 'resnet'

#0312gold
#python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --nettype 'embnet'  --base_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_aic19_id_net.pkl' --img_size 256 --batch_size 1 --num_crops 10 --crop_scale 0.6 --duplicates 2
#python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_aic19_id_net.pkl' --img_size 256 --batch_size 1 --num_crops 10 --crop_scale 0.7 --duplicates 1
#python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/10crop_embtest_79_embnet.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/10crop_embquery_79_embnet.pkl' --color_weight 0.0 --disttype 'pdist' --cmc_rank 100

#0312gold_with_ReID_CNN
# python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --nettype 'embnet'  --base_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_aic19_id_net.pkl' --color_base 'ReID_CNN/ckpt_aicverividcomp_resnet34/ep110_model_resnet34_base_net.pkl' --color_ckpt 'ReID_CNN/ckpt_aicverividcomp_resnet34/ep110_model_resnet34_veri_color_net.pkl' --img_size 256 --batch_size 1 --num_crops 10 --crop_scale 0.6 --duplicates 2 --gencolor True
# python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --nettype 'embnet' --base_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50_crop60/ep79_model_resnet50_aic19_id_net.pkl' --color_base 'ReID_CNN/ckpt_aicverividcomp_resnet34/ep110_model_resnet34_base_net.pkl' --color_ckpt 'ReID_CNN/ckpt_aicverividcomp_resnet34/ep110_model_resnet34_veri_color_net.pkl' --img_size 256 --batch_size 1 --num_crops 10 --crop_scale 0.7 --duplicates 1 --gencolor True
# python gen_ranking.py --feat_opt 'feat' --test_embs 'joint_aic_vid/embtest_resnet50/10crop_embtest_79_embnet.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/10crop_embquery_79_embnet.pkl' --color_weight 0.3 --disttype 'pdist' --cmc_rank 100 --gencolor True

#0316 resnet50 no bn(terrible)
# python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 50 --num_class 333 --nettype 'embnet_nobn'  --base_ckpt 'joint_aic_vid/resnet50_nobn/ep59_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50_nobn/ep59_model_resnet50_aic19_id_net.pkl' --img_size 256 --batch_size 1 --num_crops 10 --crop_scale 0.6 --duplicates 2
# python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 50 --num_class 333 --nettype 'embnet_nobn' --base_ckpt 'joint_aic_vid/resnet50_nobn/ep59_model_resnet50_base_net.pkl' --aic19_id_ckpt 'joint_aic_vid/resnet50_nobn/ep59_model_resnet50_aic19_id_net.pkl' --img_size 256 --batch_size 1 --num_crops 10 --crop_scale 0.7 --duplicates 1
#python gen_ranking.py --feat_opt 'emb' --test_embs 'joint_aic_vid/embtest_resnet50/10crop_embtest_59_embnet_nobn.pkl' --query_embs 'joint_aic_vid/embquery_resnet50/10crop_embquery_59_embnet_nobn.pkl' --color_weight 0.3 --disttype 'pdist' --cmc_rank 100