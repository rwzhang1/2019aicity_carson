#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 13:58:32 2019

@author: rwzhang
"""

"""
VGGM network with torch
"""
import torch
import torch.nn as nn
from torch.autograd import Variable
#from torch.legacy import nn as nnl
import torch.utils.model_zoo as model_zoo

__all__ = ['vggm']

pretrained_settings = {
    'vggm': {
        'imagenet': {
            'url': 'http://data.lip6.fr/cadene/pretrainedmodels/vggm-786f2434.pth',
            'pretrained_model': 'log/vggm/vggm-786f2434.pth',
            'input_space': 'BGR',
            'input_size': [3, 221, 221],
            'input_range': [0, 255],
            'mean': [123.68, 116.779, 103.939],
            'std': [1, 1, 1],
#            'num_classes': 333
            'num_classes': 1000
        }
    }
}

class SpatialCrossMapLRN(nn.Module):
    def __init__(self, local_size=1, alpha=1.0, beta=0.75, k=1, ACROSS_CHANNELS=True):
        super(SpatialCrossMapLRN, self).__init__()
        self.ACROSS_CHANNELS = ACROSS_CHANNELS
        if ACROSS_CHANNELS:
            self.average=nn.AvgPool3d(kernel_size=(local_size, 1, 1),
                    stride=1,
                    padding=(int((local_size-1.0)/2), 0, 0))
        else:
            self.average=nn.AvgPool2d(kernel_size=local_size,
                    stride=1,
                    padding=int((local_size-1.0)/2))
        self.alpha = alpha
        self.beta = beta
        self.k = k

    def forward(self, x):
        if self.ACROSS_CHANNELS:
            div = x.pow(2).unsqueeze(1)
            div = self.average(div).squeeze(1)
            div = div.mul(self.alpha).add(self.k).pow(self.beta)
        else:
            div = x.pow(2)
            div = self.average(div)
            div = div.mul(self.alpha).add(self.k).pow(self.beta)
        x = x.div(div)
        return x

class LambdaBase(nn.Sequential):
    def __init__(self, fn, *args):
        super(LambdaBase, self).__init__(*args)
        self.lambda_func = fn

    def forward_prepare(self, input):
        output = []
        for module in self._modules.values():
            output.append(module(input))
        return output if output else input

class Lambda(LambdaBase):
    def forward(self, input):
        return self.lambda_func(self.forward_prepare(input))

class VGGM(nn.Module):
    def __init__(self, num_classes=1000, dims = 128, *args, **kwargs):
        super(VGGM, self).__init__()
        self.num_classes = num_classes
        self.features = nn.Sequential(
            nn.Conv2d(3,96,(7, 7),(2, 2)),
            nn.ReLU(),
            SpatialCrossMapLRN(5, 0.0005, 0.75, 2),
            nn.MaxPool2d((3, 3),(2, 2),(0, 0),ceil_mode=True),
            nn.Conv2d(96,256,(5, 5),(2, 2),(1, 1)),
            nn.ReLU(),
            SpatialCrossMapLRN(5, 0.0005, 0.75, 2),
            nn.MaxPool2d((3, 3),(2, 2),(0, 0),ceil_mode=True),
            nn.Conv2d(256,512,(3, 3),(1, 1),(1, 1)),
            nn.ReLU(),
            nn.Conv2d(512,512,(3, 3),(1, 1),(1, 1)),
            nn.ReLU(),
            nn.Conv2d(512,512,(3, 3),(1, 1),(1, 1)),
            nn.ReLU(),
            nn.MaxPool2d((3, 3),(2, 2),(0, 0),ceil_mode=True)
        )
        self.classif = nn.Sequential(
            nn.Linear(18432,4096),
            nn.ReLU(),
            nn.Dropout(0.5),
            nn.Linear(4096,4096)
        )
        num_ftnum = self.classif[-1].out_features
        self.output_dim = self.classif[-1].out_features
        self.classif.add_module(name = 'relu_fc',  module = nn.ReLU(True))
        self.classif.add_module(name = 'drop_fc',  module = nn.Dropout())
#        assert num_ftnum == 4096, 'Please double check the last classification layer config'
        self.fc = nn.Linear(in_features = num_ftnum, out_features = num_classes)
#        self.classif.add_module(name='fc', module = nn.Linear(in_features = num_ftnum, out_features = num_class))
        self.fc_head = DenseNormReLU(in_feats = num_ftnum, out_feats=1024)
        self.embed = nn.Linear(in_features = 1024, out_features = dims)

        for el in self.fc_head.children():
            if isinstance(el, nn.Linear):
                nn.init.kaiming_normal_(el.weight, a=1)
                nn.init.constant_(el.bias, 0)
        nn.init.kaiming_normal_(self.embed.weight, a=1)
        #  nn.init.xavier_normal_(self.embed.weight, gain=1)
        nn.init.constant_(self.embed.bias, 0)
            
    def forward(self, x):        
        x = self.features(x)
        x = x.view(x.size(0), -1)
        x = self.classif(x) #self.classif is one layer less than model.classif
        fc = self.fc(x)
        x = self.fc_head(x)
        x = self.embed(x)
        return x, fc
    
class DenseNormReLU(nn.Module):
    def __init__(self, in_feats, out_feats, *args, **kwargs):
        super(DenseNormReLU, self).__init__(*args, **kwargs)
        self.dense = nn.Linear(in_features = in_feats, out_features = out_feats)
        self.bn = nn.BatchNorm1d(out_feats)
        self.relu = nn.ReLU(inplace = True)

    def forward(self, x):
        x = self.dense(x)
        x = self.bn(x)
        x = self.relu(x)
        return x

def vggm(num_classes=1000, pretrained='imagenet'):
    if pretrained:
        if pretrained == 'imagenet':
            settings = pretrained_settings['vggm'][pretrained]
    #        assert num_classes == settings['num_classes'], \
    #            "num_classes should be {}, but is {}".format(settings['num_classes'], num_classes)
    
            model = VGGM(num_classes=1000)
            # The following is a way to grab all the features from the original model
            # all_features = nn.Sequential(*list(model.children())[:-2])
            # the url doesnt pass checksum check, so will load from local
            # model.load_state_dict(model_zoo.load_url(settings['url']), model_dir = 'log/vggm/', strict = False)
            model.load_state_dict(torch.load(settings['pretrained_model']), strict = False)
            
            model.input_space = settings['input_space']
            model.input_size = settings['input_size']
            model.input_range = settings['input_range']
            model.mean = settings['mean']
            model.std = settings['std']
            
            num_fcft = model.fc.in_features
            model.fc = nn.Linear(num_fcft, num_classes)
            
        else:
            model = VGGM(num_classes=num_classes)
            model.load_model(pretrained, strict = False)
    else:
        model = VGGM(num_classes=num_classes)
    return model