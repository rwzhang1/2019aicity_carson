#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 16:32:53 2019

@author: rwzhang
"""

import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import sys,os,logging,time,itertools
import pandas as pd
from sklearn.utils import shuffle

from preprocess_data import AICTestData, convert_xml, AICQueryData
import numpy as np
import argparse, pickle, re
import models
from resnet50_fc import EmbedBaseNet
import cv2

torch.multiprocessing.set_sharing_strategy('file_system')
#Help: python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'log/resnet50/ep25000_model_soft_resnet.pkl'
#Query: python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet50' --num_class 333 --img_size 224 --nettype 'embnet' --base_ckpt 'log/resnet50/ep25000_model_soft_resnet.pkl'

#Help: python embed_joint_aic_veri_combi.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet' --n_layer 34  --num_class 333 --img_size 224 --nettype 'featcolor' --base_ckpt 'joint_aic_vid/joint_aic_ckpt/ep700_model_base.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep700_model_color.ckpt'
#Query: python embed_joint_aic_veri_combi.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet' --n_layer 34 --num_class 333 --img_size 224 --nettype 'featcolor' --base_ckpt 'joint_aic_vid/joint_aic_ckpt/ep700_model_base.ckpt' --color_ckpt 'joint_aic_vid/joint_aic_ckpt/ep700_model_color.ckpt'

def embed_embnet(dl, net, aic19_id_net, tencrop, mode):
    logger.info('start embedding')
#    all_iter_nums = len(ds) // args.batch_size + 1
    net.eval()
    all_iter_nums = len(dl)
    embeddings = []
    features = []
    label_ids = []
    pred_ids = []
    label_cams = []
    img_names = []
    visited_images=set()
    for it, sample in enumerate(dl):
        print('\r=======>  processing iter {} / {}'.format(it, all_iter_nums),
                end = '', flush = True)
        if args.mode == 'test':
            img, lb_id, lb_cam, img_name = sample
            label_ids.append(lb_id)
            label_cams.append(lb_cam)
            img_names.append(img_name)
        else:
            img, img_name = sample
            img_names.append(img_name)
        for imName in img_name:
            visited_images.add(imName)
        print('img_name: {}\n'.format(len(img_name)))
        if tencrop:
            bs, ncrops, c, h, w = img.size()
            print('img_shape: {}, {}, {}, {}, {}'.format(bs, ncrops, c, h, w))
            img = img.view(-1, c, h, w)            
        #for im in img:
        print('img_shape: {}'.format(img.size()))
        with torch.cuda.device(0):
            img = img.cuda(non_blocking=True)
        feat, embd = net(img)
        if tencrop:
            feat_avg = feat.view(bs, ncrops, -1).mean(1)
            pred_id = aic19_id_net(feat_avg)
            _, pred_id = torch.max(pred_id,dim=1)
            embd_avg = embd.view(bs, ncrops, -1).mean(1)
            print('feat_avg_shape: {}'.format(feat_avg.size()))
            features.append(feat_avg.detach().cpu().numpy())
            embeddings.append(embd_avg.detach().cpu().numpy())
            pred_ids.append(pred_id.detach().cpu().numpy())
        else:
            pred_id = aic19_id_net(feat)
            _, pred_id = torch.max(pred_id,dim=1)            
            features.append(feat.detach().cpu().numpy())
            embeddings.append(embd.detach().cpu().numpy())
            pred_ids.append(pred_id.detach().cpu().numpy())

    print('  ...   completed')
    print('features: {}, {}\n'.format(len(features), len(features[0])))
    features = np.vstack(features)
    embeddings = np.vstack(embeddings)
    img_names = np.hstack(img_names)
    pred_ids = np.hstack(pred_ids)
    print('total images visited: {}\n'.format(len(visited_images)))
    
    if mode == 'test':
        label_ids = np.hstack(label_ids)
        label_cams = np.hstack(label_cams)
        print('total labels: {}'.format(len(label_ids)))
        embd_res = {'features': features, 'embeddings': embeddings, 'pred_ids': pred_ids, 'label_ids': label_ids, 'label_cams': label_cams, 'img_names':img_names}
    else:
        embd_res = {'features': features, 'embeddings': embeddings, 'pred_ids': pred_ids, 'img_names':img_names}

    return embd_res
    
def embed_featnet(dl, net, tencrop, mode):
    logger.info('start embedding')
    net.eval()
    all_iter_nums = len(dl)
    features = []
    label_ids = []
    label_cams = []
    img_names = []
    visited_images=set()
    for it, sample in enumerate(dl):
        print('\r=======>  processing iter {} / {}'.format(it, all_iter_nums),
                end = '', flush = True)
        if args.mode == 'test':
            img, lb_id, lb_cam, img_name = sample
            label_ids.append(lb_id)
            label_cams.append(lb_cam)
            img_names.append(img_name)
        else:
            img, img_name = sample
            img_names.append(img_name)
        for imName in img_name:
            visited_images.add(imName)
        print('img_name: {}\n'.format(len(img_name)))
        if tencrop:
            bs, ncrops, c, h, w = img.size()
            print('img_shape: {}, {}, {}, {}, {}'.format(bs, ncrops, c, h, w))
            img = img.view(-1, c, h, w)            
        #for im in img:
        print('img_shape: {}'.format(img.size()))
        with torch.cuda.device(0):
            img = img.cuda(non_blocking=True)
        feat = net(img)
        if tencrop:
            feat_avg = feat.view(bs, ncrops, -1).mean(1)
            print('feat_avg_shape: {}'.format(feat_avg.size()))
            features.append(feat_avg.detach().cpu().numpy())
        else:
            features.append(feat.detach().cpu().numpy())
    print('  ...   completed')
    print('features: {}, {}\n'.format(len(features), len(features[0])))
    features = np.vstack(features)
    img_names = np.hstack(img_names)
    print('total images visited: {}\n'.format(len(visited_images)))
    
    if mode == 'test':
        label_ids = np.hstack(label_ids)
        label_cams = np.hstack(label_cams)
        print('total labels: {}'.format(len(label_ids)))
        embd_res = {'features': features, 'label_ids': label_ids, 'label_cams': label_cams, 'img_names':img_names}
    else:
        embd_res = {'features': features, 'img_names':img_names}
    return embd_res

def embed_featcolor(dl, net, color_net, tencrop, mode):
    logger.info('start embedding')
    net.eval()
    color_net.eval()
    all_iter_nums = len(dl)
    features = []
    colors= []
    label_ids = []
    label_cams = []
    img_names = []
    visited_images=set()
    for it, sample in enumerate(dl):
        print('\r=======>  processing iter {} / {}'.format(it, all_iter_nums),
                end = '', flush = True)
        if mode == 'test':
            img, lb_id, lb_cam, img_name = sample
            label_ids.append(lb_id)
            label_cams.append(lb_cam)
            img_names.append(img_name)
        else:
            img, img_name = sample
            img_names.append(img_name)
        for imName in img_name:
            visited_images.add(imName)      
        if tencrop:
            bs, ncrops, c, h, w = img.size()
            print('img_shape: {}, {}, {}, {}, {}'.format(bs, ncrops, c, h, w))
            img = img.view(-1, c, h, w)            
        print('img_shape: {}'.format(img.size()))
        with torch.cuda.device(0):
            img = img.cuda(non_blocking=True)   
        feat = net(img)
        print('feat_shape: {}'.format(feat.size()))     
        if tencrop:
            feat_avg = feat.view(bs, ncrops, -1).mean(1)
            pred_color = color_net(feat_avg)
            _, pred_color = torch.max(pred_color,dim=1)
            colors.append(pred_color.detach().cpu().numpy())
            features.append(feat_avg.detach().cpu().numpy())            
        else:
            pred_color = color_net(feat)
            _, pred_color = torch.max(pred_color,dim=1)
            colors.append(pred_color.detach().cpu().numpy())
            features.append(feat.detach().cpu().numpy())
        
    print('  ...   completed')
    features = np.vstack(features)
    colors = np.hstack(colors)
    img_names = np.hstack(img_names)
    print('unique colors: {}'.format(set(colors)))
#    print('colors: {}\n'.format([(img, col) for img,col in zip(img_names,colors)]))
    print('total images visited: {}\n'.format(len(visited_images)))
    
    if mode == 'test':
        label_ids = np.hstack(label_ids)
        label_cams = np.hstack(label_cams)
        print('total labels: {}'.format(len(label_ids)))
        embd_res = {'color_features': features, 'colors': colors, 'label_ids': label_ids, 'label_cams': label_cams, 'img_names':img_names}
    else:
        embd_res = {'color_features': features, 'colors': colors, 'img_names':img_names}
    return embd_res

def gen_color(emb_res, color_net, batch_size=24):
    color_net.eval()
    features, img_names = emb_res['features'],  emb_res['img_names']
    visited_images=set()
    batch_feat = []
    colors = []
    for it,(feat,img_name) in enumerate(zip(features,img_names)):
        visited_images.add(img_name)
        batch_feat.append(torch.tensor(feat))
        if (it+1)%args.batch_size == 0:
            print('total number of images in batch {} is: {}'.format(it, len(batch_feat)))
            with torch.cuda.device(0):
                batch_feat = torch.stack(batch_feat).cuda(non_blocking=True)
            pred_color = color_net(batch_feat)
            _, pred_color = torch.max(pred_color,dim=1)
            colors.append(pred_color.detach().cpu().numpy())
            logger.info('total images visited when finished iteration {} is: {}'.format(it, len(visited_images)))
            batch_feat = []
    if len(batch_feat) > 0:
        print('total number of images in batch {} is: {}'.format(it, len(batch_feat)))
        with torch.cuda.device(0):
            batch_feat = torch.stack(batch_feat).cuda(non_blocking=True)
        pred_color = color_net(batch_feat)
        _, pred_color = torch.max(pred_color,dim=1)
        colors.append(pred_color.detach().cpu().numpy())
        logger.info('total images visited when finished iteration {} is: {}'.format(it, len(visited_images)))
        batch_feat = []
    logger.info('total images visited when finished iteration {} is: {}'.format(it, len(visited_images)))
    print('  ...   completed')
    print('colors: {}, {}\n'.format(len(colors), len(colors[0])))
    colors = np.hstack(colors)
    emb_res['colors'] = colors
    print('total columns after adding colors: {}\n'.format(emb_res.keys()))
    return emb_res

def parse_args():
    ap = argparse.ArgumentParser('Running EMbeddings on the Test/Query set')
    ap.add_argument('--mode', dest='mode', help='test or query', type=str)
    ap.add_argument('--img_dir', dest='img_dir', help='img_dir', default='aic19-track2-reid/image_test/', type=str)
    ap.add_argument('--img_list', dest='img_list', help=' the raw images list for test', default='aic19-track2-reid/test_track_id.txt', type=str)
    ap.add_argument('--model', dest='model', help='model', default='resnet', type=str)
    ap.add_argument('--nettype', dest='nettype', help='the net to be used for inference', default='embnet', type=str)
    ap.add_argument('--n_layer', dest='n_layer',help='n_layer', default=50, type=int)
    ap.add_argument('--save_path', dest='save_path', help='path to save the embeddings', default='joint_aic_vid/', type=str)
    ap.add_argument('--base_ckpt', dest='base_ckpt', default='joint_aic_vid/resnet50/ep15_model_resnet50_base_net.pkl', type=str, help='The trained base_net/emb_net model to be loaded for eval')
    ap.add_argument('--aic19_id_ckpt', dest='aic19_id_ckpt', default='joint_aic_vid/resnet50/ep15_model_resnet50_aic19_id_net.pkl', type=str, help='The trained aic19_id model to be loaded for eval')
    ap.add_argument('--color_base', dest='color_base', default='joint_aic_vid/joint_aic_ckpt/ep150_model_resnet18_base.ckpt', type=str, help='The coresponding base net to color model')
    ap.add_argument('--color_nlayer', dest='color_nlayer', default=18, type=int, help='The nlayers of base net to color model')
    ap.add_argument('--n_color', dest='n_color', default=12, type=int, help='The n of colors to extract, 12 for compcar models, 10 for veri_ict')
    ap.add_argument('--color_ckpt', dest='color_ckpt', default='joint_aic_vid/joint_aic_ckpt/ep150_model_resnet18_color.ckpt', type=str, help='The trained color model to be loaded for eval')
    ap.add_argument('--gencolor', dest='gencolor', default=False, type=bool, help='whether to generate color from feature')
    ap.add_argument('--normalize', dest='normalize', default=False, type=bool, help='whether to normalize input image')
    ap.add_argument('--crop_scale',dest ='crop_scale', type=float,default=0.6,help='The percentage of scale when cropping')
    ap.add_argument('--num_crops', dest='num_crops', help='do we apply nulticrop during inference', default=10, type=int)
    ap.add_argument('--duplicates', dest='duplicates', help='how many iterations of random crops during inference', default=10, type=int)
    ap.add_argument('--num_class', dest='num_class', default=333, type=int, help='The numberK used in the PK-batches')
    ap.add_argument('--img_size', dest='img_size', default=224, type=int, help='The imageSize used')
    ap.add_argument('--batch_size', dest='batch_size', default=24, type=int, help='The batch size used')
    ap.add_argument('--ngpu', dest='ngpu', default=1, type=int, help='number of GPUs to be engaged')
    return ap.parse_args()

if __name__ == '__main__':
    args = parse_args()
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    emb_path = root_dir + args.save_path + 'emb{}_{}{}'.format(args.mode, args.model, args.n_layer) + '/'
    img_list = root_dir + args.img_list
    img_dir = root_dir + args.img_dir
    ngpu = args.ngpu
    is_tencrop = args.num_crops * args.duplicates > 1
    if not os.path.exists(emb_path): os.makedirs(emb_path)
    
    ## logging
    FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
    logger = logging.getLogger(__name__)
    logger.info('restoring model')
    m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.base_ckpt)
    cur_epoch = int(m.groupdict()['epoch'])
    print(cur_epoch)
    if is_tencrop:
        prefix = str(args.num_crops) + 'crop_'
    else:
        prefix = ''
    emb_name = '{}emb{}_{}_{}.pkl'.format(prefix, args.mode, cur_epoch, args.nettype)
    logger.info('fine-tune from checkpoint: {}, epoch: {}'.format(args.base_ckpt, cur_epoch))

    ## restore model
    base_net = models.FeatureResNet(n_layers=args.n_layer, pretrained=True)
    if args.nettype== 'embnet_nobn':
        emb_net = EmbedBaseNet(pretrained_base=True)
    else:
        emb_net = models.FeatRes51Net(n_layers=args.n_layer, pretrained=True)
    aic19_id_net = models.NLayersFC(base_net.output_dim, args.num_class)
    color_base = models.FeatureResNet(n_layers=args.color_nlayer, pretrained=True)
    if args.color_base != '':
        color_net = models.NLayersFC(color_base.output_dim, args.n_color)#12 for compcars/veri_triplet, 10 for veri_ict
    else:
	    color_net = models.NLayersFC(base_net.output_dim, args.n_color)

    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
    with torch.cuda.device(0):
        base_net = base_net.cuda()
        emb_net = emb_net.cuda()
        aic19_id_net = aic19_id_net.cuda()
        color_base = color_base.cuda()
        color_net = color_net.cuda()
    if (device.type == 'cuda') and (ngpu > 1): 
        base_net = nn.DataParallel(base_net, list(range(ngpu)))
        emb_net = nn.DataParallel(emb_net, list(range(ngpu)))
        aic19_id_net = nn.DataParallel(aic19_id_net, list(range(ngpu)))
        color_base = nn.DataParallel(color_base, list(range(ngpu)))
        color_net = nn.DataParallel(color_net, list(range(ngpu)))
        
    ## load gallery dataset
    if args.mode == 'test':
        ds = AICTestData(img_dir, img_list, img_size=args.img_size, duplicates = args.duplicates, normalize = args.normalize, num_crops = args.num_crops, crop_scale = args.crop_scale)
    else:
        ds = AICQueryData(img_dir, img_size=args.img_size, duplicates = args.duplicates, normalize = args.normalize, num_crops = args.num_crops, crop_scale = args.crop_scale)
    dl = DataLoader(ds, batch_size = args.batch_size, drop_last = False, num_workers = 4)

    if args.nettype == 'featnet':
        base_state_dict = torch.load(args.base_ckpt)
        if 'epoch' in base_state_dict:
            base_state_dict = base_state_dict['model_state_dict']
        base_net.load_state_dict(base_state_dict, strict = True)
        embd_res = embed_featnet(dl, base_net, tencrop = is_tencrop, mode = args.mode)
    if args.nettype == 'embnet' or args.nettype == 'embnet_nobn':
        emb_state_dict = torch.load(args.base_ckpt)
        if 'epoch' in emb_state_dict:
            emb_state_dict = emb_state_dict['model_state_dict']
        emb_net.load_state_dict(emb_state_dict, strict = True)
        aic19_id_state_dict = torch.load(args.aic19_id_ckpt)
        if 'epoch' in aic19_id_state_dict:
            aic19_id_state_dict = aic19_id_state_dict['model_state_dict']
        aic19_id_net.load_state_dict(aic19_id_state_dict, strict = True)
        embd_res = embed_embnet(dl, emb_net, aic19_id_net, tencrop = is_tencrop, mode = args.mode)
        
    if args.gencolor == True:
        print('start inferring from color net ')
        m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.color_ckpt)
        color_epoch = int(m.groupdict()['epoch'])
        emb_name = '{}emb{}_{}_color{}_{}.pkl'.format(prefix, args.mode, cur_epoch, color_epoch, args.nettype)
        color_state_dict = torch.load(args.color_ckpt)
        if color_state_dict.get('model_state_dict', None):
            logger.info('color_ckpt trained with joint_aic_veri_v3' )
            color_net.load_state_dict(color_state_dict['model_state_dict'], strict=True)
        else:
            logger.info('color_ckpt trained with veri_ict')
            for key in list(color_state_dict.keys()):
                if key.find('fc_c') == -1 or key.find('model.0') == -1:
                    del color_state_dict[key]
            color_net.load_state_dict(color_state_dict, strict = True)
        
        if args.color_base != '':
            '''
            if color base is not sharing the comment base net, need to re-evaluate
            '''
            base_state_dict = torch.load(args.color_base)
            for key in list(base_state_dict.keys()):
                if key.find('fc') != -1 or key.find('fc_c') != -1 or key.find('fc_t') != -1:
                    del base_state_dict[key]        
            color_base.load_state_dict(base_state_dict['model_state_dict'], strict = True)
            featcolor_res = embed_featcolor(dl, color_base, color_net, tencrop = is_tencrop, mode = args.mode)
            embd_res['colors'] = featcolor_res['colors'].copy()
            embd_res['color_features'] = featcolor_res['color_features'].copy()
        else:
            embd_res = gen_color(embd_res, color_net, args.batch_size)
   
    ## dump results
    logger.info('dump embeddings')
    emb_file = emb_path + '/' + emb_name
    if args.gencolor:
        emb_txt = emb_path + '/' + emb_name[:-3] + 'txt'
        emb_df = pd.DataFrame.from_dict(zip(embd_res['img_names'], embd_res['colors']))
        emb_df.to_csv(emb_txt, sep = ',') 
    with open(emb_file, 'wb') as fw:
        pickle.dump(embd_res, fw)
    logger.info('embedding finished')
