#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Creates a MobileNetV2 Model as defined in:
Mark Sandler, Andrew Howard, Menglong Zhu, Andrey Zhmoginov, Liang-Chieh Chen. (2018). 
MobileNetV2: Inverted Residuals and Linear Bottlenecks
arXiv preprint arXiv:1801.04381.
import from https://github.com/tonylins/pytorch-mobilenet-v2
Modified Version from: @rein9 
Purpose: On testing out add dropout layer and removing bn layer from dw conv and pw conv
"""

import torch.nn as nn
import math
import torch
from torch.autograd import Variable
from torchvision import transforms
import torch.nn.functional as F

__all__ = ['mobilenetv2']

def weight_decay_config(value=1e-4, log=True):
    def regularize_layer(m):
        non_depthwise_conv = isinstance(m, nn.Conv2d) \
            and m.groups != m.in_channels
        return isinstance(m, nn.Linear) or non_depthwise_conv

    return {'name': 'WeightDecay',
            'value': value,
            'log': log,
            'filter': {'parameter_name': lambda n: not n.endswith('bias'),
                       'module': regularize_layer}
            }
            
def _make_divisible(v, divisor, min_value=None):
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    :param v:
    :param divisor:
    :param min_value:
    :return:
    """
    if min_value is None:
        min_value = divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v


def conv_3x3_bn(inp, oup, stride):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
        nn.BatchNorm2d(oup),
        nn.ReLU6(inplace=True)
    )

def conv_1x1_bn(inp, oup):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
        nn.BatchNorm2d(oup),
        nn.ReLU6(inplace=True)
    )
    
def conv_1x1_dropbn(inp, oup):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
        nn.Dropout(0.001),
        nn.BatchNorm2d(oup),
        nn.ReLU6(inplace=True)
    )

class InvertedResidual(nn.Module):
    def __init__(self, inp, oup, stride, expand_ratio):
        super(InvertedResidual, self).__init__()
        assert stride in [1, 2]

        hidden_dim = round(inp * expand_ratio)
        self.identity = stride == 1 and inp == oup

        if expand_ratio == 1:
            self.conv = nn.Sequential(
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride, 1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )
        else:
            self.conv = nn.Sequential(
                # pw
                nn.Conv2d(inp, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride, 1, groups=hidden_dim, bias=False),
                # 04/11 commented out the BN and ReLU6 --> cooresponding res: mobile2_veritrip_dropbn
                # 04/15 uncommnet-->  cooresponding res: mobile2_veritrip_lastdrop
                # 04/18 commnet out the BN and ReLU6, no resize, no jitter --> res: mobile2_veritrip_noNRjitter
                # nn.BatchNorm2d(hidden_dim),
                # nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )

    def forward(self, x):
        if self.identity:
            return x + self.conv(x)
        else:
            return self.conv(x)


class MobileNetV2(nn.Module):
    def __init__(self, num_classes=1000, dims = 128, input_size=224, width_mult=1., with_top = True, regime = None):
        super(MobileNetV2, self).__init__()
        # setting of inverted residual blocks
        self.cfgs = [
            # t, c, n, s
            [1,  16, 1, 1],
            [6,  24, 2, 2],
            [6,  32, 3, 2],
            [6,  64, 4, 2],
            [6,  96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]
        self.model_name = 'mobilenetv2'
        # building first layer
        assert input_size % 32 == 0
        input_channel = _make_divisible(32 * width_mult, 8)
        layers = [conv_3x3_bn(3, input_channel, 2)]
        # building inverted residual blocks
        block = InvertedResidual
        for t, c, n, s in self.cfgs:
            output_channel = _make_divisible(c * width_mult, 8)
            layers.append(block(input_channel, output_channel, s, t))
            input_channel = output_channel
            for i in range(1, n):
                layers.append(block(input_channel, output_channel, 1, t))
                input_channel = output_channel
        self.features = nn.Sequential(*layers)
        # building last several layers
        output_channel = _make_divisible(1280 * width_mult, 8) if width_mult > 1.0 else 1280
        # updated on 04/11 from conv_1x1_bn to conv_1x1_drop
        self.conv = conv_1x1_dropbn(input_channel, output_channel)
        #change to avg adaptive pool
        self.avgpool = nn.AdaptiveAvgPool2d(1)
        self.with_top = with_top
        if with_top:
            self.classifier = nn.Linear(output_channel, num_classes)
        self.embed = nn.Linear(output_channel, dims)
        self.output_dim = output_channel
        self._initialize_weights()
        nn.init.orthogonal_(self.embed.weight, gain=1)
        nn.init.constant_(self.embed.bias, 0)
        
        if regime == 'small':
            scale_lr = 4
            self.regime = [
                {'epoch': 0, 'optimizer': 'SGD',
                 'momentum': 0.9, 'lr': scale_lr * 1e-1, 'regularizer': weight_decay_config(1e-4)},
                {'epoch': 30, 'lr': scale_lr * 1e-2},
                {'epoch': 60, 'lr': scale_lr * 1e-3},
                {'epoch': 80, 'lr': scale_lr * 1e-4}
            ]
            self.data_regime = [
                {'epoch': 0, 'input_size': 128, 'batch_size': 512},
                {'epoch': 80, 'input_size': 224, 'batch_size': 128},
            ]
            self.data_eval_regime = [
                {'epoch': 0, 'input_size': 128, 'batch_size': 1024},
                {'epoch': 80, 'input_size': 224, 'batch_size': 512},
            ]
        else:
            self.regime = [
                {'epoch': 0, 'optimizer': 'Adam', 'lr': 5e-4,
                 'momentum': 0.9, 'regularizer': weight_decay_config(1e-3)},
                {'epoch': 160, 'lr': 4e-4},
                {'epoch': 300, 'lr': 2e-4},
                {'epoch': 500, 'lr': 8e-5},
                {'epoch': 680, 'lr': 5e-5, 'regularizer': weight_decay_config(5e-5)},
                {'epoch': 880, 'lr': 2e-5},
                {'epoch': 1080, 'lr': 8e-6},
                {'epoch': 1280, 'lr': 5e-6},                
            ]

    def forward(self, x):
        # print('x shape before feature', x.shape)
        x = self.features(x)
        # print('x shape after feature', x.shape)
        x = self.conv(x)
        # print('x shape after conv', x.shape)
        x = self.avgpool(x)
        # print('x shape after avgpool ', x.shape)
        x = x.view(x.size(0), -1)
        # print('x shape before emb', x.shape)
        embs = self.embed(x)
        if self.with_top:
            fc = self.classifier(x)
            return fc, x, embs
        else:
            return x, embs

    def _initialize_weights(self):
        
        def truncated_normal_(tensor, mean=0, std=1):
            size = tensor.shape
            tmp = tensor.new_empty(size + (4,)).normal_()
            valid = (tmp < 2) & (tmp > -2)
            ind = valid.max(-1, keepdim=True)[1]
            tensor.data.copy_(tmp.gather(-1, ind).squeeze(-1))
            tensor.data.mul_(std).add_(mean)##
            
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                truncated_normal_(m.weight, std=0.09)
                #updates made on 04/09                
                #print('initialize conv2d with truncated_normal', m.weight.data)
                #n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                #m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                # update 04/09
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                nn.init.kaiming_normal_(m.weight, a=1)
                nn.init.constant_(m.bias, 0)
                # update 04/09
                #n = m.weight.size(1)
                #m.weight.data.normal_(0, 0.01)
                #m.bias.data.zero_()

def mobilenetv2(**kwargs):
    """
    Constructs a MobileNet V2 model
    """
    return MobileNetV2(**kwargs)

if __name__ == '__main__':
    from gen_aic_veri import TripletImage_Dataset
    from gen_aic_veri import Get_train_DataLoader
    import torch.optim as optim
    import matplotlib.pyplot as plt
    import cv2
    
    # test the hooks 
    veri_text = 'ReID_CNN/database/VeRi_train_info.txt'
    dataset = TripletImage_Dataset(veri_text, crop=True, flip=True, jitter=True, imagenet_normalize=False, image_per_class_in_batch=1)
    train_loader = Get_train_DataLoader(dataset, batch_size=2, num_workers=1)
    print('len', len(dataset), 'classes', dataset.classes)
    print('train n_batch', len(train_loader))
    n = 123
    im = dataset[n]['img']
    print(dataset[n]['class'],  dataset[n]['img_names'])
    if len(im.shape) > 3:
        for i in range(im.shape[0]):
            cv2.imshow('erased', im[i].permute(1,2,0).numpy())
            cv2.waitKey(10000)
    else:
        cv2.imshow('erased', im.permute(1,2,0).numpy())
        cv2.waitKey(10000)
    print('im shape: ', im.shape)
    
    model_ckpt = 'ReID_CNN/ckpt_mobile2_veritrip_lastdrop/model_mobilenetv2_1500.ckpt'
    model = MobileNetV2(num_classes=dataset.n_id)
    activation = {}
    def get_activation(name):
        def hook(model, input, output):
            activation[name] = output.detach()
        return hook
    
    def normalize_output(img):
        img = img - img.min()
        img = img / img.max()
        return img
    
    '''
    temporarily create data_parallel to load state dict
    ngpu = 3
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
    with torch.cuda.device(0):
        model = model.cuda()
    if (device.type == 'cuda') and (ngpu > 1):
        model = nn.DataParallel(model, list(range(ngpu)))
    model_dict = torch.load(model_ckpt)
    print(model_dict['model_state_dict'].keys())
    model.load_state_dict(model_dict['model_state_dict'])
    '''
    # original saved file with DataParallel
    state_dict = torch.load(model_ckpt)
    # create new OrderedDict that does not contain `module.`
    from collections import OrderedDict
    new_state_dict = OrderedDict()
    for k, v in state_dict['model_state_dict'].items():
        name = k[7:] # remove `module.`
        new_state_dict[name] = v
    # load params
    model.load_state_dict(new_state_dict)
        
    model.conv[0].register_forward_hook(get_activation('conv_last'))
    model.features[0].register_forward_hook(get_activation('conv'))
    # Plot some images
    pred_class, feature, embs = model(im)
    
    act = activation['conv_last'].squeeze()
    print('act shape:', act.shape)
    fig, axarr = plt.subplots(4,4)
    for idx in range(min(16,act.size(0))):
        axarr[idx//4, idx%4].imshow(act[idx])
        fig.savefig('log/visual/mobilenet2_conv_last.jpg')
    act = activation['conv'].squeeze()
    print('act shape:', act.shape)
    fig, axarr = plt.subplots(4,4)
    for idx in range(min(16,act.size(0))):
        axarr[idx//4, idx%4].imshow(act[idx])
        fig.savefig('log/visual/mobilenet2_conv.jpg')