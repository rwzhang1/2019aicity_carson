#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 14:14:00 2019

@author: rwzhang
"""

import os
import pickle
import numpy as np
import sys
import logging
import argparse
import time
from tqdm import tqdm

from utils.utils import pdist_np as pdist
from utils.utils import cal_cos_dis as cdist
from utils.utils import l2dist_np as l2dist

#python gen_ranking.py --test_embs joint_aic_vid/embtest_featnet/embtest_880_featnet.pkl --query_embs joint_aic_vid/embquery_featnet/embquery_880_featnet.pkl
def parse_args():
    parse = argparse.ArgumentParser()
    parse.add_argument('--nettype', help='the net to be used for inference', default='embnet', type=str)
    parse.add_argument('--test_embs', dest = 'test_embs', type = str, default = 'joint_aic_vid/embtest_resnet50/tencrop_embtest_32_color39_embnet.pkl', help = 'path to embeddings of test dataset')
    parse.add_argument('--query_embs', dest = 'query_embs', type = str, default = 'joint_aic_vid/embquery_resnet50/tencrop_embquery_32_color39_embnet.pkl', help = 'path to embeddings of query dataset')
    parse.add_argument('--cmc_rank', dest = 'cmc_rank', type = int, default = 100, help = 'path to embeddings of query dataset')
    parse.add_argument('--save_path', dest = 'save_path', type = str, default = 'joint_aic_vid', help = 'path to save the top cmc_ranked pictures from query')
    parse.add_argument('--gencolor', dest= 'gencolor', default=False, type=bool, help='whether to generate color from feature')
    parse.add_argument('--feat_opt', dest='feat_opt', type=str, default = 'feat', help='the option to choose where to calculate the distance using 2048 feature or 128 embeddings')
    parse.add_argument('--color_weight', dest='color_weight', type=float, default = 0.3, help='the option to choose where to calculate the distance using 2048 feature or 128 embeddings')
    parse.add_argument('--disttype', dest='disttype', type=str, default = 'pdist', help='pdistance or cosine distance')
    return parse.parse_args()

args = parse_args()
root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
test_embs = root_dir + args.test_embs
query_embs = root_dir + args.query_embs
save_path = root_dir + args.save_path + '/' + 'cmc_rank/'

#test_embs = root_dir + 'emb/resnet50/emb_25000model_trip_soft_res50.pkl'
#query_embs = root_dir + 'embquery/resnet50/emb_25000model_trip_soft_res50.pkl'
if not os.path.exists(save_path):
    os.makedirs(save_path)
else:
    new_path = '{}_{}'.format(root_dir + args.save_path + '/' + 'cmc_rank', time.strftime("%Y%m%d%H%M"))
    print(new_path)
    os.rename(save_path,new_path)

def evaluate(args):
    ## logging
    FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
    logger = logging.getLogger(__name__)

    ## load embeddings
    logger.info('loading gallery embeddings')
    with open(test_embs, 'rb') as fr:
        test_dict = pickle.load(fr)
        if args.nettype == 'featnet':
            feat_test, img_names_test = test_dict['features'], test_dict['img_names']
        elif args.nettype == 'embnet':
            feat_test, emb_test, test_pids, img_names_test = test_dict['features'], test_dict['embeddings'], test_dict['pred_ids'], test_dict['img_names']
        color_test= test_dict.get('colors',None)
        cfeat_test= test_dict.get('color_features', None)
            
    logger.info('loading query embeddings')
    with open(query_embs, 'rb') as fr:
        query_dict = pickle.load(fr)
        if args.nettype == 'featnet':
            feat_query, img_name_query= query_dict['features'], query_dict['img_names']
        elif args.nettype == 'embnet':
            feat_query, emb_query, img_name_query= query_dict['features'], query_dict['embeddings'],query_dict['img_names']
        if args.gencolor:
            print(args.gencolor)
            color_query= query_dict['colors']
            cfeat_query = query_dict['color_features']
        else:
            print('No color')
            cfeat_query = np.zeros(emb_query.shape)
            cfeat_test = np.zeros(emb_test.shape)
            args.color_weight = 0
    
    if args.feat_opt == 'emb':
        query_obj = np.copy(emb_query)
        test_obj = np.copy(emb_test)
    elif args.feat_opt == 'feat':
        query_obj = np.copy(feat_query)
        test_obj = np.copy(feat_test)
    print(query_obj.shape, test_obj.shape)
        # query_obj = np.concatenate(query_obj, np.copy(cfeat_query),axis=1)
        # test_obj = np.concatenate(test_obj, np.copy(cfeat_test),axis=1)  
    margin = args.color_weight
    for i, emb_q in tqdm(enumerate(query_obj)):
        query_name = img_name_query[i]
        temp_emb = np.expand_dims(emb_q, axis = 0)
        temp_cfeat = np.expand_dims(cfeat_query[i], axis = 0)
        # selected_inds = np.where(test_pids==query_pids[i])[0]
        # selected_inds = np.where(color_test==color_query[i])[0]
        # print('total test: {}, selected_index:{}\n'.format(len(img_names_test), selected_inds))
        # test_obj_i = test_obj[selected_inds]
        # test_cfeat_i = cfeat_test[selected_inds]
        # img_names_test_i = img_names_test[selected_inds]
        test_obj_i = test_obj[:]
        test_cfeat_i = cfeat_test[:]
        img_names_test_i = img_names_test[:]
        print('selected_length:{}\n'.format(len(img_names_test_i)))
        if args.disttype == 'pdist':
            dist_one2all = (1.0-margin)*pdist(temp_emb, test_obj_i) + margin* pdist(temp_cfeat, test_cfeat_i)
            rank = np.argsort(dist_one2all, axis = 1)
            test_imgs = img_names_test_i[rank][0][:args.cmc_rank]
        elif args.disttype == 'cosdist':
            cdist_one2all = (1.0-margin)*cdist(temp_emb, test_obj_i) + margin*cdist(temp_cfeat, test_cfeat_i)
            crank = np.argsort(cdist_one2all)
            test_imgs = img_names_test_i[crank][:args.cmc_rank]
        save_file = save_path + query_name[-10:-4] + '.txt'
        open(save_file, 'w').close()
        with open(save_file, 'w') as f:
            for img in test_imgs:
                f.write('%s\n' % img[:-4])
            f.close()

if __name__ == '__main__':
    args = parse_args()
    evaluate(args)
