#!/usr/bin/env python2/python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  12 14:28:48 2019

@author: rwzhang
"""
import os, sys, re, time
import pickle
import logging
import argparse
from tqdm import tqdm

from utils.utils import pdist_np as pdist
from utils.utils import cal_cos_dis as cdist
from utils.utils import l2dist_np as l2dist

import numpy as np
from utils.utils import pdist_np as pdist
from utils.utils import cal_cos_dis as cdist
from utils.utils import l2dist_np as l2dist



#python gen_ranking.py --test_embs joint_aic_vid/embtest_resnet50/embtest_24192_resnet50.pkl --query_embs joint_aic_vid/embquery_resnet50/embquery_24192_resnet50.pkl
def parse_args():
    parse = argparse.ArgumentParser()
    parse.add_argument('--nettype', help='the net to be used for inference', default='embnet', type=str)
    parse.add_argument('--test_embs', dest = 'test_embs', type = str, default = 'joint_aic_vid/embtest_resnet50/0312gold_10crop_embtest_79_embnet.pkl', help = 'path to embeddings of test dataset')
    parse.add_argument('--query_embs', dest = 'query_embs', type = str, default = 'joint_aic_vid/embquery_resnet50/0312gold_10crop_embquery_79_embnet.pkl', help = 'path to embeddings of query dataset')
    parse.add_argument('--test_dir', dest = 'test_dir', type = str, default = 'joint_aic_vid/embtest_resnet50', help = 'path to all test embeddings')
    parse.add_argument('--query_dir', dest = 'query_dir', type = str, default = 'joint_aic_vid/embquery_resnet50', help = 'path to all query embeddings')

    parse.add_argument('--cmc_rank', dest = 'cmc_rank', type = int, default = 100, help = 'path to embeddings of query dataset')
    parse.add_argument('--save_path', dest = 'save_path', type = str, default = 'joint_aic_vid', help = 'path to save the top cmc_ranked pictures from query')
    parse.add_argument('--gencolor', dest= 'gencolor', default=False, type=bool, help='whether to generate color from feature')
    parse.add_argument('--feat_opt', dest='feat_opt', type=str, default = 'feat', help='the option to choose where to calculate the distance using 2048 feature or 128 embeddings')
    parse.add_argument('--color_weight', dest='color_weight', type=float, default = 0.3, help='the option to choose where to calculate the distance using 2048 feature or 128 embeddings')
    parse.add_argument('--disttype', dest='disttype', type=str, default = 'pdist', help='pdistance or cosine distance')
    parse.add_argument('--dist_file', dest='dist_file', help='dist_file', default='./joint_aic_vid/dist_25000.txt', type=str)
    return parse.parse_args()

def gen_dist(test_embs, query_embs):
    logger.info('loading query embeddings')
    with open(test_embs, 'rb') as fr:
        test_dict = pickle.load(fr)
        if args.nettype == 'featnet':
            feat_test, img_names_test = test_dict['features'], test_dict['img_names']
        elif args.nettype == 'embnet':
            feat_test, emb_test, test_pids, img_names_test = test_dict['features'], test_dict['embeddings'], test_dict['pred_ids'], test_dict['img_names']
        color_test= test_dict.get('colors',None)
        cfeat_test= test_dict.get('color_features', None)
        
    logger.info('loading query embeddings')
    with open(query_embs, 'rb') as fr:
        query_dict = pickle.load(fr)
        if args.nettype == 'featnet':
            feat_query, img_name_query= query_dict['features'], query_dict['img_names']
        elif args.nettype == 'embnet':
            feat_query, emb_query, query_pids, img_names_query= query_dict['features'], query_dict['embeddings'], query_dict['pred_ids'], query_dict['img_names']
        if args.gencolor:
            print(args.gencolor)
            color_query= query_dict['colors']
            cfeat_query = query_dict['color_features']
        else:
            print('No color')
            cfeat_query = np.zeros(emb_query.shape)
            cfeat_test = np.zeros(emb_test.shape)
            args.color_weight = 0
        
    if args.feat_opt == 'emb':
        query_obj = np.copy(emb_query)
        test_obj = np.copy(emb_test)
    elif args.feat_opt == 'feat':
        query_obj = np.copy(feat_query)
        test_obj = np.copy(feat_test)
    q_n = query_obj.shape[0]
    g_n = test_obj.shape[0]        
    dist = np.zeros([g_n, q_n], dtype=np.float32)
    
    fout = open(args.dist_file, 'w')
    for g in range(0,g_n):
        for q in range(0, q_n):
            g_name = img_names_test[g]
            g_feat = test_obj[g]
            q_name = img_names_query[q]
            q_feat = query_obj[q]
            dist[g][q] = np.linalg.norm(g_feat-q_feat)
            fout.writelines('g_name: {}, q_name: {}, distance: {}\n'. format(g_name, q_name, str(dist[g][q])))
        fout.writelines('\n')
    fout.close()
    print('over')
    return query_obj, test_obj
    
def re_ranking(q_feat, g_feat, k1= 20, k2 = 6, lambda_value=0.3):
    """
    CVPR2017 paper:Zhong Z, Zheng L, Cao D, et al. Re-ranking Person Re-identification with k-reciprocal Encoding[J]. 2017.
    url:http://openaccess.thecvf.com/content_cvpr_2017/papers/Zhong_Re-Ranking_Person_Re-Identification_CVPR_2017_paper.pdf
    Matlab version: https://github.com/zhunzhong07/person-re-ranking
    
    @q_g_dist: query-gallery distance matrix, numpy array, shape [num_query, num_gallery]
    @q_q_dist: query-query distance matrix, numpy array, shape [num_query, num_query]
    @g_g_dist: gallery-gallery distance matrix, numpy array, shape [num_gallery, num_gallery]
    @k1, k2, lambda_value: parameters, the original paper is (k1=20, k2=6, lambda_value=0.3)
    @Returns:
      final_dist: re-ranked distance, numpy array, shape [num_query, num_gallery]
    """
    q_g_dist = pdist(q_feat, g_feat)
    q_q_dist = pdist(q_feat, q_feat)
    g_g_dist = pdist(g_feat, g_feat)
    original_dist = np.concatenate(
      [np.concatenate([q_q_dist, q_g_dist], axis=1),
       np.concatenate([q_g_dist.T, g_g_dist], axis=1)],
      axis=0)
    original_dist = np.power(original_dist, 2).astype(np.float32)
    original_dist = np.transpose(1. * original_dist/np.max(original_dist,axis = 0))
    V = np.zeros_like(original_dist).astype(np.float32)
    initial_rank = np.argsort(original_dist).astype(np.int32)

    query_num = q_g_dist.shape[0]
    gallery_num = q_g_dist.shape[0] + q_g_dist.shape[1]
    all_num = gallery_num

    for i in range(all_num):
        # k-reciprocal neighbors
        forward_k_neigh_index = initial_rank[i,:k1+1]
        backward_k_neigh_index = initial_rank[forward_k_neigh_index,:k1+1]
        fi = np.where(backward_k_neigh_index==i)[0]
        k_reciprocal_index = forward_k_neigh_index[fi]
        k_reciprocal_expansion_index = k_reciprocal_index
        for j in range(len(k_reciprocal_index)):
            candidate = k_reciprocal_index[j]
            candidate_forward_k_neigh_index = initial_rank[candidate,:int(np.around(k1/2.))+1]
            candidate_backward_k_neigh_index = initial_rank[candidate_forward_k_neigh_index,:int(np.around(k1/2.))+1]
            fi_candidate = np.where(candidate_backward_k_neigh_index == candidate)[0]
            candidate_k_reciprocal_index = candidate_forward_k_neigh_index[fi_candidate]
            if len(np.intersect1d(candidate_k_reciprocal_index,k_reciprocal_index))> 2./3*len(candidate_k_reciprocal_index):
                k_reciprocal_expansion_index = np.append(k_reciprocal_expansion_index,candidate_k_reciprocal_index)

        k_reciprocal_expansion_index = np.unique(k_reciprocal_expansion_index)
        weight = np.exp(-original_dist[i,k_reciprocal_expansion_index])
        V[i,k_reciprocal_expansion_index] = 1.*weight/np.sum(weight)
    original_dist = original_dist[:query_num,]
    if k2 != 1:
        V_qe = np.zeros_like(V,dtype=np.float32)
        for i in range(all_num):
            V_qe[i,:] = np.mean(V[initial_rank[i,:k2],:],axis=0)
        V = V_qe
        del V_qe
    del initial_rank
    invIndex = []
    for i in range(gallery_num):
        invIndex.append(np.where(V[:,i] != 0)[0])

    jaccard_dist = np.zeros_like(original_dist,dtype = np.float32)


    for i in range(query_num):
        temp_min = np.zeros(shape=[1,gallery_num],dtype=np.float32)
        indNonZero = np.where(V[i,:] != 0)[0]
        indImages = []
        indImages = [invIndex[ind] for ind in indNonZero]
        for j in range(len(indNonZero)):
            temp_min[0,indImages[j]] = temp_min[0,indImages[j]]+ np.minimum(V[i,indNonZero[j]],V[indImages[j],indNonZero[j]])
        jaccard_dist[i] = 1-temp_min/(2.-temp_min)

    final_dist = jaccard_dist*(1-lambda_value) + original_dist*lambda_value
    del original_dist
    del V
    del jaccard_dist
    final_dist = final_dist[:query_num,query_num:]
    return final_dist

#def gen_cmc(args, query_obj, test_obj, cfeat_query, cfeat_test, img_name_query, img_name_test):
def gen_cmc(query_embs, test_embs):
    logger.info('loading query embeddings')
    with open(test_embs, 'rb') as fr:
        test_dict = pickle.load(fr)
        if args.nettype == 'featnet':
            feat_test, img_names_test = test_dict['features'], test_dict['img_names']
        elif args.nettype == 'embnet':
            feat_test, emb_test, test_pids, img_names_test = test_dict['features'], test_dict['embeddings'], test_dict['pred_ids'], test_dict['img_names']
        color_test= test_dict.get('colors',None)
        cfeat_test= test_dict.get('color_features', None)
        
    logger.info('loading query embeddings')
    with open(query_embs, 'rb') as fr:
        query_dict = pickle.load(fr)
        if args.nettype == 'featnet':
            feat_query, img_name_query= query_dict['features'], query_dict['img_names']
        elif args.nettype == 'embnet':
            feat_query, emb_query, query_pids, img_names_query= query_dict['features'], query_dict['embeddings'], query_dict['pred_ids'], query_dict['img_names']
        if args.gencolor:
            print(args.gencolor)
            color_query= query_dict['colors']
            cfeat_query = query_dict['color_features']
        else:
            print('No color')
            cfeat_query = np.zeros(emb_query.shape)
            cfeat_test = np.zeros(emb_test.shape)
            args.color_weight = 0
        
    if args.feat_opt == 'emb':
        query_obj = np.copy(emb_query)
        test_obj = np.copy(emb_test)
    elif args.feat_opt == 'feat':
        query_obj = np.copy(feat_query)
        test_obj = np.copy(feat_test)

    
    q_n = cfeat_query.shape[0]
    g_n = cfeat_test.shape[0]        
    dist = np.zeros([q_n, g_n], dtype=np.float32)
    print('dist_matrix size', dist.shape)
    for rep in range(4):
        # 4 repeats
        # randomly assign marking to different feature 
        margin = np.random.rand()*0.6+0.5
        dist += margin*re_ranking(query_obj,test_obj)
    
    for i in range(q_n):
        query_name = img_names_query[i]
        img_names_test_i = img_names_test[:]
        dist_one2all = dist[i]
        print(dist_one2all.shape)
        rank = np.argsort(dist_one2all)
        print('rank', img_names_test_i[rank][0])
        test_imgs = img_names_test_i[rank][:args.cmc_rank]
        save_file = save_path + query_name[-10:-4] + '.txt'
        open(save_file, 'w').close()
        with open(save_file, 'w') as f:
            for img in test_imgs:
                f.write('%s\n' % img[:-4])
            f.close()
            
#def gen_cmc(args, query_obj, test_obj, cfeat_query, cfeat_test, img_name_query, img_name_test):
def gen_cmc_folder(query_dir, test_dir):
    query_list = sorted(os.listdir(query_dir))
    dist, gold_query_img, gold_test_img = None, None, None    
    for i_q, query_name in enumerate(query_list):
        test_name = re.sub('query', 'test', query_name)
        query_embs = os.path.join(query_dir, query_name)        
        test_embs = os.path.join(test_dir, test_name)
        if not os.stat(test_embs):
            print('matching query: {} not found'.format(query_name))
            continue

        print(query_embs, test_embs)
        logger.info('loading query embeddings')
        with open(test_embs, 'rb') as fr:
            test_dict = pickle.load(fr)
            print('test_keys', test_dict.keys())
            img_names_test = test_dict['img_names']
            
            if 'features' not in test_dict:
                feat_test = test_dict['embeddings']
            else:
                feat_test= test_dict['features']
            color_test= test_dict.get('colors',None)
            cfeat_test= test_dict.get('color_features', None)
            
        logger.info('loading query embeddings')
        with open(query_embs, 'rb') as fr:
            query_dict = pickle.load(fr)
            img_names_query = query_dict['img_names']
            if 'features' not in query_dict:
                feat_query= query_dict['embeddings']
            else:
                feat_query = query_dict['features']
                
            if args.gencolor:
                print(args.gencolor)
                color_query= query_dict['colors']
                cfeat_query = query_dict['color_features']
            else:
                print('No color')
                cfeat_query = np.zeros(feat_query.shape)
                cfeat_test = np.zeros(feat_test.shape)
                args.color_weight = 0
            
        if args.feat_opt == 'emb':
            query_obj = np.copy(feat_query)
            test_obj = np.copy(emb_test)
        elif args.feat_opt == 'feat':
            query_obj = np.copy(feat_query)
            test_obj = np.copy(feat_test)
       
        q_n = query_obj.shape[0]
        g_n = test_obj.shape[0]    
        if i_q == 0:
            dist = np.zeros([q_n, g_n], dtype=np.float32)
            gold_query_img = img_names_query[:]
            gold_test_img = img_names_test[:]
            
        assert list(gold_query_img) == list(img_names_query), 'query_image order not matching'
        assert list(gold_test_img) == list(img_names_test), 'test_image order not matching'
        print('dist_matrix size', dist.shape)
        for rep in range(4):
            # 4 repeats
            # randomly assign marking to different feature 
            margin = np.random.rand()*0.6+0.5
            dist += margin*re_ranking(query_obj,test_obj)
    
    for i in range(q_n):
        query_name = gold_query_img[i]
        img_names_test_i = gold_test_img[:]
        dist_one2all = dist[i]
        print(dist_one2all.shape)
        rank = np.argsort(dist_one2all)
        print('rank', img_names_test_i[rank][0])
        test_imgs = img_names_test_i[rank][:args.cmc_rank]
        save_file = save_path + query_name[-10:-4] + '.txt'
        open(save_file, 'w').close()
        with open(save_file, 'w') as f:
            for img in test_imgs:
                f.write('%s\n' % img[:-4])
            f.close()
            
if __name__ == '__main__':
    args = parse_args()
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    test_embs = root_dir + args.test_embs
    query_embs = root_dir + args.query_embs
    test_dir = root_dir + args.test_dir
    query_dir = root_dir + args.query_dir
    save_path = root_dir + args.save_path + '/' + 'cmc_rerank/'
    if not os.path.exists(save_path): os.makedirs(save_path)
    FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
    logger = logging.getLogger(__name__)
    #q_feat, g_feat = gen_dist(test_embs, query_embs)
    #res = re_ranking(q_feat,g_feat)
    #res = np.transpose(res)
    #with open(os.path.join('joint_aic_vid/re_rank', 'rerank_sim_mar.txt'), 'w') as f:
    #    np.savetxt(f, res)
    # gen_cmc(query_embs, test_embs)
    gen_cmc_folder(query_dir, test_dir)
    
    print('done')
