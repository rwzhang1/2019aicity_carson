#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 17:21:34 2019
Train aic/veri_ict/vid jointly
@author: rwzhang
"""
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.backends.cudnn as cudnn
from gen_aic_veri import TripletImage_Dataset, sv_comp_Dataset, BoxCars_Dataset, AIC19_Dataset,VehicleID_Dataset
from gen_aic_veri import Get_train_DataLoader, Get_val_DataLoader, Get_test_DataLoader
from utils.loss import Soft_TripletLoss as TripletLoss
from utils.optimizer import AdamOptimWrapper, SGDOptimWrapper
from ReID_CNN.logger import Logger
from othernet_fc import embvgg_bn,EMBVGG
from EmbedVGGM import vggm,VGGM
from mobilenetv2 import MobileNetV2
from mobilenet import MobileNet
from densenet import DenseNet
from resnext_fc import ResNeXt
import models
import sys
from tqdm import tqdm
import argparse
import sys
import os
import numpy as np
cudnn.benchmark=True

def train_joint(args, train_veri_dataloader, val_veri_dataloader,
                train_compcars_dataloader, val_compcars_dataloader, 
                train_vid_dataloader, val_vid_dataloader, 
                train_aic_dataloader, 
                test_compcars_dataloader,
                base_net, veri_id_net, color_net, 
                aic19_id_net, cams_net, 
                compcars_model_net,
                vid_id_net):
    # 03/21, adding option to enable freezing some layers for training.
    #optimizer_base = AdamOptimWrapper(list(filter(lambda p: p.requires_grad, base_net.parameters())), lr = args.lr, momentum = 0.9)
    optimizer_base = AdamOptimWrapper(base_net.parameters(), lr=args.lr, wd=1e-4, t0=args.decay_start_iteration, betas = (0.7,0.999))
    #optimizer_base = SGDOptimWrapper(base_net.parameters(), lr=args.lr, wd=1e-4, t0=args.decay_start_iteration)
    #optimizer_base = optim.Adam(base_net.parameters(), lr=args.lr)
    
    optimizer_veri = optim.Adam(veri_id_net.parameters(), lr=optimizer_base.lr)
    optimizer_color = optim.Adam(color_net.parameters(), lr=optimizer_base.lr)
    optimizer_aic19 = optim.Adam(aic19_id_net.parameters(), lr=optimizer_base.lr)
    optimizer_cid = optim.Adam(cams_net.parameters(),lr=optimizer_base.lr)
    optimizer_compcars = optim.Adam(compcars_model_net.parameters(), lr=optimizer_base.lr)
    optimizer_vid = optim.Adam(vid_id_net.parameters(), lr=optimizer_base.lr)
    
    # before 04/01
    # optimizer_veri = optim.Adam(veri_id_net.parameters(), lr=args.lr)
    # optimizer_color = optim.Adam(color_net.parameters(), lr=args.lr)
    # optimizer_aic19 = optim.Adam(aic19_id_net.parameters(), lr=args.lr)
    # optimizer_cid = optim.Adam(cams_net.parameters(),lr=args.lr)
    # optimizer_emb = optim.Adam(emb_net.parameters(), lr=args.lr)
    # optimizer_compcars = optim.Adam(compcars_model_net.parameters(), lr=args.lr)
    # optimizer_vid = optim.Adam(vid_id_net.parameters(), lr=args.lr)
    criterion_triplet = TripletLoss(margin=margin, batch_hard=args.batch_hard)
    criterion_ce = nn.CrossEntropyLoss()
    logger = Logger(os.path.join(args.save_model_dir,'train'))
    val_logger = Logger(os.path.join(args.save_model_dir,'val'))
    test_logger = Logger(os.path.join(args.save_model_dir,'test'))
    
    #epoch_size = min(len(train_veri_dataloader), len(train_compcars_dataloader), len(train_aic_dataloader))
    epoch_size = min(len(train_veri_dataloader), len(train_compcars_dataloader), len(train_vid_dataloader), len(train_aic_dataloader))
    for e in range(args.n_epochs):
        
        pbar = tqdm(total=epoch_size,ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))
        
        for n in range(epoch_size):
            logger.append_epoch(e + float(n)/epoch_size)
            # VeRi dataset
            epoch_loss = 0
            for i, samples in enumerate(train_veri_dataloader):
                if i==1: break
                imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                           samples['img'].size(2), 
                                           samples['img'].size(3),
                                           samples['img'].size(4))
                classes = samples['class'].view(-1)
                colors = samples['color'].view(-1)
                b_img = Variable(imgs).cuda()
                classes = Variable(classes).cuda()
                colors = Variable(colors).cuda()
                base_net.zero_grad()
                veri_id_net.zero_grad()
                color_net.zero_grad()
                #forward
                pred_feat,_ = base_net(b_img)
                # pred_feat = base_net(b_img)
                pred_id = veri_id_net(pred_feat)
                pred_color = color_net(pred_feat)
                b_loss_triplet = criterion_triplet(pred_feat, classes)
                loss_id = criterion_ce(pred_id, classes)
                loss_color = criterion_ce(pred_color, colors)
                loss = b_loss_triplet.mean() + loss_id + loss_color
                print(loss)
                epoch_loss += loss.data
                # backward
                optimizer_base.zero_grad()
                optimizer_veri.zero_grad()
                optimizer_color.zero_grad()
                loss.backward()
                optimizer_base.step()
                optimizer_veri.step()
                optimizer_color.step()
    
                logger.logg({'loss_veri_triplet': b_loss_triplet.data.mean(),
                            'loss_veri_triplet_max': b_loss_triplet.data.max(),
                            'loss_veri_triplet_min': b_loss_triplet.data.min(),
                            'loss_veri_id': loss_id.data,
                            'loss_veri_color': loss_color.data})

            # Compcars dataset
            for i, samples in enumerate(train_compcars_dataloader):
                if i==1: break
                img = Variable(samples['img']).cuda()
                model = Variable(samples['model']).cuda()
                color = Variable(samples['color']).cuda()
                base_net.zero_grad()
                compcars_model_net.zero_grad()
                color_net.zero_grad()
                #forward
                pred_feat,_ = base_net(img)
                # pred_feat = base_net(img)
                pred_model = compcars_model_net(pred_feat)
                pred_color = color_net(pred_feat)
                loss_model = criterion_ce(pred_model, model)
                loss_color = criterion_ce(pred_color, color)
                loss = loss_model + loss_color
                epoch_loss += loss.data
                # backward
                optimizer_base.zero_grad()
                optimizer_compcars.zero_grad()
                optimizer_color.zero_grad()
                loss.backward()
                optimizer_base.step()
                optimizer_compcars.step()
                optimizer_color.step()
    
                logger.logg({'loss_compcars_model': loss_model.data,
                            'loss_compcars_color': loss_color.data})
            
            # Vid dataset
            for i, samples in enumerate(train_vid_dataloader):
                if i==1: break
                imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                           samples['img'].size(2), 
                                           samples['img'].size(3),
                                           samples['img'].size(4))
                id = samples['id'].view(-1)
                b_img = Variable(imgs).cuda()
                id = Variable(id).cuda()
                base_net.zero_grad()
                vid_id_net.zero_grad()
                #forward
                pred_feat,_ = base_net(b_img)
                # pred_feat = base_net(b_img)
                pred_id = vid_id_net(pred_feat)
                loss_id = criterion_ce(pred_id, id)
                loss = loss_id
                epoch_loss += loss.data
                # backward
                optimizer_base.zero_grad()
                optimizer_vid.zero_grad()
                loss.backward()
                optimizer_base.step()
                optimizer_vid.step()
                logger.logg({'loss_vid_id': loss_id.data})
            
            
            # AIC dataset
            epoch_loss = 0
            for i, samples in enumerate(train_aic19_dataloader):
                if i==1: break
                imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                           samples['img'].size(2), 
                                           samples['img'].size(3),
                                           samples['img'].size(4))
                vid = samples['vid'].view(-1)
                cid = samples['cid'].view(-1)
                b_img = Variable(imgs).cuda()
                vid = Variable(vid).cuda()
                cid = Variable(cid).cuda()
                base_net.zero_grad()
                aic19_id_net.zero_grad()
                cams_net.zero_grad()
                # emb_net.zero_grad()
                #forward
                pred_feat, pred_emb = base_net(b_img)
                # pred_feat = base_net(b_img)
                pred_vid = aic19_id_net(pred_feat)
                pred_cid = cams_net(pred_feat)
                # pred_emb = emb_net(pred_feat)
                b_loss_triplet = criterion_triplet(pred_feat, vid)
                b_loss_triplet_embs = criterion_triplet(pred_emb, vid)
                loss_vid = criterion_ce(pred_vid, vid)
                loss_cid = criterion_ce(pred_cid, cid)
                loss = b_loss_triplet.mean() + b_loss_triplet_embs.mean() + loss_vid + loss_cid
                epoch_loss += loss.data
                # backward
                optimizer_base.zero_grad()
                optimizer_aic19.zero_grad()
                # optimizer_emb.zero_grad()
                optimizer_cid.zero_grad()
                loss.backward()
                optimizer_base.step()
                optimizer_aic19.step()
                # optimizer_emb.step()
                optimizer_cid.step()
    
                logger.logg({'loss_aic19_triplet': b_loss_triplet.data.mean(),
                            'loss_aic19_triplet_max': b_loss_triplet.data.max(),
                            'loss_aic19_triplet_min': b_loss_triplet.data.min(),
                            'loss_aic19_triplet_embs_max': b_loss_triplet_embs.data.max(),
                            'loss_aic19_triplet_embs_min': b_loss_triplet_embs.data.min(),
                            'loss_aic19_vid': loss_vid.data,
                            'loss_aic19_cid': loss_cid.data})

            pbar.update(1)
            pbar.set_postfix({'loss':'%f'%(epoch_loss/(n+1))})
        pbar.close()
        print('base_net lr: {:4f}; Training total loss = {:5f}\n'.format(optimizer_base.lr, epoch_loss/epoch_size))
        
        if e % args.save_every_n_epoch == 0:
            torch.save(base_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_base.ckpt'%(e, args.model)))
            torch.save(veri_id_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_veri_id.ckpt'%(e, args.model)))
            torch.save(aic19_id_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_aic19_id.ckpt'%(e, args.model)))
            torch.save(cams_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_cams.ckpt'%(e, args.model)))
            # torch.save(emb_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_emb.ckpt'%(e, args.model)))
            torch.save(compcars_model_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_compcars_model.ckpt'%(e, args.model)))
            torch.save(color_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_color.ckpt'%(e, args.model)))
            torch.save(vid_id_net.state_dict(),os.path.join(args.save_model_dir,'ep%d_model_%s_boxcars.ckpt'%(e, args.model)))
        logger.write_log()

        print('start validation')
        val_logger.append_epoch(e)
        base_net.eval()
        veri_id_net.eval()
        color_net.eval()
        aic19_id_net.eval()
        cams_net.eval()
        # emb_net.eval()
        compcars_model_net.eval()
        vid_id_net.eval()
        
        # VeRi
        correct = []
        for i,sample in enumerate(val_veri_dataloader):
            imgs = sample['img'].view(sample['img'].size(0)*sample['img'].size(1),
                                       sample['img'].size(2), 
                                       sample['img'].size(3),
                                       sample['img'].size(4))
            classes = sample['class'].view(sample['class'].size(0)*sample['class'].size(1))
            img = Variable(imgs,volatile=True).cuda()
            gt = Variable(classes,volatile=True).cuda()
            pred_feat,_ = base_net(img)
            # pred_feat = base_net(img)
            pred = veri_id_net(pred_feat)
            _, pred_cls = torch.max(pred,dim=1)
            correct.append(pred_cls.data==gt.data)
        acc = torch.cat(correct).float().mean()
        print('VeRi ID val acc: %.3f' % acc)
        val_logger.logg({'veri_id_acc':acc})
        
        '''
        # Compcars
        correct_model = []
        correct_color = []
        for i,sample in enumerate(val_compcars_dataloader):
            img = Variable(sample['img'],volatile=True).cuda()
            gt_model = Variable(sample['model'],volatile=True).cuda()
            gt_color = Variable(sample['color'],volatile=True).cuda()
            pred_feat,_ = base_net(img)
            # pred_feat = base_net(img)
            pred_model = compcars_model_net(pred_feat)
            pred_color = color_net(pred_feat)
            _, pred_model = torch.max(pred_model,dim=1)
            _, pred_color = torch.max(pred_color,dim=1)
            correct_model.append(pred_model.data == gt_model.data)
            correct_color.append(pred_color.data == gt_color.data)
        acc_model = torch.cat(correct_model).float().mean()
        acc_color = torch.cat(correct_color).float().mean()
        print('CompCars model val acc: %.3f' % acc_model)
        print('CompCars color val acc: %.3f' % acc_color)
        val_logger.logg({'compcars_model_acc':acc_model})
        val_logger.logg({'compcars_color_acc':acc_color})
        
        # Vid
        correct_id = []
        for i,sample in enumerate(val_vid_dataloader):
            imgs = sample['img'].view(sample['img'].size(0)*sample['img'].size(1),
                                       sample['img'].size(2), 
                                       sample['img'].size(3),
                                       sample['img'].size(4))
            classes = sample['id'].view(sample['id'].size(0)*sample['id'].size(1))
            img = Variable(imgs,volatile=True).cuda()
            gt_id = Variable(classes,volatile=True).cuda()
            pred_feat,_ = base_net(img)
            # pred_feat = base_net(img)
            pred_id =vid_id_net(pred_feat)
            _, pred_id = torch.max(pred_id,dim=1)
            correct_id.append(pred_id.data == gt_id.data)
        acc_id = torch.cat(correct_id).float().mean()
        print('Vid id val acc: %.3f' % acc_id)
        val_logger.logg({'vid_id_acc':acc_id})
        val_logger.write_log()
        '''

        if e%25 == 0:
            print('start testing')
            test_logger.append_epoch(e)
            '''
            pbar = tqdm(total=len(test_vid_dataloader),ncols=100,leave=True)
            pbar.set_description('Test BoxCar')
            correct_model = []
            for i,sample in enumerate(test_vid_dataloader):
                img = Variable(sample['img'],volatile=True).cuda()
                gt_model = Variable(sample['model'],volatile=True).cuda()
                pred_feat,_ = base_net(img)
                # pred_feat = base_net(img)
                pred_model =vid_id_net(pred_feat)
                _, pred_model = torch.max(pred_model,dim=1)
                correct_model.append(pred_model.data == gt_model.data)
                pbar.update(1)
            pbar.close()
            acc_model = torch.cat(correct_model).float().mean()
            print('BoxCars model val acc: %.3f' % acc_model)
            test_logger.logg({'vid_model_acc':acc_model})
            '''
            pbar = tqdm(total=len(test_compcars_dataloader),ncols=100,leave=True)
            pbar.set_description('Test CompCar_sv')
            correct_model = []
            correct_color = []
            for i,sample in enumerate(test_compcars_dataloader):
                img = Variable(sample['img'],volatile=True).cuda()
                gt_model = Variable(sample['model'],volatile=True).cuda()
                gt_color = Variable(sample['color'],volatile=True).cuda()
                pred_feat,_ = base_net(img)
                # pred_feat = base_net(img)
                pred_model = compcars_model_net(pred_feat)
                pred_color = color_net(pred_feat)
                _, pred_model = torch.max(pred_model,dim=1)
                _, pred_color = torch.max(pred_color,dim=1)
                correct_model.append(pred_model.data == gt_model.data)
                correct_color.append(pred_color.data == gt_color.data)
                pbar.update(1)
            pbar.close()
            acc_model = torch.cat(correct_model).float().mean()
            acc_color = torch.cat(correct_color).float().mean()
            print('CompCars model val acc: %.3f' % acc_model)
            print('CompCars color val acc: %.3f' % acc_color)
            test_logger.logg({'compcars_model_acc':acc_model})
            test_logger.logg({'compcars_color_acc':acc_color})
            test_logger.write_log()

        base_net.train()
        aic19_id_net.train()
        cams_net.train()
        # emb_net.train()
        veri_id_net.train()
        color_net.train()
        compcars_model_net.train()
        vid_id_net.train()

if __name__ == '__main__':
    ## Parse arg
    parser = argparse.ArgumentParser(description='Train Re-ID net', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--veri_txt', required=True, help='txt for VeRi dataset')
    parser.add_argument('--compcars_txt', required=True, help='txt for CompCars sv dataset')
    parser.add_argument('--compcars_test_txt', required=True, help='txt for CompCars sv dataset')
    parser.add_argument('--vid_txt', required=True, help='txt for BoxCars116k dataset')
    parser.add_argument('--aic19_txt', required=True, help='txt for AIC19 dataset')
    parser.add_argument('--crop',type=bool,default=True,help='Whether crop the images')
    parser.add_argument('--flip',type=bool,default=True,help='Whether randomly flip the image')
    parser.add_argument('--jitter',type=int,default=0,help='Whether randomly jitter the image')
    parser.add_argument('--pretrain',type=bool,default=True,help='Whether use pretrained model')
    parser.add_argument('--lr',type=float,default=0.001,help='learning rate')
    parser.add_argument('--batch_size',type=int,default=128,help='batch size number')
    parser.add_argument('--decay_start_iteration',dest ='decay_start_iteration', default = 1000, type=int, help='At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.')
    parser.add_argument('--n_epochs',type=int,default=20,help='number of training epochs')
    parser.add_argument('--model', dest = 'model', help='model', default='mobilenetv2', type=str)
    parser.add_argument('--save_model_dir',default=None,help='path to save model')
    parser.add_argument('--n_layer',type=int,default=18,help='number of Resnet layers')
    parser.add_argument('--margin',type=str,default='0',help='margin of triplet loss ("soft" or float)')
    parser.add_argument('--resume', dest = 'resume', default=None, type=str, help='The resume point for continuous training')
    parser.add_argument('--class_in_batch',type=int,default=32,help='# of class in a batch for triplet training')
    parser.add_argument('--image_per_class_in_batch',type=int,default=4,help='# of images of each class in a batch for triplet training')
    parser.add_argument('--batch_hard',action='store_true',help='whether to use batch_hard for triplet loss')
    parser.add_argument('--save_every_n_epoch',type=int,default=1,help='save model every n epoch')
    parser.add_argument('--class_w',type=float,help='wieghting of classification loss when triplet training')
    parser.add_argument('--ngpu', dest = 'ngpu', default = 1, type=int, help='number of gpus to use')
    args = parser.parse_args()
    ngpu = args.ngpu
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
    margin = args.margin if args.margin=='soft' else float(args.margin)
    assert args.class_in_batch*args.image_per_class_in_batch == args.batch_size, \
           'batch_size need to equal class_in_batch*image_per_class_in_batch'

    # Get Dataset & DataLoader    
    veri_dataset = TripletImage_Dataset(args.veri_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.1,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
    train_veri_dataloader = Get_train_DataLoader(veri_dataset, batch_size=args.class_in_batch)
    val_veri_dataloader = Get_val_DataLoader(veri_dataset,batch_size=args.class_in_batch)

    compcars_dataset = sv_comp_Dataset(args.compcars_txt, crop=args.crop, flip=args.flip, jitter=args.jitter,
                                       imagenet_normalize=args.pretrain, val_split=0.1)
    train_compcars_dataloader = Get_train_DataLoader(compcars_dataset, batch_size=args.batch_size)
    val_compcars_dataloader = Get_val_DataLoader(compcars_dataset, batch_size=args.batch_size)

    vid_dataset = VehicleID_Dataset(args.vid_txt, crop=args.crop, flip=args.flip, jitter=args.jitter,
                                  imagenet_normalize=args.pretrain, val_split=0.1) 
    train_vid_dataloader = Get_train_DataLoader(vid_dataset, batch_size=args.batch_size) 
    val_vid_dataloader = Get_val_DataLoader(vid_dataset, batch_size=args.batch_size)

    aic19_dataset = AIC19_Dataset(args.aic19_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.1,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
    train_aic19_dataloader = Get_train_DataLoader(aic19_dataset, batch_size=args.class_in_batch)

    # Test Dataset & loader
    compcars_dataset_test = sv_comp_Dataset(args.compcars_test_txt, crop=False, flip=False, jitter=False,imagenet_normalize=True)
    test_compcars_dataloader = Get_test_DataLoader(compcars_dataset_test, batch_size=args.batch_size)

    # vid_dataset = BoxCars_Dataset(args.vid_test_txt, crop=False, flip=False, jitter=False, imagenet_normalize=True) 
    # test_vid_dataloader = Get_test_DataLoader(vid_dataset, batch_size=args.batch_size) 

    # Get Model
    #base_net = models.FeatureResNet(n_layers=args.n_layer, pretrained=args.pretrain)
    #base_net = embvgg_bn(model_name = 'vgg16', pretrained=False, num_classes = 333, with_top = False)
    #base_net = DenseNet(num_classes=333, dims = 128, with_top = False)
    base_net = ResNeXt(num_classes=333, dims = 128, with_top = False)
    veri_id_net = models.NLayersFC(base_net.output_dim, veri_dataset.n_id)
    color_net = models.NLayersFC(base_net.output_dim, 12)
    compcars_model_net = models.NLayersFC(base_net.output_dim, compcars_dataset.n_models)
    vid_id_net = models.NLayersFC(base_net.output_dim, vid_dataset.n_id)
    
    aic19_id_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_id)
    # emb_net = models.NLayersFC(base_net.output_dim, 128)# the embedding features
    cams_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_cid)
    
    with torch.cuda.device(0):
        base_net = base_net.cuda()
        aic19_id_net = aic19_id_net.cuda()
        cams_net = cams_net.cuda()
        veri_id_net = veri_id_net.cuda()
        color_net = color_net.cuda()
        compcars_model_net = compcars_model_net.cuda()
        vid_id_net = vid_id_net.cuda()

    if (device.type == 'cuda') and (ngpu > 1):
        base_net = nn.DataParallel(base_net, list(range(ngpu)))
        vid_id_net = nn.DataParallel(vid_id_net, list(range(ngpu)))
        aic19_id_net = nn.DataParallel(aic19_id_net, list(range(ngpu)))
        cams_net = nn.DataParallel(cams_net, list(range(ngpu)))
        veri_id_net = nn.DataParallel(veri_id_net, list(range(ngpu)))
        color_net = nn.DataParallel(color_net, list(range(ngpu)))
        compcars_model_net = nn.DataParallel(compcars_model_net, list(range(ngpu)))

    if args.save_model_dir !=  None:
        os.system('mkdir -p %s' % os.path.join(args.save_model_dir))
    
    if args.resume is not None:
        state_dict = torch.load(args.resume)
        if 'epoch' in state_dict:
            state_dict = state_dict['model_state_dict']
        #for key in list(state_dict.keys()):
        #    if key in set(list(['fc','embed','fc_head'])):
        #        del state_dict[key]
        base_net.load_state_dict(state_dict,strict=False)

    # Train
    train_joint(args, train_veri_dataloader, val_veri_dataloader, \
                train_compcars_dataloader, val_compcars_dataloader, \
                train_vid_dataloader, val_vid_dataloader, \
                train_aic19_dataloader, \
                test_compcars_dataloader,\
                base_net, veri_id_net, color_net,
                aic19_id_net, cams_net,
                compcars_model_net,
                vid_id_net)