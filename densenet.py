import torch
import torch.nn as nn
import torch.nn.functional as F
from collections import OrderedDict

__all__ = ['densenet']

cfg = {'121': dict(num_init_features=64, growth_rate=32, block_config=(6, 12, 24, 16)),
       '169': dict(num_init_features=64, growth_rate=32, block_config=(6, 12, 32, 32)),
       '201': dict(num_init_features=64, growth_rate=32, block_config=(6, 12, 48, 32)),
       '161': dict(num_init_features=96, growth_rate=48, block_config=(6, 12, 36, 24))}


def init_model(model):
    # Official init from torch repo.
    for m in model.modules():
        if isinstance(m, nn.Conv2d):
            nn.init.kaiming_normal_(m.weight)
        elif isinstance(m, nn.BatchNorm2d):
            nn.init.constant_(m.weight, 1)
            nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.Linear):
            nn.init.constant_(m.bias, 0)


def weight_decay_config(value=1e-4, log=False):
    return {'name': 'WeightDecay',
            'value': value,
            'log': log,
            'filter': {'parameter_name': lambda n: not n.endswith('bias'),
                       'module': lambda m: not isinstance(m, nn.BatchNorm2d)}
            }


class _DenseLayer(nn.Sequential):
    def __init__(self, num_input_features, growth_rate, bn_size, drop_rate):
        super(_DenseLayer, self).__init__()
        self.add_module('norm1', nn.BatchNorm2d(num_input_features)),
        self.add_module('relu1', nn.ReLU(inplace=True)),
        self.add_module('conv1', nn.Conv2d(num_input_features, bn_size *
                                           growth_rate, kernel_size=1, stride=1, bias=False)),
        self.add_module('norm2', nn.BatchNorm2d(bn_size * growth_rate)),
        self.add_module('relu2', nn.ReLU(inplace=True)),
        self.add_module('conv2', nn.Conv2d(bn_size * growth_rate, growth_rate,
                                           kernel_size=3, stride=1, padding=1, bias=False)),
        self.drop_rate = drop_rate

    def forward(self, x):
        new_features = super(_DenseLayer, self).forward(x)
        if self.drop_rate > 0:
            new_features = F.dropout(
                new_features, p=self.drop_rate, training=self.training)
        return torch.cat([x, new_features], 1)


class _DenseBlock(nn.Sequential):
    def __init__(self, num_layers, num_input_features, bn_size, growth_rate, drop_rate):
        super(_DenseBlock, self).__init__()
        for i in range(num_layers):
            layer = _DenseLayer(num_input_features + i *
                                growth_rate, growth_rate, bn_size, drop_rate)
            self.add_module('denselayer%d' % (i + 1), layer)


class _Transition(nn.Sequential):
    def __init__(self, num_input_features, num_output_features):
        super(_Transition, self).__init__()
        self.add_module('norm', nn.BatchNorm2d(num_input_features))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv2d(num_input_features, num_output_features,
                                          kernel_size=1, stride=1, bias=False))
        self.add_module('pool', nn.AvgPool2d(kernel_size=2, stride=2))


class DenseNet(nn.Module):
    r"""Densenet-BC model class, based on
    `"Densely Connected Convolutional Networks" <https://arxiv.org/pdf/1608.06993.pdf>`_

    Args:
        growth_rate (int) - how many filters to add each layer (`k` in paper)
        block_config (list of 4 ints) - how many layers in each pooling block
        num_init_features (int) - the number of filters to learn in the first convolution layer
        bn_size (int) - multiplicative factor for number of bottle neck layers
          (i.e. bn_size * k features in the bottleneck layer)
        drop_rate (float) - dropout rate after each dense layer
        num_classes (int) - number of classification classes
    """

    @staticmethod
    def _create_features(num_features):
        # First convolution
        return nn.Sequential(OrderedDict([
            ('conv0', nn.Conv2d(3, num_features,
                                kernel_size=7, stride=2, padding=3, bias=False)),
            ('norm0', nn.BatchNorm2d(num_features)),
            ('relu0', nn.ReLU(inplace=True)),
            ('pool0', nn.MaxPool2d(kernel_size=3, stride=2, padding=1)),
        ]))

#    def __init__(self, growth_rate=32, block_config=(6, 12, 24, 16),
#                 num_init_features=64, bn_size=4, drop_rate=0, num_classes=1000, checkpoint_segments=0):
    def __init__(self, n_layers = 121,  bn_size=4, drop_rate=0, num_classes=1000, dims = 128, with_top = False):
        super(DenseNet, self).__init__()
        
        num_init_features = cfg[str(n_layers)].get('num_init_features')
        growth_rate = cfg[str(n_layers)].get('growth_rate')
        block_config = cfg[str(n_layers)].get('block_config')
        
        self.features = self._create_features(num_init_features)
        # Each denseblock
        num_features = num_init_features
        for i, num_layers in enumerate(block_config):
            block = _DenseBlock(num_layers=num_layers, num_input_features=num_features,
                                bn_size=bn_size, growth_rate=growth_rate, drop_rate=drop_rate)
            self.features.add_module('denseblock%d' % (i + 1), block)
            num_features = num_features + num_layers * growth_rate
            if i != len(block_config) - 1:
                trans = _Transition(
                    num_input_features=num_features, num_output_features=num_features // 2)
                self.features.add_module('transition%d' % (i + 1), trans)
                num_features = num_features // 2

        # Final batch norm
        self.features.add_module('norm5', nn.BatchNorm2d(num_features))
        self.with_top = with_top
        if with_top:
        # Linear layer
            self.classifier = nn.Linear(num_features, num_classes)
        self.embed = nn.Linear(num_features, dims)
        self.output_dim = num_features
        
        init_model(self)

    def forward(self, x):
        features = self.features(x)
        x = F.relu(features, inplace=True)
        x = F.avg_pool2d(x, kernel_size=7, stride=1).view(
            features.size(0), -1)
        embs = self.embed(x)
        if self.with_top:
            fc = self.classifier(x)
            return fc, x, embs
        else:
            return x, embs
        
if __name__ == '__main__':
    model = DenseNet(n_layers = 121, with_top = True)
    print(model)
    in_ten = torch.randn(32, 3, 224, 224)
    feat, embs, output = model(in_ten)
    print(feat[0].size())
    print(embs[0].size())
    print(feat[2].size())