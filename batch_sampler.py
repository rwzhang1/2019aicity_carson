#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 25 16:13:50 2019

@author: rwzhang
"""

import torch
from torch.utils.data import Dataset, DataLoader
from torch.utils.data.sampler import Sampler
import cv2
import numpy as np
import random,logging, sys, os
from preprocess_data import AICData, convert_xml
from sklearn.utils import shuffle

class BatchSampler(Sampler):
    '''
    sampler used in dataloader. method __iter__ should output the indices each time it is called
    '''
    def __init__(self, dataset, n_class, n_num, *args, **kwargs):
        super(BatchSampler, self).__init__(dataset, *args, **kwargs)
        self.n_class = n_class
        self.n_num = n_num
        self.batch_size = n_class * n_num
        self.dataset = dataset
        self.labels = np.array(dataset.lb_ids)
        self.labels_uniq = np.array(list(dataset.lb_ids_uniq))
        self.len = len(dataset) // self.batch_size
        self.lb_img_dict = dataset.lb_img_dict
        self.iter_num = len(self.labels_uniq) // self.n_class
        print('batch sampler iter_num: {}'.format(self.iter_num))
    def __iter__(self):
        curr_p = 0
        np.random.shuffle(self.labels_uniq)
        for k, v in self.lb_img_dict.items():
            random.shuffle(self.lb_img_dict[k])
        for i in range(self.iter_num):
            label_batch = self.labels_uniq[curr_p: curr_p + self.n_class]
            curr_p += self.n_class
            idx = []
            for lb in label_batch:
                if len(self.lb_img_dict[lb]) > self.n_num:
                    idx_smp = np.random.choice(self.lb_img_dict[lb],
                            self.n_num, replace = False)
                else:
                    idx_smp = np.random.choice(self.lb_img_dict[lb],
                            self.n_num, replace = True)
                idx.extend(idx_smp.tolist())
            yield idx

    def __len__(self):
        return self.iter_num

class BatchSamplerWithCam(Sampler):
    '''
    sampler used in dataloader. method __iter__ should output the indices each time it is called
    '''
    def __init__(self, dataset, n_class, n_num, n_cams, *args, **kwargs):
        super(BatchSamplerWithCam, self).__init__(dataset, *args, **kwargs)
        self.n_class = n_class
        self.n_num = n_num
        self.n_cams = n_cams
        self.batch_size = n_class * n_num * n_cams
        
        self.dataset = dataset
        self.len = len(dataset) // self.batch_size
        self.labels = dataset.lb_array
        self.labels_uniq = np.array(list(dataset.lb_ids_uniq))
        self.cams = dataset.cams_array
        self.cams_uniq = np.array(list(dataset.cam_ids_uniq))
        self.lb_img_dict = dataset.lb_img_dict
        self.cam_img_dict = dataset.cam_img_dict
        
        self.tuple_df = dataset.tuple_df
        self.img_list = dataset.img_list
        self.iter_num = len(self.labels_uniq) // self.n_class

    def __iter__(self):
        curr_p = 0
        np.random.shuffle(self.labels_uniq)
        np.random.shuffle(self.cams_uniq)
        for k, v in self.lb_img_dict.items():
            random.shuffle(self.lb_img_dict[k])

        for i in range(self.iter_num):
            label_batch = self.labels_uniq[curr_p: curr_p + self.n_class]
            curr_p += self.n_class
            idx = []
            for lb in label_batch:
                cam_in_lb = self.tuple_df.loc[lb].index.unique()
                idx_smp = []
                if len(cam_in_lb) > self.n_cams:
                    cams_smp = np.random.choice(cam_in_lb, self.n_cams)
                    for cam_id in cams_smp:
                        if len(self.tuple_df.loc[(lb, cam_id), 'imageName']) > self.n_num:
                            imgs_smp = self.tuple_df.loc[(lb, cam_id), 'imageName'].sample(self.n_num).tolist()
                            idx_smp +=[self.img_list.index(x) for x in imgs_smp]
                        else:
                            imgs_smp = self.tuple_df.loc[(lb, cam_id), 'imageName'].sample(self.n_num, replace=True).tolist()
                            idx_smp +=[self.img_list.index(x) for x in imgs_smp]
                else:
                    print('[Warning:]For iteration:{}/{}, CamID number less than self.n_cams for label: {}'.format(i,self.iter_num,lb))
                    cams_smp = np.random.choice(cam_in_lb, self.n_cams, replace=True)
                    idx_smp = np.random.choice(self.lb_img_dict[lb], self.n_num*self.n_cams, replace = True)
#                idx.extend(idx_smp.tolist())
                idx.extend(idx_smp)
#            print('[Warning:]For iteration:{}, total of: {}, sampled index label: {}'.format(i, len(idx), idx))
            yield idx

    def __len__(self):
        return self.iter_num

if __name__ == "__main__":
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    train_xml = root_dir + 'aic19-track2-reid/train_label.xml'
    train_img_path = root_dir + 'aic19-track2-reid/image_train/'
    cam2img_df = convert_xml(train_xml)
    cam2img_df = shuffle(cam2img_df, random_state = 10086)
    ds = AICData(train_img_path, cam2img_df, 64, is_train = True)
    ds = AICData(train_img_path, cam2img_df, 64, is_train = True)

    vid_img_dir = 'DATASETS/VehicleID/image/'
    vid_img_list = 'DATASETS/VehicleID/train_test_split/train_list.txt'
    sampler = BatchSampler(ds, 18, 4) # each time train 18 classes, 4 categories
    samplerc = BatchSamplerWithCam(ds, 18, 4, 2) # each time train 18 classes, 4 categories
    dl = DataLoader(ds, batch_sampler = sampler, num_workers = 4)
    dlc = DataLoader(ds, batch_sampler = samplerc, num_workers = 4)
    import itertools
#
    diter = iter(dl)
    diterc = iter(dlc)
    visited_lb = set()
    visited_cams = set()
#    while True:
    for _ in range(sampler.iter_num):
#        ims, lbs, _, _ = next(diter)        
        ims, lbs, cams, _ = next(diterc)  
        for lb in lbs:
            visited_lb.add(lb.item())
        for cam in cams:
            visited_cams.add(cam.item())
    print('total visited labels', len(visited_lb), len(visited_cams))
    print('visited', visited_lb, visited_cams)
    print(len(list(ds.lb_ids_uniq)))