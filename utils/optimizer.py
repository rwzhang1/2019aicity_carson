#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 17:10:55 2019

@author: rwzhang
"""
import torch
from .logger import logger

class AdamOptimWrapper(object):
    '''
    A wrapper of Adam optimizer which allows to adjust the optimizing parameters
    according to the strategy presented in the paper
    Similar to keras.callbacks.LearningRateScheduler
    '''
    def __init__(self, params, lr, wd=0, t0=15000, t1=25000, resume_steps=0, *args, **kwargs):
        super(AdamOptimWrapper, self).__init__(*args, **kwargs)
        self.base_lr = lr
        self.wd = wd
        self.t0 = t0
        self.t1 = t1
        self.step_count = resume_steps
        self.optim = torch.optim.Adam(params, lr = self.base_lr, weight_decay = self.wd)


    def step(self):
        self.step_count += 1
        self.optim.step()
        # adjust optimizer parameters
        if self.step_count == self.t0:
            logger.info('On Fine Tune Epoch {} Begin'.format(self.t0))
            betas_old = self.optim.param_groups[0]['betas']
            self.optim.param_groups[0]['betas'] = (0.5, 0.999) # on epoch begin 
            betas_new = self.optim.param_groups[0]['betas']
            logger.info('==> changing adam betas from {} to {}'.format(betas_old, betas_new))
            logger.info('[INFO] On Fine Tune, start Learning Rate Decay')
        elif self.t0 < self.step_count < self.t1:
            lr = self.base_lr * (0.001 ** ((self.step_count + 1.0 - self.t0) / (self.t1 + 1.0 - self.t0)))
            for pg in self.optim.param_groups:
                pg['lr'] = lr
            self.optim.defaults['lr'] = lr

    def zero_grad(self):
        self.optim.zero_grad()

    @property
    def lr(self):
        return self.optim.param_groups[0]['lr']

class SGDOptimWrapper(object):
    '''
    A wrapper of SGD optimizer which allows to adjust the optimizing parameters
    according to the strategy presented in the paper
    Similar to keras.callbacks.LearningRateScheduler
    '''
    def __init__(self, params, lr, wd=0, t0=15000, t1=25000, resume_steps=0,*args, **kwargs):
        super(SGDOptimWrapper, self).__init__(*args, **kwargs)
        self.base_lr = lr
        self.wd = wd
        self.t0 = t0
        self.t1 = t1
        self.step_count = resume_steps
        self.optim = torch.optim.SGD(params, lr = self.base_lr, weight_decay = self.wd)


    def step(self):
        self.step_count += 1
        self.optim.step()
        # adjust optimizer parameters
        if self.step_count == self.t0:
            logger.info('On Fine Tune Epoch {} Begin'.format(self.t0))
            print('keys', self.optim.param_groups[0].keys())
            momentum_old = self.optim.param_groups[0]['momentum']
            self.optim.param_groups[0]['momentum'] = 0.9 # on epoch begin 
            momentum_new = self.optim.param_groups[0]['momentum']
            logger.info('==> changing SGD momentum from {} to {}'.format(momentum_old, momentum_new))
            logger.info('[INFO] On Fine Tune, start Weight Decay and Learning Rate Decay')
        elif self.t0 < self.step_count < self.t1:
            lr = self.base_lr * (0.1 ** ((self.step_count + 1.0 - self.t0) / (self.t1 + 1.0 - self.t0)))
            logger.info('On Fine Tune Epoch {}, current learning rate: {}'.format(self.step_count, lr))
            for pg in self.optim.param_groups:
                pg['lr'] = lr
            self.optim.defaults['lr'] = lr

    def zero_grad(self):
        self.optim.zero_grad()

    @property
    def lr(self):
        return self.optim.param_groups[0]['lr']
