#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 17:21:34 2019

@author: rwzhang
"""

import time
import logging
import os

def get_logger(log_path = 'log/soft_trip'):
    #TODO: Need to add model and run information to the logger
#    if not os.path.exists('log/soft_trip'):
#        os.makedirs('log/soft_trip')
    if not os.path.exists(log_path): os.makedirs(log_path)
    FORMAT = '%(levelname)s %(filename)s(%(lineno)d): %(message)s'
    logfile = 'TRAINING-{}.log'.format(time.strftime('%Y-%m-%d-%H-%M-%S'))
    logfile = os.path.join(log_path, logfile)
    logging.basicConfig(level=logging.INFO, format=FORMAT, filename=logfile)
    logger = logging.getLogger(__name__)
    logger.addHandler(logging.StreamHandler())
    return logger


logger = get_logger()