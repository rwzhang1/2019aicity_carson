# Adaptive Feature Learning CNN

Refer to: 
Chih-Wei Wu, Chih-Ting Liu, Chen-En Jiang, Wei-Chih Tu, Shao-Yi Chien "Vehicle Re-Identification with the Space-Time Prior" CVPRW, 2018.  

Please follow the steps below for training multiple dataset together. 
1. Download and extract [VeRi](https://github.com/VehicleReId/VeRidataset), [CompCars](http://mmlab.ie.cuhk.edu.hk/datasets/comp_cars/index.html), [BoxCars116k](https://medusa.fit.vutbr.cz/traffic/research-topics/fine-grained-vehicle-recognition/
boxcars-improving-vehicle-fine-grained-recognition-using-3d-bounding-boxes-in-traffic-surveillance/) dataset, [VehicleID](https://www.pkuml.org/resources/pku-vehicleid.html), [AIC19](https://www.aicitychallenge.org/)
2. Run the following script to setup training:
```
bash setup.sh <VeRi_dir> <AIC19_dir> <CompCars_DIR> <BoxCars116k_DIR> <VID_dir>
```
3. Now, we are ready to train! Train the model by:
```
python train_joint.py --veri_txt 'database/VeRi_train_info.txt' --compcars_txt 'database/Comp_info/Comp_sv_train.txt' --compcars_test_txt 'database/Comp_info/Comp_sv_test.txt' --boxcars_txt 'database/BoxCars_train.txt' --boxcars_test_txt 'database/BoxCars_test.txt' --aic19_txt 'database/AIC19_train_info.txt'  --n_epochs 1501 --save_model_dir ./ckpt --lr 1e-3 --save_every_n_epoch 50 --class_w 1 --batch_size 128 --class_in_batch 16 --image_per_class_in_batch 8 --decay_start_iteration 2500 --model 'mobilenet' --ngpu 3
```
The model will be in `./ckpt`

## Train on VeRi Dataset

* Train classification model with VeRi or VeRi\_ict dataset
```
python3 train.py --info VeRi_train_info.txt --save_model_dir ./ckpt --lr 0.001 --batch_size 64 --n_epochs 20 --n_layer 18 --dataset VeRi
```

* Train triplet model with VeRi dataset
```
python train.py --info database/VeRi_train_info.txt --n_epochs 1501 --save_model_dir ./ckpt_mobile_veritrip_droplast --model 'mobilenet' --margin soft --class_in_batch 18 --batch_size 72 --triplet --lr 0.0006 --batch_hard --save_every_n_epoch 50 --decay_start_iteration 1000 --ngpu 3 --with_class
```

* Other training options
```
  --info                txt file contain path of data (default:train_info.txt)
  --crop                Whether crop the images (default: True)
  --flip                Whether randomly flip the image (default: True)
  --jitter              Whether randomly jitter the image (default: 0)
  --pretrained          Whether use pretrained model (default: True)
  --lr                  learning rate (default: 0.001)
  --batch_size          batch size number (default: 64)
  --n_epochs            number of training epochs (default: 20)
  --model               model for base net (default: mobilenetv2)
  --decay_start_iteration  At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.(default: 2000)
  --load_ckpt           path to load ckpt (default: None)
  --save_model_dir      path to save model (default: None)
  --n_layer             number of Resnet layers (default: 50)
  --dataset             which dataset:VeRi or VeRi_ict (default: VeRi_ict)
  --triplet             use triplet training (default: False)
  --unsupervised        use unsupervised triplet training (default: False)
  --with_class          whether to train with class label during triplet training (default: False)
  --margin              margin of triplet loss ("soft" or float) (default: 0)
  --class_in_batch      # of class in a batch for triplet training (default:32)
  --image_per_class_in_batch # of images of each class in a batch for triplet training (default: 4)
  --batch_hard          whether to use batch_hard for triplet loss (default: False)
  --save_every_n_epoch  save model every n epoch (default: 1)
  --class_w             weighting of classification loss when triplet training(default: 0.8)
  --ngpu                number of gpus to use (default: 1)
  --weight_decay        weight decay (default: 1e-4) (default: 4e-05)
  --optimizer           optimizer function used (default: Adam)
  --momentum            momentum (default: 0.9)
```
## Evaluate CNN on VeRi

1. dump distance matrix and generate embeddings
```
python compute_VeRi_dis.py --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --ngpu 3 --load_ckpt 'ckpt_mobile2_veritrip_noNRjitter/model_mobilenetv2_1500.ckpt' --dis_mat 'mobile2_1500_noNRjitter.mat' --model_name 'mobilenetv2' --n_layer 50 --tencrop True --batch_size 4 --emb_pkl 'mobile2_1500_noNRjitter.pkl'

optional arguments:
  --load_ckpt           path to load ckpt (default: None)
  --model_name          model to reload for inferencing (default: mobilenet)
  --n_layer             number of Resnet layers (default: 50)
  --gallery_txt         path to load gallery (default: None)
  --query_txt           path to load query (default: None)
  --dis_mat             path to store distance (default: None)
  --batch_size          batch size number (default: 64)

  --tencrop             whether to tencrop for inferencing (default: False)
  --save_path           the save path for query/test embeddings (default: veri_embs)
  --emb_pkl             path to store reranked distance (default: None)
  --feat_op             using feature or embeddings (default: embs)
  --ngpu                number of gpus to use (default: 1)
  --info                txt file contain path of data (default: database/VeRi_train_info.txt)
```

2. compute cmc curve
```  
  1. open matlab in the "VeRi_cmc/" directory
  2. open "baseline_evaluation_FACT_776.m" file
  3. change "dis_CNN" mat path, "gt_index",  "jk_index" txt file path
  4. run and get plot
```

## Reference
* NVIDIA AI City Challenge. https://www.aicitychallenge.org/, 2019.
* X. Liu, W. Liu, H. Ma, and H. Fu. Large-scale vehicle reidentification in urban surveillance videos. ICME, 2016.
* X. Liu, W. Liu, T. Mei, and H. Ma. A deep learning-based approach to progressive vehicle re-identification for urban surveillance. ECCV, 2016
* L. Yang, P. Luo, C. C. Loy, and X. Tang. A large-scale car dataset for fine-grained categorization and verification. CVPR, 2015.
* J. Sochor, J. pahel, and A. Herout. Boxcars: Improving finegrained recognition of vehicles using 3-d bounding boxes in traffic surveillance. IEEE Transactions on Intelligent Transportation Systems, PP(99):1–12, 2018.

## Citing

```
@inproceedings{wu2018vreid,
  title={Vehicle Re-Identification with the Space-Time Prior},
  author={Wu, Chih-Wei and Liu, Chih-Ting and Jiang, Chen-En and Tu, Wei-Chih and Chien, Shao-Yi},
  booktitle={IEEE Conference on Computer Vision and Pattern Recognition (CVPR) Workshop},
  year={2018},
}
```
