import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.backends.cudnn as cudnn
from logger import Logger
import sys
from tqdm import tqdm
import argparse
import sys
import os
import numpy as np
sys.path.append('..')
import models
from gen_aic_veri import VReID_Dataset, TripletImage_Dataset, sv_comp_Dataset, BoxCars_Dataset, AIC19_Dataset
from gen_aic_veri import Get_train_DataLoader, Get_val_DataLoader, Unsupervised_TripletImage_Dataset
from utils.loss import Soft_TripletLoss as TripletLoss

cudnn.benchmark=True

def train_joint(args, 
                train_compcars_dataloader, val_compcars_dataloader, 
                train_aic_dataloader, 
                test_compcars_dataloader,
                base_net, color_net, 
                aic19_id_net, cams_net, emb_net, 
                compcars_model_net):

    optimizer_base = optim.Adam(base_net.parameters(), lr=args.lr)
    optimizer_color = optim.Adam(color_net.parameters(), lr=args.lr)
    optimizer_aic19 = optim.Adam(aic19_id_net.parameters(), lr=args.lr)
    optimizer_cid = optim.Adam(cams_net.parameters(),lr=args.lr)
    optimizer_emb = optim.Adam(emb_net.parameters(), lr=args.lr)
    optimizer_compcars = optim.Adam(compcars_model_net.parameters(), lr=args.lr)
    criterion_triplet = TripletLoss(margin=margin, batch_hard=args.batch_hard)
    criterion_ce = nn.CrossEntropyLoss()
    logger = Logger(os.path.join(args.save_model_dir,'train'))
    val_logger = Logger(os.path.join(args.save_model_dir,'val'))
    test_logger = Logger(os.path.join(args.save_model_dir,'test'))
    
    epoch_size = min(len(train_compcars_dataloader), len(train_aic_dataloader))
    for e in range(args.n_epochs):
        
        pbar = tqdm(total=epoch_size,ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))
        
        for n in range(epoch_size):
            logger.append_epoch(e + float(n)/epoch_size)
            # VeRi dataset
            epoch_loss = 0
            # Compcars dataset
            for i, samples in enumerate(train_compcars_dataloader):
                if i==1: break
                img = Variable(samples['img']).cuda()
                model = Variable(samples['model']).cuda()
                color = Variable(samples['color']).cuda()
                base_net.zero_grad()
                compcars_model_net.zero_grad()
                color_net.zero_grad()
                #forward
                pred_feat = base_net(img)
                pred_model = compcars_model_net(pred_feat)
                pred_color = color_net(pred_feat)
                loss_model = criterion_ce(pred_model, model)
                loss_color = criterion_ce(pred_color, color)
                loss = loss_model + loss_color
                epoch_loss += loss.data
                # backward
                loss.backward()
                optimizer_base.step()
                optimizer_compcars.step()
                optimizer_color.step()
    
                logger.logg({'loss_compcars_model': loss_model.data,
                            'loss_compcars_color': loss_color.data})
            # AIC dataset
            epoch_loss = 0
            for i, samples in enumerate(train_aic19_dataloader):
                if i==1: break
                imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                           samples['img'].size(2), 
                                           samples['img'].size(3),
                                           samples['img'].size(4))
                vid = samples['vid'].view(-1)
                cid = samples['cid'].view(-1)
                b_img = Variable(imgs).cuda()
                vid = Variable(vid).cuda()
                cid = Variable(cid).cuda()
                base_net.zero_grad()
                aic19_id_net.zero_grad()
                cams_net.zero_grad()
                emb_net.zero_grad()
                #forward
                pred_feat = base_net(b_img)
                pred_vid = aic19_id_net(pred_feat)
                pred_cid = cams_net(pred_feat)
                pred_emb = emb_net(pred_feat)
                b_loss_triplet = criterion_triplet(pred_feat, vid)
                b_loss_triplet_embs = criterion_triplet(pred_emb, vid)
                loss_vid = criterion_ce(pred_vid, vid)
                loss_cid = criterion_ce(pred_cid, cid)
                loss = b_loss_triplet.mean() + b_loss_triplet_embs.mean() + loss_vid + loss_cid
                epoch_loss += loss.data
                # backward
                loss.backward()
                optimizer_base.step()
                optimizer_aic19.step()
                optimizer_emb.step()
                optimizer_cid.step()
    
                logger.logg({'loss_aic19_triplet': b_loss_triplet.data.mean(),
                            'loss_aic19_triplet_embs': b_loss_triplet_embs.data.mean(),
                            'loss_aic19_vid': loss_vid.data,
                            'loss_aic19_cid': loss_cid.data})

            pbar.update(1)
            pbar.set_postfix({'loss':'%f'%(epoch_loss/(n+1))})
        pbar.close()
        print('Training total loss = %f'%(epoch_loss/epoch_size))
        
        if e % args.save_every_n_epoch == 0:
            torch.save(base_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_base.ckpt'%(e)))
            torch.save(aic19_id_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_aic19_id.ckpt'%(e)))
            torch.save(cams_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_cams.ckpt'%(e)))
            torch.save(emb_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_emb.ckpt'%(e)))
            torch.save(compcars_model_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_compcars_model.ckpt'%(e)))
            torch.save(color_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_color.ckpt'%(e)))
        logger.write_log()

        print('start validation')
        val_logger.append_epoch(e)
        base_net.eval()
        color_net.eval()
        aic19_id_net.eval()
        cams_net.eval()
        emb_net.eval()

        # Compcars
        correct_model = []
        correct_color = []
        for i,sample in enumerate(val_compcars_dataloader):
            img = Variable(sample['img'],volatile=True).cuda()
            gt_model = Variable(sample['model'],volatile=True).cuda()
            gt_color = Variable(sample['color'],volatile=True).cuda()
            pred_feat = base_net(img)
            pred_model = compcars_model_net(pred_feat)
            pred_color = color_net(pred_feat)
            _, pred_model = torch.max(pred_model,dim=1)
            _, pred_color = torch.max(pred_color,dim=1)
            correct_model.append(pred_model.data == gt_model.data)
            correct_color.append(pred_color.data == gt_color.data)
        acc_model = torch.cat(correct_model).float().mean()
        acc_color = torch.cat(correct_color).float().mean()
        print('CompCars model val acc: %.3f' % acc_model)
        print('CompCars color val acc: %.3f' % acc_color)
        val_logger.logg({'compcars_model_acc':acc_model})
        val_logger.logg({'compcars_color_acc':acc_color})

        
        if e%25 == 0:
            print('start testing')
            test_logger.append_epoch(e)
            pbar = tqdm(total=len(test_compcars_dataloader),ncols=100,leave=True)
            pbar.set_description('Test CompCar_sv')
            correct_model = []
            correct_color = []
            for i,sample in enumerate(test_compcars_dataloader):
                img = Variable(sample['img'],volatile=True).cuda()
                gt_model = Variable(sample['model'],volatile=True).cuda()
                gt_color = Variable(sample['color'],volatile=True).cuda()
                pred_feat = base_net(img)
                pred_model = compcars_model_net(pred_feat)
                pred_color = color_net(pred_feat)
                _, pred_model = torch.max(pred_model,dim=1)
                _, pred_color = torch.max(pred_color,dim=1)
                correct_model.append(pred_model.data == gt_model.data)
                correct_color.append(pred_color.data == gt_color.data)
                pbar.update(1)
            pbar.close()
            acc_model = torch.cat(correct_model).float().mean()
            acc_color = torch.cat(correct_color).float().mean()
            print('CompCars model val acc: %.3f' % acc_model)
            print('CompCars color val acc: %.3f' % acc_color)
            test_logger.logg({'compcars_model_acc':acc_model})
            test_logger.logg({'compcars_color_acc':acc_color})
            test_logger.write_log()

        base_net.train()
        color_net.train()
        compcars_model_net.train()

if __name__ == '__main__':
    ## Parse arg
    parser = argparse.ArgumentParser(description='Train Re-ID net', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--veri_txt', required=True, help='txt for VeRi dataset')
    parser.add_argument('--compcars_txt', required=True, help='txt for CompCars sv dataset')
    parser.add_argument('--compcars_test_txt', required=True, help='txt for CompCars sv dataset')
    parser.add_argument('--boxcars_txt', required=True, help='txt for BoxCars116k dataset')
    parser.add_argument('--boxcars_test_txt', required=True, help='txt for BoxCars116k dataset')
    parser.add_argument('--aic19_txt', required=True, help='txt for AIC19 dataset')
    parser.add_argument('--crop',type=bool,default=True,help='Whether crop the images')
    parser.add_argument('--flip',type=bool,default=True,help='Whether randomly flip the image')
    parser.add_argument('--jitter',type=int,default=0,help='Whether randomly jitter the image')
    parser.add_argument('--pretrain',type=bool,default=True,help='Whether use pretrained model')
    parser.add_argument('--lr',type=float,default=0.001,help='learning rate')
    parser.add_argument('--batch_size',type=int,default=128,help='batch size number')
    parser.add_argument('--n_epochs',type=int,default=20,help='number of training epochs')
    parser.add_argument('--load_ckpt',default=None,help='path to load ckpt')
    parser.add_argument('--save_model_dir',default=None,help='path to save model')
    parser.add_argument('--n_layer',type=int,default=18,help='number of Resnet layers')
    parser.add_argument('--margin',type=str,default='0',help='margin of triplet loss ("soft" or float)')
    parser.add_argument('--class_in_batch',type=int,default=32,help='# of class in a batch for triplet training')
    parser.add_argument('--image_per_class_in_batch',type=int,default=4,help='# of images of each class in a batch for triplet training')
    parser.add_argument('--batch_hard',action='store_true',help='whether to use batch_hard for triplet loss')
    parser.add_argument('--save_every_n_epoch',type=int,default=1,help='save model every n epoch')
    parser.add_argument('--class_w',type=float,help='wieghting of classification loss when triplet training')
    parser.add_argument('--ngpu', default=3, type=int, help='number of GPUs to be engaged')
    args = parser.parse_args()
    margin = args.margin if args.margin=='soft' else float(args.margin)
    assert args.class_in_batch*args.image_per_class_in_batch == args.batch_size, \
           'batch_size need to equal class_in_batch*image_per_class_in_batch'

    # Get Dataset & DataLoader    

    compcars_dataset = sv_comp_Dataset(args.compcars_txt, crop=args.crop, flip=args.flip, jitter=args.jitter,
                                       imagenet_normalize=args.pretrain, val_split=0.1)
    train_compcars_dataloader = Get_train_DataLoader(compcars_dataset, batch_size=args.batch_size)
    val_compcars_dataloader = Get_val_DataLoader(compcars_dataset, batch_size=args.batch_size)

    aic19_dataset = AIC19_Dataset(args.aic19_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.0,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
    train_aic19_dataloader = Get_train_DataLoader(aic19_dataset, batch_size=args.class_in_batch)

    # Test Dataset & loader
    compcars_dataset_test = sv_comp_Dataset(args.compcars_test_txt, crop=False, flip=False, jitter=False,imagenet_normalize=True)
    test_compcars_dataloader = Get_test_DataLoader(compcars_dataset_test, batch_size=args.batch_size)

    # Get Model
    base_net = models.FeatureResNet(n_layers=args.n_layer, pretrained=args.pretrain)
    color_net = models.NLayersFC(base_net.output_dim, 12)
    compcars_model_net = models.NLayersFC(base_net.output_dim, compcars_dataset.n_models)    
    aic19_id_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_id)
    emb_net = models.NLayersFC(base_net.output_dim, 128)# the embedding features
    cams_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_cid)
    ngpu = args.ngpu

    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
    with torch.cuda.device(0):
        base_net = base_net.cuda()
        color_net.cuda()
        aic19_id_net.cuda()
        cams_net.cuda()
        emb_net.cuda()
        compcars_model_net.cuda()
        
    if (device.type == 'cuda') and (ngpu > 1): 
        base_net = nn.DataParallel(base_net, list(range(ngpu)))
        aic19_id_net = nn.DataParallel(aic19_id_net, list(range(ngpu)))
        cams_net = nn.DataParallel(cams_net, list(range(ngpu)))
        emb_net = nn.DataParallel(emb_net, list(range(ngpu)))
        color_net = nn.DataParallel(color_net, list(range(ngpu)))
        compcars_model_net = nn.DataParallel(compcars_model_net, list(range(ngpu)))
    
    if args.save_model_dir !=  None:
        os.system('mkdir -p %s' % os.path.join(args.save_model_dir))

    # Train
    train_joint(args, train_compcars_dataloader, val_compcars_dataloader, \
                train_aic19_dataloader, \
                test_compcars_dataloader,\
                base_net, color_net,
                aic19_id_net, cams_net, emb_net,
                compcars_model_net)

