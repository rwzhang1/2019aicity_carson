#!/bin/sh -f
python train.py --info database/VeRi_train_info.txt --n_epochs 1501 --save_model_dir ./ckpt_mobile_veritrip --model 'mobilenet' --margin soft --class_in_batch 24 --batch_size 96 --triplet --lr 0.005 --batch_hard --save_every_n_epoch 50 --decay_start_iteration 400 --ngpu 3 --with_class


#python train_joint.py --veri_txt 'database/VeRi_train_info.txt' --compcars_txt 'database/Comp_info/Comp_sv_train.txt' --compcars_test_txt 'database/Comp_info/Comp_sv_test.txt' --boxcars_txt 'database/BoxCars_train.txt' --boxcars_test_txt 'database/BoxCars_test.txt' --aic19_txt 'database/AIC19_train_info.txt'  --n_epochs 1501 --save_model_dir ./ckpt --lr 1e-3 --save_every_n_epoch 50 --class_w 1 --batch_size 128 --class_in_batch 16 --image_per_class_in_batch 8 --decay_start_iteration 2500 --model 'mobilenet' --ngpu 3
