"""
@author: rein9
"""

import torch
import os
import sys
import torch.nn as nn
from torch.autograd import Variable
from torchvision import transforms
from PIL import Image
import numpy as np
from progressbar import Bar, ETA, ReverseBar, Percentage, ProgressBar, Timer
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from models import ICT_ResNet, FeatRes51Net
from mobilenetv2 import MobileNetV2
from mobilenet import MobileNet
from densenet import DenseNet
from resnet50_fc import EmbedNetwork

class Feature_ResNet(nn.Module):
    def __init__(self,n_layer,output_color):
        super(Feature_ResNet,self).__init__()
        all_model = ICT_ResNet(1,10,9,n_layer,pretrained=False)
        for name,modules in all_model._modules.items():
            if name.find('fc') == -1 :
                self.add_module(name,modules)
        if output_color == True:
            self.fc_c = all_model.fc_c
        self.output_color = output_color
    def forward(self,x):
        for name,module in self._modules.items():
            if name.find('fc') == -1:
                x = module(x)
        x = x.view(x.size(0),-1)
        if self.output_color == False:  
            return x
        else:
            x  = self.fc_c(x)
            color = torch.max(self.fc_c(x),dim=1)[1]
            return x,color

class ResNet_Loader(object):
    def __init__(self,model_path,n_layer,batch_size=128,output_color=False, ngpu = 1):
        self.batch_size = batch_size
        self.output_color = output_color

        self.model = Feature_ResNet(n_layer,output_color)
        device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
        with torch.cuda.device(0):
            self.model = self.model.cuda()

        if (device.type == 'cuda') and (ngpu > 1):
            self.model = nn.DataParallel(self.model, list(range(ngpu)))
            
        state_dict = torch.load(model_path)
        if 'epoch' in state_dict:
            state_dict = state_dict['model_state_dict']
            
        for key in list(state_dict.keys()):
            if key.find('fc') != -1 and key.find('fc_c') == -1:
                del state_dict[key]
            elif output_color == False and key.find('fc_c') != -1:
                del state_dict[key]
        if 'emb' in state_dict: 
            print('loading base model trained with embed net')
            self.model.load_state_dict(state_dict, strict = False)
        self.model.load_state_dict(state_dict)
        self.model.eval()
        print('loading resnet%d model'%(n_layer))
        self.compose = transforms.Compose([transforms.Resize((224,224)),transforms.ToTensor(),
                                           transforms.Normalize(mean=[0.485,0.456,0.406],std=[0.229,0.224,0.225])])
    def inference(self,file_name_list):
         
        self.model.cuda()
        feature_list = []
        color_list = []
        batch_list = []
        #widgets = [Timer(format='ET: %(elapsed)s'), Bar('>'), ' ', 'Inferencing: ', Percentage(), ' ', ReverseBar('<'), ETA()]
        #pbar = ProgressBar(widgets=widgets, max_value=len(file_name_list))
        #pbar = ProgressBar(widgets=widgets, maxval=len(file_name_list))
        for i,name in enumerate(file_name_list):
            img = Image.open(name)
            img = self.compose(img)
            batch_list.append(img)
            if (i+1)% self.batch_size == 0:
                if self.output_color == False:
                    features = self.model(Variable(torch.stack(batch_list)).cuda())
                    feature_list.append(features.cpu().data)
                else:
                    features,colors = self.model(Variable(torch.stack(batch_list)).cuda())
                    feature_list.append(features.cpu().data)
                    color_list.append(colors.cpu().data)
                batch_list = []
                #pbar.update(i)
        #pbar.finish()
        if len(batch_list)>0:
            if self.output_color == False:
                features = self.model(Variable(torch.stack(batch_list)).cuda())
                feature_list.append(features.cpu().data)
            else:
                features,colors = self.model(Variable(torch.stack(batch_list)).cuda())
                feature_list.append(features.cpu().data)
                color_list.append(colors.cpu().data)
            batch_list = []
        self.model.cpu()
        if self.output_color == False:
            feature_list = torch.cat(feature_list,dim=0)
            return feature_list
        else:
            feature_list = torch.cat(feature_list,dim=0)
            color_list = torch.cat(color_list,dim=0)
            return feature_list,color_list

class MobileNet_Loader(object):
    def __init__(self, model_path, model_name='mobilenet', batch_size=128, with_top=False, ngpu=3, n_layers=50, img_size=224, tencrop=True):
        self.batch_size = batch_size
        self.with_top = with_top
        self.tencrop = tencrop
        if self.tencrop:
            self.with_top = False #inference wont be correct with tencrop

        if model_name == 'mobilenetv2':
            print('[Info] Rebuilding mobilenetv2...')
            self.model = MobileNetV2(num_classes=579, dims = 128, input_size=224, width_mult=1., with_top = with_top)
        elif model_name == 'mobilenet':
            print('[Info] Rebuilding mobilenet...')
            self.model = MobileNet(num_classes=579, dims=128, input_size=224, with_top=with_top)
        elif model_name == 'densenet':
            print('[Info] Rebuilding densenet...')
            self.model = DenseNet(num_classes=579, dims=128, with_top=with_top)
        elif model_name == 'embednet':
            print('[Info] Rebuilding embednet...')
            self.model = EmbedNetwork(num_classes=579, dims=128, with_top=with_top, bn_last=False)
        elif model_name == 'resnet':
            print('[Info] Rebuilding resnet...')
            self.model = FeatRes51Net(num_classes=579, emb_dims=128, n_layers=n_layers, with_top=with_top, bn_last=False, pretrained=False)
            
        device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
        with torch.cuda.device(0):
            self.model = self.model.cuda()

        if (device.type == 'cuda') and (ngpu > 1):
            self.model = nn.DataParallel(self.model, list(range(ngpu)))
        state_dict = torch.load(model_path)
        if 'epoch' in state_dict:
            state_dict = state_dict['model_state_dict']
            
        for key in list(state_dict.keys()):
            if with_top == False and (key.find('classifier') != -1 or (key.find('fc_head') == -1 and key.find('fc') != -1)):
                del state_dict[key]
        if model_name == 'resnet':
            self.model.load_state_dict(state_dict, strict = False)
        else:
            self.model.load_state_dict(state_dict)          
        print(self.model)
        self.model.eval()
        print('loading {} model'.format(model_name))
        if tencrop:
            self.trans_tuple = transforms.Compose([
                    transforms.ToTensor(),
                    transforms.Normalize((0.486, 0.459, 0.408), (0.229, 0.224, 0.225))
                    ])
            self.Lambda = transforms.Lambda(
                lambda crops: [self.trans_tuple(crop) for crop in crops])
            self.compose = transforms.Compose([
                transforms.Resize((img_size+50, img_size+50)),
                transforms.TenCrop((img_size, img_size)),
                self.Lambda,
            ])
        else:
            self.compose = transforms.Compose([transforms.Resize((img_size,img_size)),transforms.ToTensor(),transforms.Normalize(mean=[0.485,0.456,0.406],std=[0.229,0.224,0.225])])

    def inference(self, file_name_list):
        self.model.cuda()
        feature_list = []
        embs_list = []
        id_list = []
        batch_list = []
        for i,name in enumerate(file_name_list):
            img = Image.open(name)
            img = self.compose(img)
            if self.tencrop:
                img = torch.stack(img)
            batch_list.append(img)
            if (i+1)% self.batch_size == 0:
                batch_list = Variable(torch.stack(batch_list))
                #batch_list = np.array(batch_list)
                if self.tencrop:
                    bs, ncrops, c, h, w = batch_list.shape
                    # print('batch_list_shape: {}, {}, {}, {}, {}'.format(bs, ncrops, c, h, w))
                    batch_list = batch_list.reshape(-1, c, h, w)
                    # print('after stacking, batch_list_shape: {}'.format(batch_list.shape))
                with torch.cuda.device(0):
                    batch_list = Variable(batch_list).cuda(non_blocking=True)  
                if self.with_top == False:
                    features, embs = self.model(batch_list)
                    if self.tencrop:
                        features = features.view(bs, ncrops, -1).mean(1)
                        embs = embs.view(bs, ncrops, -1).mean(1)
                        # print('features shape: {},embs shape: {}'.format(features.size(), embs.size()))
                else:
                    '''
                    only when tencrop is False, this model could infer ids
                    '''
                    ids, features, embs = self.model(batch_list)
                    id_list.append(id.cpu().data)
                feature_list.append(features.cpu().data)
                embs_list.append(embs.cpu().data)                          
                batch_list = []
                #pbar.update(i)
        #pbar.finish()
        if len(batch_list)>0:
            batch_list = Variable(torch.stack(batch_list))
            if self.tencrop:
                bs, ncrops, c, h, w = batch_list.shape
                print('batch_list_shape: {}, {}, {}, {}, {}'.format(bs, ncrops, c, h, w))
                batch_list = batch_list.reshape(-1, c, h, w)
                print('after stacking, batch_list_shape: {}'.format(batch_list.shape))
            with torch.cuda.device(0):
                batch_list = Variable(batch_list).cuda(non_blocking=True)  
            if self.with_top == False:
                features, embs = self.model(batch_list)
                if self.tencrop:
                    features = features.view(bs, ncrops, -1).mean(1)
                    embs = embs.view(bs, ncrops, -1).mean(1)
                    print('features shape: {},embs shape: {}'.format(features.size(), embs.size()))
            else:
                '''
                only when tencrop is False, this model could infer ids
                '''
                ids, features, embs = self.model(batch_list)
                id_list.append(id.cpu().data)
            feature_list.append(features.cpu().data)
            embs_list.append(embs.cpu().data)
            batch_list = []
        self.model.cpu()
        if self.with_top == False:
            feature_list = torch.cat(feature_list,dim=0)
            embs_list = torch.cat(embs_list,dim=0)
            print('embs_list shape', embs_list.shape)
            return feature_list, embs_list
        else:
            feature_list = torch.cat(feature_list,dim=0)
            embs_list = torch.cat(embs_list,dim=0)
            id_list = torch.cat(id_list, dim = 0)
            return id_list, feature_list, embs_list