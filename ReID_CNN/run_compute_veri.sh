# python compute_VeRi_dis.py --load_ckpt model_880_base.ckpt --n_layer 50 --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --dis_mat dist_CNN.mat

# python compute_VeRi_dis.py --load_ckpt ckpt_4dataset/model_850_base.ckpt --n_layer 34 --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --dis_mat veri_dist_res/model_850_dist_CNN.mat

# python compute_VeRi_dis.py --n_layer 50 --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --load_ckpt ckpt_ict_wnorm/model_39.ckpt --dis_mat veri_dist_res/ict_wnorm39_dist_CNN.mat

python compute_VeRi_dis.py --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --load_ckpt ckpt_150/ep150_model_mobilenetv2_base.ckpt --dis_mat veri_dist_res/mobilenet150_dist_CNN.mat


python compute_VeRi_dis.py --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --model_name 'mobilenet' --load_ckpt ckpt/ep100_model_mobilenet_base.ckpt --dis_mat veri_dist_res/mobilenet100_dist_CNN.mat
python compute_VeRi_dis.py --gallery_txt database/VeRi_gallery_info.txt --query_txt database/VeRi_query_info.txt --ngpu 1 --load_ckpt ckpt_trip1450/model_1450.ckpt --dis_mat veri_dist_res/tencrop_triplet_1450_dist_CNN.mat --model mobilenet --tencrop True
