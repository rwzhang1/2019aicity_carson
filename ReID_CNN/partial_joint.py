import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.backends.cudnn as cudnn
from logger import Logger
import models
import sys
from tqdm import tqdm
import argparse
import sys
import os
import numpy as np
import models
from gen_aic_veri import VReID_Dataset, TripletImage_Dataset, sv_comp_Dataset, BoxCars_Dataset, AIC19_Dataset
from gen_aic_veri import Get_train_DataLoader, Get_val_DataLoader, Unsupervised_TripletImage_Dataset
from utils.loss import Soft_TripletLoss as TripletLoss
cudnn.benchmark=True

def train_joint(args, train_veri_dataloader, val_veri_dataloader,
                train_aic19_dataloader, val_aic19_dataloader,
                base_net, veri_id_net, color_net, 
                aic19_id_net, cams_net, emb_net):

    optimizer_base = optim.Adam(base_net.parameters(), lr=args.lr)
    optimizer_veri = optim.Adam(veri_id_net.parameters(), lr=args.lr)
    optimizer_color = optim.Adam(color_net.parameters(), lr=args.lr)
    optimizer_aic19 = optim.Adam(aic19_id_net.parameters(), lr=args.lr)
    optimizer_cid = optim.Adam(cams_net.parameters(),lr=args.lr)
    optimizer_emb = optim.Adam(emb_net.parameters(), lr=args.lr)
    criterion_triplet = TripletLoss(margin=margin, batch_hard=args.batch_hard)
    criterion_ce = nn.CrossEntropyLoss()
    logger = Logger(os.path.join(args.save_model_dir,'train'))
    val_logger = Logger(os.path.join(args.save_model_dir,'val'))
    
    epoch_size = min(len(train_veri_dataloader), len(train_aic19_dataloader))
    for e in range(args.n_epochs):
        
        pbar = tqdm(total=epoch_size,ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))
        for n in range(epoch_size):
            logger.append_epoch(e + float(n)/epoch_size)
            # VeRi dataset
            epoch_loss = 0
            for i, samples in enumerate(train_veri_dataloader):
                if i==1: break
                print('samples keys',samples.keys())
                print('samples size',samples['img'].size())
                imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                           samples['img'].size(2), 
                                           samples['img'].size(3),
                                           samples['img'].size(4))
                print('total sample in batch', imgs.shape)
                classes = samples['class'].view(-1)
                colors = samples['color'].view(-1)
#                b_img = Variable(imgs).cuda()
#                classes = Variable(classes).cuda()
#                colors = Variable(colors).cuda()
                with torch.cuda.device(0):
                    b_img = imgs.cuda(non_blocking=True)
                    classes = classes.cuda(non_blocking=True)
                    colors = colors.cuda(non_blocking=True)
                base_net.zero_grad()
                veri_id_net.zero_grad()
                color_net.zero_grad()
                #forward
                pred_feat = base_net(b_img)
                print('pred_feat: {}'.format(pred_feat))
                pred_id = veri_id_net(pred_feat)
                pred_color = color_net(pred_feat)
                b_loss_triplet = criterion_triplet(pred_feat, classes)
                print('triplet_loss', b_loss_triplet.shape)
                loss_id = criterion_ce(pred_id, classes)
                loss_color = criterion_ce(pred_color, colors)
                loss = b_loss_triplet.mean() + loss_id + loss_color
                print('loss', loss, loss.data, loss.data.shape)
                epoch_loss += loss
#                epoch_loss += loss.data[0]
                # backward
                loss.backward()
                optimizer_base.step()
                optimizer_veri.step()
                optimizer_color.step()
    
                logger.logg({'loss_veri_triplet': b_loss_triplet.data.mean(),
                            'loss_veri_triplet_max': b_loss_triplet.data.max(),
                            'loss_veri_triplet_min': b_loss_triplet.data.min(),
                            'loss_veri_id': loss_id,
                            'loss_veri_color': loss_color})

            # AIC dataset
            epoch_loss = 0
            for i, samples in enumerate(train_aic19_dataloader):
                if i==1: break
                imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                           samples['img'].size(2), 
                                           samples['img'].size(3),
                                           samples['img'].size(4))
                vid = samples['vid'].view(-1)
                cid = samples['cid'].view(-1)
                b_img = Variable(imgs).cuda()
                vid = Variable(vid).cuda()
                cid = Variable(cid).cuda()
                base_net.zero_grad()
                aic19_id_net.zero_grad()
                cams_net.zero_grad()
                emb_net.zero_grad()
                #forward
                pred_feat = base_net(b_img)
                pred_vid = aic19_id_net(pred_feat)
                pred_cid = cams_net(pred_feat)
                pred_emb = emb_net(pred_feat)
                b_loss_triplet = criterion_triplet(pred_feat, vid)
                b_loss_triplet_embs = criterion_triplet(pred_emb, vid)
                loss_vid = criterion_ce(pred_vid, vid)
                loss_cid = criterion_ce(pred_cid, cid)
                loss = b_loss_triplet.mean() + b_loss_triplet_embs.mean() + loss_vid + loss_cid
#                epoch_loss += loss.data[0]
                epoch_loss += loss
                # backward
                loss.backward()
                optimizer_base.step()
                optimizer_aic19.step()
                optimizer_emb.step()
                optimizer_cid.step()
    
                logger.logg({'loss_aic19_triplet': b_loss_triplet.data.mean(),
                            'loss_aic19_triplet_max': b_loss_triplet.data.max(),
                            'loss_aic19_triplet_min': b_loss_triplet.data.min(),
                            'loss_aic19_triplet_embs': b_loss_triplet_embs.data.mean(),
                            'loss_aic19_triplet_embs_max': b_loss_triplet_embs.data.max(),
                            'loss_aic19_triplet_embs_min': b_loss_triplet_embs.data.min(),
                            'loss_aic19_vid': loss_vid,
                            'loss_aic19_cid': loss_cid})

            pbar.update(1)
            pbar.set_postfix({'loss':'%f'%(epoch_loss/(n+1))})
        pbar.close()
        print('Training total loss = %f'%(epoch_loss/epoch_size))
        
        if e % args.save_every_n_epoch == 0:
            torch.save(base_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_base.ckpt'%(e)))
            torch.save(veri_id_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_veri_id.ckpt'%(e)))
            torch.save(color_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_color.ckpt'%(e)))
            torch.save(aic19_id_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_aic19_id.ckpt'%(e)))
            torch.save(cams_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_cams.ckpt'%(e)))
            torch.save(emb_net.state_dict(),os.path.join(args.save_model_dir,'model_%d_emb.ckpt'%(e)))
        logger.write_log()

        print('start validation')
        val_logger.append_epoch(e)
        base_net.eval()
        veri_id_net.eval()
        color_net.eval()
        aic19_id_net.eval()
        cams_net.eval()
        emb_net.eval()

        # VeRi
        correct = []
        for i,sample in enumerate(val_veri_dataloader):
            imgs = sample['img'].view(sample['img'].size(0)*sample['img'].size(1),
                                       sample['img'].size(2), 
                                       sample['img'].size(3),
                                       sample['img'].size(4))
            classes = sample['class'].view(sample['class'].size(0)*sample['class'].size(1))
            img = Variable(imgs,volatile=True).cuda()
            gt = Variable(classes,volatile=True).cuda()
            pred = veri_id_net(base_net(img))
            _, pred_cls = torch.max(pred,dim=1)
            correct.append(pred_cls.data==gt.data)
        acc = torch.cat(correct).float().mean()
        print('VeRi ID val acc: %.3f' % acc)
        val_logger.logg({'veri_id_acc':acc})
        
        correct = []
        for i,sample in enumerate(val_aic19_dataloader):
            imgs = sample['img'].view(sample['img'].size(0)*sample['img'].size(1),
                                       sample['img'].size(2), 
                                       sample['img'].size(3),
                                       sample['img'].size(4))
            vid = sample['vid'].view(sample['vid'].size(0)*sample['vid'].size(1))
            img = Variable(imgs,volatile=True).cuda()
            gt = Variable(vid,volatile=True).cuda()
            pred = aic19_id_net(base_net(img))
            _, pred_cls = torch.max(pred,dim=1)
            correct.append(pred_cls.data==gt.data)
        acc = torch.cat(correct).float().mean()
        print('VeRi ID val acc: %.3f' % acc)
        val_logger.logg({'veri_id_acc':acc})

        base_net.train()
        veri_id_net.train()
        color_net.train()
        aic19_id_net.train()
        emb_net.train()
        cams_net.train()

if __name__ == '__main__':
    ## Parse arg
    parser = argparse.ArgumentParser(description='Train Re-ID net', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--veri_txt', required=True, help='txt for VeRi dataset')
    parser.add_argument('--aic19_txt', required=True, help='txt for AIC19 dataset')
    parser.add_argument('--crop',type=bool,default=True,help='Whether crop the images')
    parser.add_argument('--flip',type=bool,default=True,help='Whether randomly flip the image')
    parser.add_argument('--jitter',type=int,default=0,help='Whether randomly jitter the image')
    parser.add_argument('--pretrain',type=bool,default=True,help='Whether use pretrained model')
    parser.add_argument('--lr',type=float,default=0.001,help='learning rate')
    parser.add_argument('--batch_size',type=int,default=128,help='batch size number')
    parser.add_argument('--n_epochs',type=int,default=20,help='number of training epochs')
    parser.add_argument('--load_ckpt',default=None,help='path to load ckpt')
    parser.add_argument('--save_model_dir',default=None,help='path to save model')
    parser.add_argument('--n_layer',type=int,default=18,help='number of Resnet layers')
    parser.add_argument('--margin',type=str,default='0',help='margin of triplet loss ("soft" or float)')
    parser.add_argument('--class_in_batch',type=int,default=32,help='# of class in a batch for triplet training')
    parser.add_argument('--image_per_class_in_batch',type=int,default=4,help='# of images of each class in a batch for triplet training')
    parser.add_argument('--batch_hard',action='store_true',help='whether to use batch_hard for triplet loss')
    parser.add_argument('--save_every_n_epoch',type=int,default=1,help='save model every n epoch')
    parser.add_argument('--class_w',type=float,help='wieghting of classification loss when triplet training')
    args = parser.parse_args()
    margin = args.margin if args.margin=='soft' else float(args.margin)
    assert args.class_in_batch*args.image_per_class_in_batch == args.batch_size, \
           'batch_size need to equal class_in_batch*image_per_class_in_batch'

    # Get Dataset & DataLoader    
    veri_dataset = TripletImage_Dataset(args.veri_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.01,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
    train_veri_dataloader = Get_train_DataLoader(veri_dataset, batch_size=args.class_in_batch)
    val_veri_dataloader = Get_val_DataLoader(veri_dataset,batch_size=args.class_in_batch)

    aic19_dataset = AIC19_Dataset(args.aic19_txt, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                        imagenet_normalize=args.pretrain, val_split=0.01,
                                        class_in_batch=args.class_in_batch,
                                        image_per_class_in_batch=args.image_per_class_in_batch)
    train_aic19_dataloader = Get_train_DataLoader(aic19_dataset, batch_size=args.class_in_batch)
    val_aic19_dataloader = Get_val_DataLoader(aic19_dataset, batch_size=args.class_in_batch)

    # Get Model
    base_net = models.FeatureResNet(n_layers=args.n_layer, pretrained=args.pretrain)
    veri_id_net = models.NLayersFC(base_net.output_dim, veri_dataset.n_id)
    color_net = models.NLayersFC(base_net.output_dim, 12)    
    aic19_id_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_id)
    emb_net = models.NLayersFC(base_net.output_dim, 128)# the embedding features
    cams_net = models.NLayersFC(base_net.output_dim, aic19_dataset.n_cid)
    
    if torch.cuda.is_available():
        base_net.cuda()
        veri_id_net.cuda()
        color_net.cuda()
        aic19_id_net.cuda()
        cams_net.cuda()
        emb_net.cuda()
    
    if args.save_model_dir !=  None:
        os.system('mkdir -p %s' % os.path.join(args.save_model_dir))

    # Train
    train_joint(args, train_veri_dataloader, val_veri_dataloader, train_aic19_dataloader, val_aic19_dataloader, base_net, veri_id_net, color_net, aic19_id_net, cams_net, emb_net)

