# -*- coding: utf-8 -*-
# @Author: w84117436
# @Date:   2019-04-30 14:03:15
# @Last Modified by:   w84117436
# @Last Modified time: 2019-05-03 21:51:12
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:24:22 2019

@author: rwzhang
"""


'''
This file is intended for n input of feature embeddings , compare the ensemble of these embeddings
'''
import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import sys,os,logging,time,itertools
import pandas as pd
from sklearn.utils import shuffle
import numpy as np
import argparse, pickle, re
import cv2
import scipy.io as sio
import xgboost as xgb
import pandas as pd
sys.path.append('..')
from re_ranking import re_ranking

def parse_args():
    parse = argparse.ArgumentParser()
    parse.add_argument('--test_dir', dest = 'test_dir', type = str, default = 'veri_embs', help = 'path to all test embeddings')
    parse.add_argument('--query_dir', dest = 'query_dir', type = str, default = 'veri_embs', help = 'path to all query embeddings')
    parse.add_argument('--save_path', dest = 'save_path', type = str, default = 'veri_embs', help = 'path to save the top cmc_ranked pictures from query')
    parse.add_argument('--dis_mat', dest='dis_mat', help='dis_mat', default='dist_ensemble.mat', type=str)
    parse.add_argument('--feat_opt', dest='feat_opt', type=str, default='feat', help='the option to choose where to calculate the distance using 2048 feature or 128 embeddings')
    parse.add_argument('--ens_output', dest='ens_output', type=str, default='ens_mobilev1v2_dropbn.pkl', help='the ensemble file name for ensembled feature embeddings')
    return parse.parse_args()

def gen_cmc_folder(query_dir, test_dir, name_list = None):
    '''
    @query_dir: the collection of query embeddings
    @test_dir: the collection of test embeddings
    @name_list: the list of embeddings selected for ensembling
    '''
    if name_list != None:
        query_list = sorted(name_list)
    else:
        query_list = sorted(os.listdir(query_dir))
    print('[INFO] query list', query_list)
    dist, gold_query_img, gold_test_img = None, None, None    

    for i_q, query_name in enumerate(query_list):
        test_name = re.sub('query', 'test', query_name)
        query_embs = os.path.join(query_dir, query_name)        
        test_embs = os.path.join(test_dir, test_name)
        if not os.stat(test_embs):
            print('matching query: {} not found'.format(query_name))
            continue

        print(query_embs, test_embs)
        logger.info('loading query embeddings')
        with open(test_embs, 'rb') as fr:
            test_dict = pickle.load(fr)
            # if i_q == 0:
            #     final_test_dict = test_dict
            print('test_keys', test_dict.keys())
            img_names_test = test_dict['img_names']
            embs_test = test_dict['embeddings']
            print(embs_test.shape)
            feat_test= test_dict['features']
            
        logger.info('loading query embeddings')
        with open(query_embs, 'rb') as fr:
            query_dict = pickle.load(fr)
            img_names_query = query_dict['img_names']
            embs_query= query_dict['embeddings']
            feat_query = query_dict['features']
        
        if args.feat_opt == 'emb':
            query_obj = np.copy(embs_query)
            test_obj = np.copy(embs_test)
        elif args.feat_opt == 'feat':
            query_obj = np.copy(feat_query)
            test_obj = np.copy(feat_test)

        q_n = query_obj.shape[0]
        g_n = test_obj.shape[0]    
        if i_q == 0:
            dist = np.zeros([q_n, g_n], dtype=np.float32)
            gold_query_img = img_names_query[:]
            gold_test_img = img_names_test[:]

        # print(list(img_names_query))
        # print(list(img_names_test))        
        assert list(gold_query_img) == list(img_names_query), 'query_image order not matching'
        assert list(gold_test_img) == list(img_names_test), 'test_image order not matching'
        print('dist_matrix size', dist.shape)
        for rep in range(4):
            # 4 repeats
            # randomly assign marking to different feature 
            margin = np.random.rand()*0.6+0.5
            dist += margin*re_ranking(query_obj,test_obj)
    print("final shape", dist.shape, type(dist))
    sio.savemat(rerank_dis_mat,{'dist_CNN_reranked':dist.T})

def gen_features(query_dir, test_dir, save_file, name_list = None):
    '''
    @query_dir: the collection of query embeddings
    @test_dir: the collection of test embeddings
    @name_list: the list of embeddings selected for ensembling
    '''
    if name_list != None:
        query_list = sorted(name_list)
    else:
        query_list = sorted(os.listdir(query_dir))
    print('[INFO] query list', query_list)
    dist, gold_query_img, gold_test_img = None, None, None    
    query_out_name = 'query_' + save_file 
    test_out_name = 'test_' + save_file 
    for i_q, query_name in enumerate(query_list):
        test_name = re.sub('query', 'test', query_name)
        query_embs = os.path.join(query_dir, query_name)        
        query_ens_embs = os.path.join(query_dir, query_out_name)        
        test_embs = os.path.join(test_dir, test_name)
        test_ens_embs = os.path.join(test_dir, test_out_name)        
        if not os.stat(test_embs):
            print('matching query: {} not found'.format(query_name))
            continue

        print(query_embs, test_embs)
        logger.info('loading test embeddings')
        with open(test_embs, 'rb') as fr:
            test_dict = pickle.load(fr)
            test_pd = pd.read_pickle(test_embs)
            test_pd.sort_values('img_names')
            if i_q == 0:
                ftest_pd = test_pd.copy(deep=True)
            else:
                assert(list(ftest_pd['img_names']) == list(test_pd['img_names'])), 'image name not matching'
                print('i_q:{}\n, ftest_pd shape:{}\n, test_pd shape:{}\n'.format(i_q, len(ftest_pd.iloc[0]['embeddings']), ftest_pd.iloc[0]['embeddings']))
                total_len = len(test_pd)
                for idx_pd in range(total_len):
                    assert(ftest_pd.iloc[idx_pd]['img_names']== test_pd.iloc[idx_pd]['img_names']),'image name not matching'
                    ftest_pd.iloc[idx_pd]['embeddings'] += test_pd.iloc[idx_pd]['embeddings']
                    ftest_pd.iloc[idx_pd]['features'] += test_pd.iloc[idx_pd]['features']
        # complex indexing 
        print(len(ftest_pd.iloc[0]['embeddings']))
        with open(test_ens_embs, 'wb') as fw:
            pickle.dump(ftest_pd, fw)

        logger.info('loading query embeddings')
        with open(query_embs, 'rb') as fr:
            query_dict = pickle.load(fr)
            query_pd = pd.read_pickle(query_embs)
            query_pd.sort_values('img_names')
            if i_q == 0:
                fquery_pd = query_pd.copy(deep=True)
            else:
                assert(list(fquery_pd['img_names']) == list(query_pd['img_names'])), 'image name not matching'
                print('i_q:{}\n, fquery_pd shape:{}\n, query_pd shape:{}\n'.format(i_q, len(fquery_pd.iloc[0]['embeddings']), fquery_pd.iloc[0]['embeddings']))
                total_len = len(query_pd)
                for idx_pd in range(total_len):
                    assert(fquery_pd.iloc[idx_pd]['img_names']== query_pd.iloc[idx_pd]['img_names']),'image name not matching'
                    fquery_pd.iloc[idx_pd]['embeddings'] += query_pd.iloc[idx_pd]['embeddings']
                    fquery_pd.iloc[idx_pd]['features'] += query_pd.iloc[idx_pd]['features']
        # complex indexing 
        print(len(fquery_pd.iloc[0]['embeddings']))

        with open(query_ens_embs, 'wb') as fw:
            pickle.dump(fquery_pd, fw)

def train_xgb_cnn(x_train_cnn, y_train, cnn=None, num_round=100):
    '''
    Build the training module for XGBoost
    '''
#   xgb1 = xgb(learning_rate=0.1,
#        n_estimators=1000,
#        max_depth=6,
#        min_child_weight=11,
#        gamma=0.1,
#        subsample=0.8,
#        colsample_bytree=0.7,
#        objective='multi:softprob',
#        n_jobs=-1,
#        scale_pos_weight=1,
#        seed=seed)
         
    param = {'eta':0.1,
            'max_depth':6,
            'objective':'multi:softmax',
            'n_estimators':175,
            'silent':1,
            'num_class':579}
             
    # feat_out = get_feature_layer(cnn_model, x_train_cnn)
    dtrain = xgboost.DMatrix(query_obj,label=y_train.idxmax(axis=1).values)
    xgb_feature_layer = xgboost.train(param, dtrain, num_round)
    return xgb_feature_layer
    
EMB_LOOKUP ={'test_emb_paths': ['test_emb1', 'test_emb2', 'test_emb3'],
             'query_emb_paths': ['query_emb1', 'query_emb2', 'query_emb3']}

name_list = ['query_mobile_1500_dropbn.pkl', 'query_mobile2_1500_noNRjitter.pkl']

args = parse_args()
root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
test_dir = root_dir + args.test_dir
query_dir = root_dir + args.query_dir
save_path = root_dir + args.save_path + '/' + 'ensemble'
rerank_dis_mat = save_path + '/' + args.dis_mat
ens_output = args.ens_output

if not os.path.exists(save_path):
    os.makedirs(save_path)
else:
    new_path = '{}_{}'.format(save_path, time.strftime("%Y%m%d%H%M"))
    print(new_path)
    os.rename(save_path,new_path)

## logging
FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
logger = logging.getLogger(__name__)

gen_features(query_dir, test_dir, ens_output, name_list)
#gen_cmc_folder(query_dir, test_dir, name_list)

