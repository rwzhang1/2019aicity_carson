# -*- coding: utf-8 -*-
# @Author: w84117436
# @Date:   2019-04-30 14:02:36
# @Last Modified by:   w84117436
# @Last Modified time: 2019-05-03 19:59:58
"""
@author: rein9
"""

import torch
import torch.nn as nn
from torch.autograd import Variable
from torchvision import transforms
from collections import defaultdict,OrderedDict
from PIL import Image
import os, sys, argparse, random, pickle
import numpy as np
import scipy.io as sio
import pandas as pd
import glob
from Model_Wrapper import ResNet_Loader, MobileNet_Loader
sys.path.append('..')
from re_ranking import re_ranking
from gen_aic_veri import TripletImage_Dataset
from torch.utils.data import DataLoader

def compute_cmc(features,labels):
    error = []
    norm_features = nn.functional.normalize(features,dim=1).cuda()
    norm_features_2 = norm_features.transpose(0,1).cuda()
    SimMat = -1*torch.mm(norm_features,norm_features_2).cpu()
    del norm_features,norm_features_2
    
    cmc = torch.zeros(SimMat.size(0))
    for i in range(SimMat.size(0)):
        _,argsort = torch.sort(SimMat[i])
        
        for j in range(SimMat.size(0)):
            if labels[argsort[j]] != labels[i] and argsort[j]!=i and j==1:
                error.append(argsort[j])
            if labels[argsort[j]] == labels[i] and argsort[j]!=i:
                rank = j-1
                break
        for j in range(rank,SimMat.size(0)):
            cmc[j]+=1

    cmc = torch.floor((cmc/SimMat.size(0))*100)  
    return cmc,error


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compute VeRi Distance for  Re-ID',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--load_ckpt',default=None,help='path to load ckpt')
    parser.add_argument('--model_name',default='mobilenet',help='model to reload for inferencing')
    parser.add_argument('--n_layer',type=int,default=18,help='number of Resnet layers')
    parser.add_argument('--gallery_txt',default=None,help='path to load gallery')
    parser.add_argument('--query_txt',default=None,help='path to load query')
    parser.add_argument('--dis_mat',default=None,help='path to store distance')
    parser.add_argument('--batch_size',default=64,type=int,help='batch_size for inferencing')
    parser.add_argument('--tencrop',default=False,type=bool,help='whether to tencrop for inferencing')
    parser.add_argument('--save_path',default='/home/carson/2019AICity_carson/Track2/ReID_CNN/veri_embs',type=str,help='the save path for query/test embeddings')
    parser.add_argument('--emb_pkl',default=None,help='path to store reranked distance')
    parser.add_argument('--feat_op',default='embs',help='using feature or embeddings', type=str)
    parser.add_argument('--ngpu',dest='ngpu',default=1,type=int,help='number of gpus to use')
    ##### The following is added for generate training embeddings#######
    parser.add_argument('--info',default='/home/carson/2019AICity_carson/Track2/ReID_CNN/database/VeRi_train_info.txt',help='txt file contain path of data')
    parser.add_argument('--crop',type=bool,default=True,help='Whether crop the images')
    parser.add_argument('--flip',type=bool,default=True,help='Whether randomly flip the image')
    parser.add_argument('--jitter',type=int,default=0,help='Whether randomly jitter the image')
    parser.add_argument('--pretrain',type=bool,default=True,help='Whether use pretrained model')
    parser.add_argument('--class_in_batch',type=int,default=32,help='# of class in a batch for triplet training')
    parser.add_argument('--image_per_class_in_batch',type=int,default=4,help='# of images of each class in a batch for triplet training')
    args = parser.parse_args()
    
    ## double check the path
    emb_path = args.save_path + '/'
    test_emb_file = emb_path + 'test_' + args.emb_pkl
    query_emb_file = emb_path + 'query_' + args.emb_pkl
    train_emb_file = emb_path + 'train_' + args.emb_pkl
    dist_path = '/home/carson/2019AICity_carson/Track2/ReID_CNN/veri_dist_res/'
    if args.tencrop:
        dis_mat = dist_path + 'tencrop_' + args.dis_mat
    else:
        dis_mat = dist_path + args.dis_mat
    rerank_dis_mat = dist_path + 'rerank_' + args.dis_mat
    print('{}\n {}\n {}\n {}\n {}\n'.format(test_emb_file, query_emb_file, train_emb_file, dis_mat, rerank_dis_mat))
    print('loading model....')
    #model = ResNet_Loader(args.load_ckpt,args.n_layer,output_color=False,batch_size=64)#only used for veri_ict now

def gen_embeddings(args, query_emb_file, test_emb_file):
    '''
    generate feature embeddings
    '''
    model = MobileNet_Loader(args.load_ckpt,model_name = args.model_name,batch_size=args.batch_size, ngpu = args.ngpu, n_layers=args.n_layer, tencrop=args.tencrop)
    with open(args.query_txt,'r') as f:
        query_txt = [q.strip() for q in f.readlines()]
        query_txt = query_txt[1:]
    with open(args.gallery_txt,'r') as f:
        gallery_txt = [q.strip() for q in f.readlines()]
        gallery_txt = gallery_txt[1:]
        
    ###  for using ResNet_Loader only
    # print('inferencing q_features')
    # q_features = model.inference(query_txt)
    # print('inferencing g_features')
    # g_features = model.inference(gallery_txt)
    
    print('inferencing q_features')
    q_features,q_embs = model.inference(query_txt)
    print('inferencing g_features')
    g_features,g_embs = model.inference(gallery_txt)
    ## dump results
    query_names = np.hstack(query_txt)
    gallery_names = np.hstack(gallery_txt)
    q_embd_res = pd.DataFrame({'features': q_features.detach().cpu().numpy().tolist(), 'embeddings': q_embs.detach().cpu().numpy().tolist(), 'img_names': query_names.tolist()})
    g_embd_res = pd.DataFrame({'features': g_features.detach().cpu().numpy().tolist(), 'embeddings': g_embs.detach().cpu().numpy().tolist(), 'img_names': gallery_names.tolist()})
    print('[INFO] img_names')
    print(q_embd_res['img_names'])
    print(g_embd_res['img_names'])
    
    with open(query_emb_file, 'wb') as fw:
        pickle.dump(q_embd_res, fw)
    with open(test_emb_file, 'wb') as fw:
        pickle.dump(g_embd_res, fw)
    print('Embedding Done')
    print('compute distance')
    print("q_features shape: {}, q_embs shape: {}".format(q_features.shape, q_embs.shape))
    print("g_features shape: {}, q_embs shape: {}".format(g_features.shape, g_embs.shape))

    if args.feat_op == 'feat': 
        return q_features,g_features
    elif args.feat_op == 'embs': 
        # changed on 04/05 for adam drop 0.1
        return q_embs,g_embs
    
def gen_dist(q_feat, g_feat, rerank_dis_mat):
    '''
    compute distance
    '''
    q_obj = nn.functional.normalize(q_feat,dim=1).cuda()
    g_obj = nn.functional.normalize(g_feat,dim=1).transpose(0,1).cuda()
    SimMat = -1 * torch.mm(q_obj,g_obj)
    SimMat = SimMat.cpu().transpose(0,1)
    print(SimMat.size())
    SimMat = SimMat.numpy()
    sio.savemat(dis_mat,{'dist_CNN':SimMat})    
    # added on 04/10, re_ranking
    print("q_obj shape: {}, q_feat shape: {}".format(q_obj.shape, q_feat.shape))
    print("g_obj shape: {}, g_feat shape: {}".format(g_obj.shape, g_feat.shape))

    q_n = q_feat.shape[0] 
    g_n = g_feat.shape[0]    
    dist = np.zeros([q_n, g_n], dtype=np.float32)
    print('dist_matrix size', dist.shape)
    
    for rep in range(10):
        # 4 repeats
        # randomly assign marking to different feature 
        margin = np.random.rand()*0.5+0.5
        dist += margin*re_ranking(q_feat.numpy(),g_feat.numpy())
    print("final shape", dist.shape, type(dist))
    sio.savemat(rerank_dis_mat,{'dist_CNN_reranked':dist.T})

q_feat, g_feat = gen_embeddings(args, query_emb_file, test_emb_file)
gen_dist(q_feat, g_feat, rerank_dis_mat)

def gen_trainembeddings(args, train_emb_file):
    '''
    For any given image set, generate embeddings for future training purpose
    '''
    model = MobileNet_Loader(args.load_ckpt,model_name = args.model_name,batch_size=args.batch_size, ngpu = args.ngpu, n_layers=args.n_layer, tencrop=False)
    txt = np.loadtxt(args.info, dtype=str)[1:]
    train_txt = txt[:, 0]
    features, embeddings  = model.inference(train_txt)
        
    # for i, sample in enumerate(test_Dataloader):
        # img = sample['img']
        # classes = sample['class']
        # img_name = sample['img_names']
        
        # label_ids.append(classes)
        # img_names.append(img_name)

        # for imName in img_name:
            # visited_images.add(imName)      
        # if tencrop:
            # bs, ncrops, c, h, w = img.size()
            # print('img_shape: {}, {}, {}, {}, {}'.format(bs, ncrops, c, h, w))
            # img = img.view(-1, c, h, w)            
        # print('img_shape: {}'.format(img.size()))
        # with torch.cuda.device(0):
            # b_img = img.cuda(non_blocking=True)   
        # pred_feat, pred_embs = net(b_img)

        # print('feat_shape: {}'.format(pred_feat.size()))     
        # if tencrop:
            # feat_avg = pred_feat.view(bs, ncrops, -1).mean(1)
            # embs_avg = pred_embs.view(bs, ncrops, -1).mean(1)
            # features.append(feat_avg.detach().cpu().numpy())            
            # embeddings.append(embs_avg.detach().cpu().numpy())            
        # else:
            # features.append(pred_feat.detach().cpu().numpy())
            # embeddings.append(pred_embs.detach().cpu().numpy())
    features = features.detach().cpu().numpy()
    embeddings = embeddings.detach().cpu().numpy()
    img_names = np.hstack(train_txt)
    label_ids = np.hstack(txt[:, 1])
    print('total labels: {}\n img_names: {}'.format(label_ids, train_txt))
    embd_res = {'features': features, 'embeddings': embeddings, 'label_ids': label_ids, 'img_names':img_names}
    with open(train_emb_file, 'wb') as fw:
        pickle.dump(embd_res, fw)
    print('Training Embedding Done')
    return embd_res

#gen_trainembeddings(args, train_emb_file)