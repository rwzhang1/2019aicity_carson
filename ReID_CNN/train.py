import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.backends.cudnn as cudnn
from logger import Logger
from tqdm import tqdm
import argparse, sys, os, re
import numpy as np
sys.path.append('..')
import models
from mobilenet import MobileNet
from mobilenetv2 import MobileNetV2
from densenet import DenseNet
from resnet50_fc import EmbedNetwork
from gen_aic_veri import VReID_Dataset, TripletImage_Dataset, sv_comp_Dataset, BoxCars_Dataset, AIC19_Dataset
from gen_aic_veri import Get_train_DataLoader, Get_val_DataLoader, Unsupervised_TripletImage_Dataset
from utils.loss import Soft_TripletLoss as TripletLoss
from utils.optimizer import AdamOptimWrapper, SGDOptimWrapper
from utils.optim import OptimRegime
from gen_triplet import BatchHardTripletSelector
cudnn.benchmark=True


def train_ict(args,Dataset,train_Dataloader,val_Dataloader,net):

    optimizer = AdamOptimWrapper(net.parameters(), lr=args.lr, wd=0, t0=args.decay_start_iteration, betas=(0.7,0.999))
    criterion = nn.CrossEntropyLoss()

    for e in range(args.n_epochs):
        pbar = tqdm(total=len(Dataset.train_index),ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))
        epoch_loss = 0
        iter_count = 0
        for i_batch,samples in enumerate(train_Dataloader):
            iter_count +=1
            b_img = Variable(samples['img']).cuda()
            b_gt = Variable(samples['gt'].squeeze(1)).cuda()
            b_c = Variable(samples['color'].squeeze(1)).cuda()
            b_t = Variable(samples['type'].squeeze(1)).cuda()
            b_size = b_img.size(0)
            net.zero_grad()
            optimizer.zero_grad()
            #forward
            b_pred,b_cpred,b_tpred = net(b_img)
            loss = criterion(b_pred,b_gt)+criterion(b_cpred,b_c)+criterion(b_tpred,b_t)
            epoch_loss += loss.data
            #epoch_loss += loss.data[0]
            # backward
            loss.backward()

            optimizer.step()
            pbar.update(b_size)
            pbar.set_postfix({'loss':'%.2f'%(loss.data)})
        pbar.close()
        print('Training total loss = %.3f'%(epoch_loss/iter_count))
        if e % args.save_every_n_epoch == 0:
            torch.save(net.state_dict(),os.path.join(args.save_model_dir,'model_%s_%d.ckpt'%(args.model, e)))
        print('start validation')
        correct_i = []
        correct_c = []
        correct_t = []
        for i,sample in enumerate(val_Dataloader):
            img = Variable(sample['img'],volatile=True).cuda()
            gt = sample['gt']
            c = sample['color']
            t = sample['type']
            net.eval()
            pred,color,type = net(img)
            pred_cls = torch.max(pred.cpu().data,dim=1)[1]
            pred_color = torch.max(color.cpu().data,dim=1)[1]
            pred_type = torch.max(type.cpu().data,dim=1)[1]
            for x in range(gt.size(0)):
                if gt[x][0] == pred_cls[x]:
                    correct_i.append(1)
                else:
                    correct_i.append(0)
                if c[x][0] == pred_color[x]:
                    correct_c.append(1)
                else:
                    correct_c.append(0)
                if t[x][0] == pred_type[x]:
                    correct_t.append(1)
                else:
                    correct_t.append(0)
        print('val acc: id:%.3f, color:%.3f, type:%.3f'%(np.mean(correct_i),np.mean(correct_c),np.mean(correct_t)))
        net.train()
        file = open(os.path.join(args.save_model_dir,'val_log.txt'),'a')
        file.write('Epoch %d: val_id_acc = %.3f, val_color_acc = %.3f, val_type_acc= %.3f\n'%(e,np.mean(correct_i),np.mean(correct_c),np.mean(correct_t)))
        file.close()

def train(args,Dataset,train_Dataloader,val_Dataloader,net):

    optimizer = optim.Adam(net.parameters(),lr=args.lr)
    criterion = nn.CrossEntropyLoss()
    
    for e in range(args.n_epochs):
        
        pbar = tqdm(total=len(Dataset.train_index),ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))

        epoch_loss = 0
        iter_count = 0
        for i_batch,samples in enumerate(train_Dataloader):
            iter_count +=1
            b_img = Variable(samples['img']).cuda()
            b_gt = Variable(samples['gt'].squeeze(1)).cuda()
            b_size = b_img.size(0)
            net.zero_grad()
            #forward
            b_pred,_ = net(b_img)
            loss = criterion(b_pred,b_gt)
            epoch_loss += loss.data
            #epoch_loss += loss.data[0]
            # backward
            loss.backward()

            optimizer.step()
            pbar.update(b_size)
            pbar.set_postfix({'loss':'%.2f'%(loss.data[0])})
        pbar.close()
        print('Training total loss = %.3f'%(epoch_loss/iter_count))
        if e % args.save_every_n_epoch == 0:
            save_model = args.model
            if args.model == 'resnet':
                save_model += str(args.n_layer)
            torch.save(net.state_dict(),os.path.join(args.save_model_dir,'model_%s_%d.ckpt'%(model, e)))
        print('start validation')
        net.eval()
        correct = []
        for i,sample in enumerate(val_Dataloader):
            img = Variable(sample['img'],volatile=True).cuda()
            gt = sample['gt']
            pred = net(img)
            pred_cls = torch.max(pred.cpu().data,dim=1)[1]
            for x in range(gt.size(0)):
                if gt[x][0] == pred_cls[x]:
                    correct.append(1)
                else:
                    correct.append(0)
        print('val acc: %.3f'%(np.mean(correct)))
        net.train()
        file = open(os.path.join(args.save_model_dir,'val_log.txt'),'a')
        file.write('Epoch %d: val_acc = %.3f\n'%(e,np.mean(correct)))
        file.close()

def train_triplet(args, starting_epoch, resume_lr, resume_steps, Dataset,train_Dataloader,val_Dataloader,net):
    print('model summary', net)
    margin = args.margin if args.margin=='soft' else float(args.margin)
    
    max_iter = args.n_epochs*len(train_Dataloader)
# =============================================================================
# optimizer configuration
    #optim_regime = getattr(net, 'regime', [{'epoch': 0,'optimizer': args.optimizer,'lr': args.lr,'momentum': args.momentum,'weight_decay': args.weight_decay}])
    optim_regime = getattr(net.module, 'regime')
    print("the original regime: ", optim_regime)
    if isinstance(optim_regime, OptimRegime):
        print('using general optim_regime setting')
        optimizer = optim_regime 
    else:
        print('creating new regime')
        optimizer = OptimRegime(net, optim_regime, use_float_copy=False)
# =============================================================================
        
    print('total iterations: ', max_iter, 'decay_start_iteration: ', args.decay_start_iteration)
    # optimizer = AdamOptimWrapper(net.parameters(), lr=resume_lr, resume_steps=resume_steps, wd=args.weight_decay, t0=args.decay_start_iteration, t1=25000)
    # optimizer = SGDOptimWrapper(net.parameters(),lr=resume_lr, resume_steps=resume_steps, wd=args.weight_decay, t0=args.decay_start_iteration, t1=max_iter)
    criterion_triplet = TripletLoss(margin=margin, batch_hard=args.batch_hard)
    if args.with_class: criterion_class = nn.CrossEntropyLoss()
    training_steps = resume_steps
    logger = Logger(args.save_model_dir, prefix='train_')
    
    for e in range(starting_epoch, args.n_epochs):
        pbar = tqdm(total=len(Dataset.train_index),ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))

        epoch_loss = 0
        iter_count = 0
        for i_batch, samples in enumerate(train_Dataloader):
            iter_count +=1
            imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                       samples['img'].size(2), 
                                       samples['img'].size(3),
                                       samples['img'].size(4))
            classes = samples['class'].view(samples['class'].size(0)*samples['class'].size(1))
            b_img = Variable(imgs).cuda()
            b_class = Variable(classes).cuda()
            b_size = samples['class'].size(0)
            net.zero_grad()
            #forward
            pred_class, pred_feat, pred_embs = net(b_img)
            print('veri_dataset: veri_feat.mean: {}, embs.mean: {}\n'.format(pred_feat.mean(), pred_embs.mean()))
            #b_loss = criterion_triplet(pred_feat, b_class)
            b_loss = criterion_triplet(pred_embs, b_class)
            # modified on 03/17 to add triplet loss for embeddings
            # loss = 0.5 * b_loss.mean() + 0.5 * b_loss_emb.mean()
            loss = b_loss.mean()
            if args.with_class:
                print('without class loss', loss.data)
                loss += args.class_w * criterion_class(pred_class, b_class)
            epoch_loss += loss.data
            #epoch_loss += loss.data[0]
            # backward
            optimizer.zero_grad()
            optimizer.update(e, training_steps)
            optimizer.pre_forward()
            loss.backward()
            optimizer.pre_backward()
            optimizer.step()
            training_steps+=1

            logger.append_epoch(e + float(i_batch)/len(train_Dataloader))
            logger.append_loss(b_loss.data.cpu().numpy())
            logger.append_feat(pred_feat.data.cpu().numpy())
            logger.write_log()
            pbar.update(b_size)
            pbar.set_postfix({'loss':'%.2f'%(loss.data)})
        pbar.close()
        print('training_steps:{}, base_net lr: {:4f}; Training total loss = {:5f}\n'.format(training_steps, optimizer.lr, epoch_loss/iter_count))
        #print('training_steps:{}, Training total loss = {:5f}\n'.format(training_steps, epoch_loss/iter_count))
        if e % args.save_every_n_epoch == 0:
            #updates made on 04/06 to save epoch, learning rate info and step_count of optimizer
            #torch.save(net.state_dict(),os.path.join(args.save_model_dir,'model_%s_%d.ckpt'%(save_model, e)))
            save_model = args.model
            if args.model == 'resnet':
                save_model += str(args.n_layer)
            torch.save({'epoch': e, 'model_state_dict': net.state_dict(), 'base_lr': optimizer.lr, 'training_steps': training_steps}, os.path.join(args.save_model_dir,'model_%s_%d.ckpt'%(save_model, e)))
        logger.plot()
        print('start validation')
        net.eval()
        correct = []
        correct_i = []
 
        for i,samples in enumerate(val_Dataloader):
            imgs = samples['img'].view(samples['img'].size(0)*samples['img'].size(1),
                                       samples['img'].size(2),
                                       samples['img'].size(3),
                                       samples['img'].size(4))
            classes = samples['class'].view(samples['class'].size(0)*samples['class'].size(1))
            b_img = Variable(imgs, volatile=True).cuda()
            b_class = Variable(classes, volatile=True).cuda()
            pred_class, pred_feat, pred_embs = net(b_img)
            b_loss = criterion_triplet(pred_embs, b_class).data.cpu().numpy().squeeze()
            # b_loss = criterion_triplet(pred_feat, b_class).data.cpu().numpy().squeeze()
            pred_cls = torch.max(pred_class.cpu().data,dim=1)[1]
            for x in range(b_loss.shape[0]):
                #print('current class:', classes[x], pred_cls[x])
                if b_loss[x] < 1e-3:
                    correct.append(1)
                else:
                    correct.append(0)
                if classes[x] == pred_cls[x]:
                    correct_i.append(1)
                else:
                    correct_i.append(0)
        print('val acc: %.3f, id: %.3f'%(np.mean(correct), np.mean(correct_i),))
        net.train()
        file = open(os.path.join(args.save_model_dir,'val_log.txt'),'a')
        file.write('Epoch %d: val_acc = %.3f, val_id = %.3f\n'%(e,np.mean(correct), np.mean(correct_i),))
        file.close()

# =============================================================================
def train_unsupervised_triplet(args,Dataset,train_Dataloader,net):
    margin = args.margin if args.margin=='soft' else float(args.margin)

    optimizer = optim.Adam(net.parameters(), lr=args.lr)
    criterion_triplet = TripletLoss(margin=margin, batch_hard=args.batch_hard)
    logger = Logger(args.save_model_dir, prefix='train_')
    
    for e in range(args.n_epochs):
        pbar = tqdm(total=len(train_Dataloader),ncols=100,leave=True)
        pbar.set_description('Epoch %d'%(e))

        epoch_loss = 0
        iter_count = 0
        for i_batch, samples in enumerate(train_Dataloader):
            iter_count +=1
            imgs = samples['img'].squeeze(0)
            pos_mask = samples['pos_mask'].squeeze(0)
            neg_mask = samples['neg_mask'].squeeze(0)
            b_img = Variable(imgs).cuda()
            pos_mask = Variable(pos_mask).cuda()
            neg_mask = Variable(neg_mask).cuda()
            net.zero_grad()
            #forward
            pred_feat = net(b_img)
            b_loss = criterion_triplet(pred_feat, pos_mask=pos_mask, neg_mask=neg_mask, mode='mask')
            loss = b_loss.mean()
            epoch_loss += loss.data
            # backward
            loss.backward()
            optimizer.step()

            logger.append_epoch(e + float(i_batch)/len(train_Dataloader))
            logger.append_loss(b_loss.data.cpu().numpy())
            logger.append_feat(pred_feat.data.cpu().numpy())
            logger.write_log()
            pbar.update(1)
            pbar.set_postfix({'loss':'%.2f'%(loss.data)})
        pbar.close()
        print('Training total loss = %.3f'%(epoch_loss/iter_count))
        if e % args.save_every_n_epoch == 0:
            save_model = args.model
            if args.model == 'resnet':
                save_model += str(args.n_layer)
            torch.save(net.state_dict(),os.path.join(args.save_model_dir,'model_%s_%d.ckpt'%(save_model, e)))
        logger.plot()

if __name__ == '__main__':
    ## Parse arg
    parser = argparse.ArgumentParser(description='Train Re-ID net',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--info',default='train_info.txt',help='txt file contain path of data')
    parser.add_argument('--crop',type=bool,default=True,help='Whether crop the images')
    parser.add_argument('--flip',type=bool,default=True,help='Whether randomly flip the image')
    parser.add_argument('--jitter',type=int,default=0,help='Whether randomly jitter the image')
    parser.add_argument('--pretrain',type=bool,default=True,help='Whether use pretrained model')
    parser.add_argument('--lr',type=float,default=0.001,help='learning rate')
    parser.add_argument('--batch_size',type=int,default=64,help='batch size number')
    parser.add_argument('--n_epochs',type=int,default=20,help='number of training epochs')
    parser.add_argument('--model', dest = 'model', help='model', default='mobilenetv2', type=str)
    parser.add_argument('--decay_start_iteration',dest ='decay_start_iteration', default = 2000, type=int, help='At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.')
    parser.add_argument('--load_ckpt',default=None,help='path to load ckpt')
    parser.add_argument('--save_model_dir',default=None,help='path to save model')
    parser.add_argument('--n_layer',type=int,default=18,help='number of Resnet layers')
    parser.add_argument('--dataset',default='VeRi_ict',help='which dataset:VeRi or VeRi_ict')
    parser.add_argument('--triplet',action='store_true',help='use triplet training')
    parser.add_argument('--unsupervised',action='store_true',help='use unsupervised triplet training')
    parser.add_argument('--with_class',action='store_true',help='whether to train with class label during triplet training')
    parser.add_argument('--margin',type=str,default='0',help='margin of triplet loss ("soft" or float)')
    parser.add_argument('--class_in_batch',type=int,default=32,help='# of class in a batch for triplet training')
    parser.add_argument('--image_per_class_in_batch',type=int,default=4,help='# of images of each class in a batch for triplet training')
    parser.add_argument('--batch_hard',action='store_true',help='whether to use batch_hard for triplet loss')
    parser.add_argument('--save_every_n_epoch',dest='save_every_n_epoch',type=int,default=1,help='save model every n epoch')
    parser.add_argument('--class_w',dest='class_w', default=0.8, type=float,help='weighting of classification loss when triplet training')
    parser.add_argument('--ngpu', dest='ngpu', default = 1, type=int, help='number of gpus to use')
# =============================================================================
#   Additional Settings for optimizer regime creation
# =============================================================================
    parser.add_argument('--weight_decay',dest='weight_decay', default=4e-5, type=float, help='weight decay (default: 1e-4)')
    parser.add_argument('--optimizer',dest='optimizer',default='Adam', type=str, help='optimizer function used')
    parser.add_argument('--momentum',dest='momentum', default=0.9, type=float, help='momentum') 
    args = parser.parse_args()
    ngpu = args.ngpu
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
    ## Get Dataset & DataLoader
    if args.triplet:
        if args.unsupervised:
            Dataset = Unsupervised_TripletImage_Dataset(args.info, crop=args.crop, flip=args.flip, jitter=args.jitter, imagenet_normalize=args.pretrain)
            train_Dataloader = Get_train_DataLoader(Dataset,batch_size=1)
        else:
            Dataset = TripletImage_Dataset(args.info, crop=args.crop, flip=args.flip, jitter=args.jitter, 
                                           imagenet_normalize=args.pretrain, 
                                           class_in_batch=args.class_in_batch,
                                           image_per_class_in_batch=args.image_per_class_in_batch)
            train_Dataloader = Get_train_DataLoader(Dataset,batch_size=args.class_in_batch)
            val_Dataloader = Get_val_DataLoader(Dataset,batch_size=args.class_in_batch)
    else:
        Dataset = VReID_Dataset(args.info,crop=args.crop,flip=args.flip,jitter=args.jitter,
                                pretrained_model=args.pretrain,dataset=args.dataset)
        train_Dataloader = Get_train_DataLoader(Dataset,batch_size=args.batch_size)
        val_Dataloader = Get_val_DataLoader(Dataset,batch_size=args.batch_size)

    ## Get Model
    if args.triplet:
        if args.unsupervised:
            net = models.FeatureResNet(Dataset.n_id,n_layers=args.n_layer,pretrained=args.pretrain)
        else:
            #net = models.ResNet(Dataset.n_id,n_layers=args.n_layer,pretrained=args.pretrain)
            if args.model == 'mobilenet':
                net = MobileNet(num_classes=Dataset.n_id, dims = 128, with_top = args.with_class)
            elif args.model == 'densenet':
                net = DenseNet(num_classes=Dataset.n_id, dims = 128, with_top = args.with_class)
            elif args.model == 'embednet':
                net = EmbedNetwork(num_classes=Dataset.n_id, dims = 128, with_top = args.with_class, bn_last=False)
            elif args.model == 'resnet':
                net = models.FeatRes51Net(num_classes=579, emb_dims=128, n_layers=args.n_layer, with_top=args.with_class, bn_last=False, pretrained=args.pretrain)
    else:
        if args.dataset != 'VeRi_ict':
            net = models.ResNet(Dataset.n_id,n_layers=args.n_layer,pretrained=args.pretrain)
        else:
            net = models.ICT_ResNet(Dataset.n_id,Dataset.n_color,Dataset.n_type,n_layers=args.n_layer,
                                    pretrained=args.pretrain)
    
    with torch.cuda.device(0):
        net = net.cuda()

    if (device.type == 'cuda') and (ngpu > 1):
        net = nn.DataParallel(net, list(range(ngpu)))

    if args.save_model_dir !=  None:
        os.system('mkdir -p %s' % os.path.join(args.save_model_dir))
    
    if args.load_ckpt is not None:
        state_dict = torch.load(args.load_ckpt)
        if 'epoch' in state_dict:
            state_dict = state_dict['model_state_dict']
            starting_epoch = state_dict['epoch']
            resume_lr = state_dict['base_lr']
            resume_steps = state_dict['trainging_steps']
        else:
            m = re.match(r"model_.*_(?P<epoch>\d+)\.ckpt", args.load_ckpt)
            starting_epoch = int(m.groupdict()['epoch'])
            resume_lr = args.lr
            resume_steps = args.decay_start_iteration
        #for key in list(state_dict.keys()):
        #    if key in set(list(['fc','embed','fc_head'])):
        #        del state_dict[key]
        net.load_state_dict(state_dict)
    else:
        starting_epoch = 0
        resume_lr = args.lr
        resume_steps = 0
        
    if args.save_model_dir !=  None:
        os.system('mkdir -p %s' % os.path.join(args.save_model_dir))
        
    ## train
    if True: #args.load_ckpt == None:
        print('total data:',len(Dataset))
        #print('training data:',Dataset.n_train)
        #print('validation data:',Dataset.n_val)
        if args.triplet:
            if args.unsupervised:
                train_unsupervised_triplet(args, Dataset, train_Dataloader, net)
            else:
                #updated on 04/05
                train_triplet(args, starting_epoch, resume_lr, resume_steps, Dataset, train_Dataloader, val_Dataloader, net)
        else:
            if args.dataset != 'VeRi_ict':
                train(args,Dataset,train_Dataloader,val_Dataloader,net)
            else:
                train_ict(args,Dataset,train_Dataloader,val_Dataloader,net)
