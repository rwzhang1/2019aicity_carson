from xml.dom import minidom
import os
import argparse



if __name__ == '__main__':
  # Argparse
    parser = argparse.ArgumentParser(description='Database generator for UA-DETRAC dataset')
    parser.add_argument('--img_dir', help='the dir containing dataset imgs')
    parser.add_argument('--label_dir', help='the dir containing dataset label files (TXT format)')
    parser.add_argument('--train_txt', help='the output txt file listing all imgs to database and its label')
    args = parser.parse_args()
    img_dir = args.img_dir
    txt_file = open(args.train_txt,'w')
    
    img_list = []
    V_ID_list = []
    V_ID_dict = {}
    count = 1
    xmlfile = open(args.label_dir,'r')
    for i,row in enumerate(xmlfile):
        line = row.strip().split(' ')
        img_name = line[0] + '.jpg'
        img_list.append(os.path.join(img_dir,img_name))
        V_ID = line[1]
        if V_ID not in V_ID_dict:
            V_ID_dict[V_ID]=count
            count +=1 
        V_ID_list.append(V_ID)

    txt_file.write('img_path id\n')
    for i in range(len(img_list)):
        img_path = img_list[i]
        V_ID = str(V_ID_dict[V_ID_list[i]])
        txt_file.write(img_path+' '+V_ID+'\n')
    txt_file.close()
    xmlfile.close()