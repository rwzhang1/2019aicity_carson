#VeRi_dir=$1
#AIC19_dir=$2
#COMPCARS_DIR=$3
#BOXCARS_DIR=$4
#VID_dir = $5
VeRi_dir=../DATASETS/VeRi
VID_dir=../DATASETS/VehicleID
AIC19_dir=../aic19-track2-reid/
COMPCARS_DIR=../DATASETS/compcars
BOXCARS_DIR=../DATASETS/BoxCars116k
DATABASE_DIR=database
#mkdir -p $DATABASE_DIR
#python create_VeRi_database.py --img_dir $VeRi_dir/image_train --query_dir $VeRi_dir/image_query \
#                                --gallery_dir $VeRi_dir/image_test --label_dir $VeRi_dir/train_label.xml \
#                                --train_txt $DATABASE_DIR/VeRi_train_info.txt \
#                                --query_txt $DATABASE_DIR/VeRi_query_info.txt \
#                                --gallery_txt $DATABASE_DIR/VeRi_gallery_info.txt
#python create_Comp_database.py --comp_dataset_dir $COMPCARS_DIR/data --sv_dataset_dir $COMPCARS_DIR/sv_data \
#                                --comp_image_dir $COMPCARS_DIR/data --sv_image_dir $COMPCARS_DIR/sv_data \
#                                --info_dir $DATABASE_DIR/Comp_info
#python create_AIC_database.py $AIC_dir/post_tracking/fasta  --output_pkl $DATABASE_DIR/AIC2018.pkl
#python create_BoxCars_database.py --dataset_dir $BOXCARS_DIR --image_dir $BOXCARS_DIR --output_dir $DATABASE_DIR
python create_AIC19_database.py --img_dir $AIC19_dir/image_train --query_dir $AIC19_dir/image_query \
                                --gallery_dir $AIC19_dir/image_test --label_dir $AIC19_dir/train_label.xml \
                                 --train_txt $DATABASE_DIR/AIC19_train_info.txt \
                                 --query_txt $DATABASE_DIR/AIC19_query_info.txt \
                                 --gallery_txt $DATABASE_DIR/AIC19_gallery_info.txt
python create_VehicleID_database.py --img_dir $VID_dir/image_train \
                                    --label_dir $VID_dir/attribute/img2vid.txt \
                                    --train_txt $VID_dir/train_test_split/train_list.txt