#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 12:57:47 2019

@author: rwzhang
"""
import torch
import numpy as np
from utils.utils import pdist_torch as pdist


class BatchHardTripletSelector(object):
    '''
    a selector to generate hard batch embeddings from the embedded batch
    @input: triplet id
    @output: returns the triplet pairs

    '''
    def __init__(self, *args, **kwargs):
        super(BatchHardTripletSelector, self).__init__()

    def __call__(self, embeds, labels):
        dist_mtx = pdist(embeds, embeds).detach().cpu().numpy()
        labels = labels.contiguous().cpu().numpy().reshape((-1, 1))
        num = labels.shape[0]
        dia_inds = np.diag_indices(num)
        lb_eqs = labels == labels.T
        lb_eqs[dia_inds] = False
        dist_same = dist_mtx.copy()
        dist_same[lb_eqs == False] = -np.inf
        pos_idxs = np.argmax(dist_same, axis = 1)
        #multinomial: np.random.multinomial
        dist_diff = dist_mtx.copy()
        lb_eqs[dia_inds] = True
        dist_diff[lb_eqs == True] = np.inf
        neg_idxs = np.argmin(dist_diff, axis = 1)
        pos = embeds[pos_idxs].contiguous().view(num, -1)
        neg = embeds[neg_idxs].contiguous().view(num, -1)
        return embeds, pos, neg
    
class ClusterHardTripletSelector(object):
    '''
    a selector to generate hard batch embeddings from the embedded batch
    @input: triplet id
    @output: returns the triplet pairs
    @anchor: the average of all embeddings of a particular 

    '''
    def __init__(self, *args, **kwargs):
        super(ClusterHardTripletSelector, self).__init__()

    def __call__(self, embeds, labels, clusters):
        dist_mtx = pdist(embeds, embeds).detach().cpu().numpy()
        labels = labels.contiguous().cpu().numpy().reshape((-1, 1))
        clusters = clusters.contiguous().cpu().numpy().reshape((-1,1))#for each label we select 2 camIDs
        num = labels.shape[0]
        dia_inds = np.diag_indices(num)
        lb_eqs = labels == labels.T
        cls_eqs= clusters == clusters.T
        
        hard_eqs = lb_eqs & ~cls_eqs
        hard_neg = ~lb_eqs & cls_eqs
        hard_eqs[dia_inds] = False
        dist_same = dist_mtx.copy()
        dist_same[hard_eqs == False] = -np.inf
        pos_idxs = np.argmax(dist_same, axis = 1)
        dist_diff = dist_mtx.copy()
        dist_diff[hard_neg == False] = np.inf
        neg_idxs = np.argmin(dist_diff, axis = 1)
        pos = embeds[pos_idxs].contiguous().view(num, -1)
        neg = embeds[neg_idxs].contiguous().view(num, -1)        
        return embeds, pos, neg



if __name__ == '__main__':
    embeds = torch.randn(16, 128)
#    labels = torch.randint(5, (10,))
#    clusters = torch.tensor([0, 4, 0, 3, 1, 1, 4, 1, 1, 3])
    labels = torch.tensor([292, 292, 292, 292, 292, 292, 292, 292, 201, 201, 201, 201, 201, 201, 201, 201]) 
    clusters = torch.tensor([18, 18, 18, 18, 22, 22, 22, 22, 18, 18, 18, 18, 22, 22, 22, 22])
    selector = BatchHardTripletSelector()
    anchor, pos, neg = selector(embeds, labels)
    print('anchor', anchor.data())
    # selector = ClusterHardTripletSelector()
    # anchor, pos, neg = selector(embeds, labels, clusters)