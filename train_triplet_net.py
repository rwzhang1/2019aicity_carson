#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 17:21:34 2019

@author: rwzhang
"""

import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import sys,os,logging,time,itertools
import pandas as pd
from sklearn.utils import shuffle

from resnet50_fc import EmbedNetwork
from othernet_fc import embvgg_bn,EMBVGG
from EmbedVGGM import vggm,VGGM
from utils.loss import TripletLoss
from gen_triplet import BatchHardTripletSelector
from batch_sampler import BatchSampler
from utils.optimizer import AdamOptimWrapper
from utils.logger import logger
from preprocess_data import AICData, convert_xml

import argparse, pickle,re
# construct the argument parser and parse the arguments
#Usage:  python train_triplet_net.py --model resnet50 --checkpoint_frequency 1000 --resume log/resnet50/ep25000_model_soft_trip.pkl --ngpu 3
ap = argparse.ArgumentParser(description='Train a verification model')
ap.add_argument("-lb", "--label_bin", default = "smallvggnet_lb.pickle", help="path to output label binarizer")
ap.add_argument('-mp', '--multi_proc', default=4, help= 'the number of workers to deploy for multiprocessing options')
ap.add_argument('-d', '--img_dir', help='img_dir', default='aic19-track2-reid/image_train/', type=str)
ap.add_argument('-l', '--img_list', help='input xml file', default='aic19-track2-reid/train_label.xml', type=str)
ap.add_argument('-m', '--model', help='model', default='resnet50', type=str)
ap.add_argument('-sp','--save_path', help='save_path', default='log/', type=str)
ap.add_argument('-inp', '--input_size', help = 'the input size to resize the original image to for training', type = int, default=224)
ap.add_argument('-lr', '--learning_rate', default=3e-4, type=float,help='The initial value of the learning-rate, before it kicks in.')
ap.add_argument('-it', '--train_iterations', default=50000, type=int,help='Number of training iterations.')
ap.add_argument('-dit', '--decay_start_iteration', default=15000, type=int, help='At which iteration the learning-rate decay should kick-in. Set to -1 to disable decay completely.')
ap.add_argument('-ckf', '--checkpoint_frequency', default=5000, type=int, help='Set this to 0 to disable intermediate storing.')
ap.add_argument('-bsp', '--batch_p', default=18, type=int, help='The number P used in the PK-batches')
ap.add_argument('-bsk', '--batch_k', default=16, type=int, help='The numberK used in the PK-batches')
ap.add_argument('-cls', '--num_class', default=333, type=int, help='The num_classes for classification')
ap.add_argument('-ckp','--resume', default='', type=str, help='The resume point for continuous training')
#ap.add_argument('-tr', '--train_val', default = 0.1, type= float, help='train/validataion split ratio')
ap.add_argument('--ngpu',default = 1, type=int, help='number of gpus to use')
args = ap.parse_args()
# =============================================================================
ngpu = args.ngpu
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu") 
# =============================================================================
root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
save_path = root_dir + args.save_path + args.model + '/'
#dataset = args.dataset
#img_dir = args.img_dir
#img_list = args.img_list
model = args.model
label_bin = root_dir + args.label_bin
train_xml = root_dir + args.img_list
img_dir = root_dir + args.img_dir
train_csv = root_dir + "aic19-track2-reid/train_label.csv"
train_feat = root_dir + "aic19-track2-reid/train_feat.csv"
train_feat_pkl = root_dir + "aic19-track2-reid/train_feat.pkl"
train_track = root_dir + "aic19-track2-reid/train_track.txt"
# =============================================================================
# =============================================================================
# construct the image generator for data augmentation
lb = pickle.loads(open(label_bin, "rb").read())
assert args.num_class == len(lb.classes_)
cam2img_df = convert_xml(train_xml)
#cam2img_df['zip_target'] = cam2img_df.vehicleID + cam2img_df.cameraID
cam2img_df = shuffle(cam2img_df, random_state = 10086)
#trainimg_lst = cam2img_df['imageName'].values[:num_train]
#valimg_lst = cam2img_df['imageName'].values[num_train:]
#trainvid_lst = cam2img_df['vehicleID'].values[:num_train]
#valvid_lst = cam2img_df['vehicleID'].values[num_train:]
#train_df = pd.DataFrame({'imageName': trainimg_lst, 'vehicleID': trainvid_lst}, dtype=str)
#val_df = pd.DataFrame({'imageName': valimg_lst, 'vehicleID': valvid_lst}, dtype=str)
# =============================================================================

# =============================================================================
def train():
    #setup
    torch.multiprocessing.set_sharing_strategy('file_system')
    if not os.path.exists(save_path): os.makedirs(save_path)
    
    #model and loss
    logger.info('setting up backnone models and loss')
    logger.info('Build Model from scratch')
    
    if model == 'resnet50':
        logger.info('Training with ResNet50')
#        input_size = 256 #if 256, need add additional pool layer 
        input_size = 224
        net = EmbedNetwork(num_class = args.num_class)
    elif model == 'vggm':
        logger.info('Training with VGGM model')
        input_size = 221
        net = vggm(num_classes = args.num_class, pretrained ='imagenet')
    else:
        logger.info('Training with {} model'.format(args.model))
        input_size = 224
        net = embvgg_bn(model_name = args.model, pretrained=False, num_classes = args.num_class)
    cur_epoch = 0
    if args.resume == 'from_veri':
        veri_res50 = 'Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl'
        net = EmbedNetwork(num_class = 13164) #loading VeRI data
        net.load_state_dict(torch.load(veri_res50), strict = False)
        logger.info('fine-turn from {}'.format(veri_res50))
        num_inft = net.fc.in_features
        net.fc = nn.Linear(in_features = num_inft, out_features = args.num_class)
    elif args.resume != '':
        m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.resume)
        cur_epoch = int(m.groupdict()['epoch'])
        print(cur_epoch)
        net.load_state_dict(torch.load(args.resume['model_state_dict']))
#        check_point = torch.load(args.resume)
#        net.load_state_dict(check_point['model_state_dict'], strict = False)
#        cur_epoch = check_point['epoch']
        logger.info('fine-turn from checkpoint: {}, epoch: {}'.format(args.resume, cur_epoch))
    with torch.cuda.device(0):
        net = net.cuda()
    #net = nn.DataParallel(net)
    if (device.type == 'cuda') and (ngpu > 1): net = nn.DataParallel(net, list(range(ngpu)))

    # define Triplet loss
    triplet_loss = TripletLoss(margin=None).cuda()
    #all get the softmax categorical cross entropy loss
    softmax_criterion = torch.nn.CrossEntropyLoss()
    
    #optimizer
    logger.info('create optimizer')
    optim = AdamOptimWrapper(net.parameters(), lr=args.learning_rate, wd=0, t0=args.decay_start_iteration, t1 = args.train_iterations)    
    #dataloader
    selector = BatchHardTripletSelector()
    ds = AICData(img_dir, cam2img_df, img_size = input_size, is_train = True)
    logger.info('Dataloading Done')
    sampler = BatchSampler(ds, args.batch_p, args.batch_k) #batch sample the dataset
    print(sampler)
    dl = DataLoader(ds, batch_sampler = sampler, num_workers = args.multi_proc) # need to update the worker 
#    if args.model == 'vggm':
#        dl = DataLoader(ds, batch_sampler = sampler)
    diter = iter(dl) #mode like yield

    #train
    logger.info('start training...')
    loss_avg = []
    loss_soft_avg = []
    count = cur_epoch
    t_start = time.time()

    while True:
        print('INFO] initial train to reach a decent loss')
        try:
            imgs, lbs, cams, _ = next(diter)
        except StopIteration:
            diter = iter(dl)
            imgs, lbs, cams, _ = next(diter)
        
        net.train()
        #imgs = imgs.cuda()
        #lbs = lbs.cuda()
        with torch.cuda.device(0):
            imgs = imgs.cuda(non_blocking=True)
            lbs = lbs.cuda(non_blocking=True)
            cams = cams.cuda(non_blocking=True)
        #aply inference to generate embeddings from fc
        embds, cls_lbs, cls_cams = net(imgs)
        #generate the triplet
        anchor, positives, negatives = selector(embds, lbs)
        if anchor.is_cuda:
            print('at main train file, anchor is on cuda')        
        #Mean_valued triplet loss: 0.5 * crossentropyloss + triplet_loss
        loss_triplet = triplet_loss(anchor, positives, negatives)
        loss_softmax = softmax_criterion(cls_lbs, lbs)
        loss_cid = softmax_criterion(cls_cams, cams)
        loss_all = 0.5*loss_softmax + 0.5* loss_cid + loss_triplet.mean()
        
        optim.zero_grad()
        loss_all.backward()
#        loss_triplet.backward()
        optim.step()
        
        #detach the current tensor from current graph. The result will never require gradient.
        loss_avg.append(loss_triplet.detach().cpu().numpy())#calculate on the cpu
        loss_soft_avg.append(loss_softmax.detach().cpu().numpy())
        if count % args.batch_p == 0 and count != 0:
            loss_avg = sum(loss_avg) / len(loss_avg)
            t_end = time.time()
            time_interval = t_end - t_start
            logger.info('iter: {}, loss: {:4f}, lr: {:4f}, time: {:3f})'.format(count, loss_avg, optim.lr, time_interval))
            loss_avg = [] # reinitialize loss_avg
            loss_soft_avg = []
            t_start = max(time.time(), t_end) # check to see if we have a pause
        count += 1
        
        if count % args.checkpoint_frequency == 0 and count != 0:
            logger.info('Saving the intermediate training status')
            save_model = save_path + 'ep{}_model_{}.pkl'.format(count, args.model)
            print(save_model)
            ver = 2 #verbose = 2
            while os.path.exists(save_model):
                logger.info('Model Exists, Version Update!!!')
                save_model = save_model + '_v' + str(ver)
                ver = ver + 1
            torch.save({'epoch': count, 'model_state_dict': net.state_dict()}, save_model)
            
        if count == args.train_iterations: break
        
    ## dump 
    logger.info('saving trained model')
    save_model = save_path + 'ep{}_model_{}.pkl'.format(count, args.model)
    ver = 2
    while os.path.exists(save_model):
        logger.info('Model Exists, Version Update!!!')
        save_model = save_model + '_v' + str(ver)
        ver = ver + 1
    torch.save({'epoch': count,'model_state_dict': net.state_dict()}, save_model)
    logger.info('Finished at {}'.format(time.strftime("%D_%H:%M:%S")))

if __name__ == '__main__':
    train()
    
