#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 16:38:25 2019

@author: rwzhang
"""
Main Contribution:
1. Exploit Using VGGM and ResNet50 to train AIC19 dataset.
2. Hard triplet generation and Hardest Triplet Generation
	1) Hard: Sampling from the AIC19 dataset to build the hard triplet as, pos/anchor share the same id, neg is of different id
	2) Hardest: Sampleing from the AIC19 dataset to, pos share the same vid/cid with anchor and neg is of different vid/cid
We exploit this nature in traffic videos to generate triplets, along with samples from existing vehicle datasets([VeRi](https://github.com/VehicleReId/VeRidataset), [CompCars](http://mmlab.ie.cuhk.edu.hk/datasets/comp_cars/index.html), [BoxCars116k](https://medusa.fit.vutbr.cz/traffic/research-topics/fine-grained-vehicle-recognition/boxcars-improving-vehicle-fine-grained-recognition-using-3d-bounding-boxes-in-traffic-surveillance/)), to train our CNN in a multi-task learning manner.

Please follow the steps below for training CNN:
1. Install requirements in root system's
2. In this step, we prepare data for [2018 NVIDIA AI City Challenge](https://www.aicitychallenge.org/). Follow detail guide section in root system's [Readme.md](https://github.com/cw1204772/AIC2018_iamai#detail-guide) until finishing stage III, Post-Tracking.
3. Download and extract [VeRi](https://github.com/VehicleReId/VeRidataset), [CompCars](http://mmlab.ie.cuhk.edu.hk/datasets/comp_cars/index.html), [BoxCars116k](https://medusa.fit.vutbr.cz/traffic/research-topics/fine-grained-vehicle-recognition/boxcars-improving-vehicle-fine-grained-recognition-using-3d-bounding-boxes-in-traffic-surveillance/) dataset.  
4. Run the following script to setup training:
	
	```
	cd ReID_CNN
	bash setup.sh <VeRi_DIR> <WORK_DIR> <CompCars_DIR> <BoxCars116k_DIR>
	```
5. Trainonly AIC19 dataset for feature extractor
	'''
	python train_triplet_net.py
	'''
		Options provided:
			model selection: resnet(layer number can be selected with n_layers), vggm, vgg16, vgg16_bn, vgg19, vgg19_bn
			learning rate decay: can select the decay starting point
		Inference:
	'''
	python embed_triplet_net.py
	'''
		Generates embeddings for test dataset
	'''
	python embed_query.py
	'''
		Generates the embeddings for query dataset

6. Train with joint dataset
	1) VeRi + AIC19
		'''
		cd ReID_CNN
		python partial_joint.py
		'''
	2) VehicleID + AIC19
		'''
		python train_joint_aic_veri_v2.py
		'''

The model will be in `./ckpt`

##Clustering:
	1. written in k_means.py as YOLO_K_means
		Idea one is for each embedding, calculate the iou of that embedding with any cluster <--  requires me to determine the cluster area first.

	2. gen_cluster.py <-- this requires me to determine how many clusters ahead of time and not seem to be successful
		The problem with k-means and hierarchical agglomerative clustering is it requires us to specify the number of clusters we seek ahead of time

	In fact we have no idea how many unique identifications we have
	Therefore, we need to use a density-based or graph-based clustering algorithm that can not only cluster the data points but can also determine the number of clusters as well based on the density of the data.
	For face clustering I would recommend two algorithms:
	1. Density-based spatial clustering of applications with noise (DBSCAN) Chinese whispers clustering
	2. The DBSCAN algorithm works by grouping points together that are closely packed in an N-dimensional space. Points that lie close together will be grouped together in a single cluster.
	DBSCAN also naturally handles outliers, marking them as such if they fall in low-density regions where their “nearest neighbors” are far away.

## Train on VeRi Dataset

* Train classification model with VeRi or VeRi\_ict dataset
```
python3 train.py --info VeRi_train_info.txt --save_model_dir ./ckpt --lr 0.001 --batch_size 64 --n_epochs 20 --n_layer 18 --dataset VeRi
```

* Train triplet model with VeRi dataset
```
python3 train.py --info VeRi_train_info.txt --n_epochs 1500 --save_model_dir ./ckpt --n_layer 18 --margin soft --class_in_batch 32 --triplet --lr 0.001 --batch_hard --save_every_n_epoch 50
```

## Evaluate CNN on VeRi

1. dump distance matrix
```
python3 compute_VeRi_dis.py --load_ckpt <base_model_path> --n_layer <Resnet_layer> --gallery_txt VeRi_gallery_info.txt --query_txt VeRi_query_info.txt --dis_mat dist_CNN.mat
```

2. compute cmc curve
```  
  1. open matlab in the "VeRi_cmc/" directory
  2. open "baseline_evaluation_FACT_776.m" file
  3. change "dis_CNN" mat path, "gt_index",  "jk_index" txt file path
  4. run and get plot
```