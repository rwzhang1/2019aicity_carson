#!/usr/bin/python
# -*- encoding: utf-8 -*-
"""
@author: Huawei
"""

import torch
import torch.nn as nn
import torchvision
from torch.autograd import Variable
from torch.utils.data import DataLoader
import sys,os,logging,time,itertools
import pandas as pd
from sklearn.utils import shuffle

from resnet50_fc import EmbedNetwork
from othernet_fc import embvgg_bn,EMBVGG
from EmbedVGGM import vggm,VGGM
from preprocess_data import AICTestData, convert_xml,AICQueryData
import numpy as np
import argparse, pickle, re

torch.multiprocessing.set_sharing_strategy('file_system')
#Help: python embed_triplet_net.py ---mode 'test' -img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet50' --num_class 333 --img_size 256 --resume 'from_veri'
#Help: python embed_triplet_net.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'resnet50' --num_class 333 --img_size 256 --resume log/resnet50/ep25000_model_soft_resnet.pkl
#Help: python embed_triplet_net.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'vggm' --num_class 333 --img_size 221 --resume log/vggm/ep25000_model_vggm.pkl
#Help: python embed_triplet_net.py --mode 'test' --img_dir aic19-track2-reid/image_test/ --img_list  'aic19-track2-reid/test_track_id.txt' --model 'vgg16_bn' --num_class 333 --img_size 224 --resume log/vgg16_bn/ep25000_model_vgg16_bn.pkl
#Query: python embed_triplet_net.py --mode 'query' --img_dir aic19-track2-reid/image_query/ --model 'resnet50' --num_class 333 --img_size 224 --resume 'from_veri'

def parse_args():
    ap = argparse.ArgumentParser('Running EMbeddings on the Test/Query set')
    ap.add_argument('--mode', help='test or query', default ='test', required=True, type=str)
    ap.add_argument('--img_dir', help='img_dir', default='aic19-track2-reid/image_test/', type=str)
    ap.add_argument('--img_list', help=' the raw images list for test', default='aic19-track2-reid/test_track_id.txt', type=str)
    ap.add_argument('--model', help='model', default='resnet50', type=str)
    ap.add_argument('--save_path', help='path to save the embeddings', default='emb/', type=str)
    ap.add_argument('--resume', default='Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl', type=str, help='The trained model to be loaded for eval')
    ap.add_argument('--num_class', default=333, type=int, help='The numberK used in the PK-batches')
    ap.add_argument('--img_size', default=256, type=int, help='The imageSize used')
    ap.add_argument('--ngpu', default=1, type=int, help='number of GPUs to be engaged')
    return ap.parse_args()

def embed(args):
    #path init, load arguments
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    emb_path = root_dir + args.save_path + 'emb' + args.mode + '_' + args.model + '/'
    img_list = root_dir + args.img_list
    img_dir = root_dir + args.img_dir
    emb_name = 'emb_'+ args.model +'.pkl'
    ngpu = args.ngpu
    input_size = args.img_size
    device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

    ## logging
    if not os.path.exists(emb_path): os.makedirs(emb_path)
    FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
    logger = logging.getLogger(__name__)

    ## restore model
    logger.info('restoring model')

    if args.model == 'resnet50':
        logger.info('Training with ResNet50')
#        input_size = args.img_size
        input_size = 256
        net = EmbedNetwork(num_class = args.num_class)
    elif args.model == 'vggm':
        logger.info('Training with VGGM model')
        input_size = 221
        net = vggm(num_classes = args.num_class, pretrained ='imagenet')
    else:
        logger.info('Training with {} model'.format(args.model))
        input_size = 224
        net = embvgg_bn(model_name = args.model, pretrained=False, num_classes = args.num_class)

    if args.resume == 'from_veri':
        veri_res50 = 'Embedding-Network/res/soft_trip_res50_VehicleID/50000_model_trip_soft_res50_v2.pkl'
        emb_name = 'emb_50000model_trip_soft_res50.pkl'
        net = EmbedNetwork(num_class = 13164) #loading VeRI data
        net.load_state_dict(torch.load(veri_res50), strict = False)
        logger.info('fine-tune from {}'.format(veri_res50))
        num_inft = net.fc.in_features
        net.fc = nn.Linear(in_features = num_inft, out_features = args.num_class)

    elif args.resume != '':
        m = re.match(r".*ep(?P<epoch>\d+)\_model.*", args.resume)
        cur_epoch = int(m.groupdict()['epoch'])
        print(cur_epoch)
        emb_name = 'emb_{}_{}.pkl'.format(cur_epoch, args.model)
        net.load_state_dict(torch.load(args.resume), strict = False)
    with torch.cuda.device(0):
        net = net.cuda()
    #net = nn.DataParallel(net)
    if (device.type == 'cuda') and (ngpu > 1): net = nn.DataParallel(net, list(range(ngpu)))
    net.eval()

    ## load gallery dataset
    batchsize = 24
    if args.mode == 'test':
        logger.info('Loading test dataset from: {}'.format(img_dir))
        #    ds = AICTestData(img_dir, img_list, img_size=args.img_size, is_train = False)
        ds = AICTestData(img_dir, img_list, img_size=input_size, is_train = False)
#    elif args.mode == 'query':
    else:
        logger.info('Loading query dataset from: {}'.format(img_dir))
        ds = AICQueryData(img_dir, img_size=input_size, is_train = False)

    dl = DataLoader(ds, batch_size = batchsize, drop_last = False, num_workers = 4)

    ## embedding samples
    logger.info('start embedding')
    all_iter_nums = len(ds) // batchsize + 1
    embeddings = []
    label_ids = []
    label_cams = []
    img_names = []
    for it, sample in enumerate(dl):
        print('\r=======>  processing iter {} / {}'.format(it, all_iter_nums),
                end = '', flush = True)
        if args.mode == 'test':
            img, lb_id, lb_cam, img_name = sample
            label_ids.append(lb_id)
            label_cams.append(lb_cam)
            img_names.append(img_name)
        else:
            img, img_name = sample
            img_names.append(img_name)
        embds = []
        for im in img:
            with torch.cuda.device(0):
                im = im.cuda(non_blocking=True)
            embd, _ = net(im)
            embd = embd.detach().cpu().numpy()
            embds.append(embd)
        embed = sum(embds) / len(embds)
        embeddings.append(embed)
    print('  ...   completed')

    embeddings = np.vstack(embeddings)
    img_names = np.hstack(img_names)

    ## dump results
    logger.info('dump embeddings')
    if args.mode == 'test':
        label_ids = np.hstack(label_ids)
        label_cams = np.hstack(label_cams)
        embd_res = {'embeddings': embeddings, 'label_ids': label_ids, 'label_cams': label_cams, 'img_names':img_names}
    else:
        embd_res = {'embeddings': embeddings, 'img_names':img_names}
    emb_file = emb_path + '/' + emb_name
    with open(emb_file, 'wb') as fw:
        pickle.dump(embd_res, fw)
    logger.info('embedding finished')

if __name__ == '__main__':
    args = parse_args()
    embed(args)