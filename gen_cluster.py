#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 19:19:41 2019

@author: rwzhang
"""

"""
Different Means of clustering
@input: Test/Query embeddings
@run: python gen_cluster.py
@output: clustered image 
"""

import cv2
import numpy as np
import progressbar
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import DBSCAN
from imutils import build_montages
import pickle,sys,os,logging,time,argparse

ap = argparse.ArgumentParser()
ap.add_argument("-j", "--jobs", type=int, default=-1, help="# of parallel jobs to run (-1 will use all CPUs)")
ap.add_argument("-ncls", "--n_clusters", type=int, default = 8, help="# of clusters to be adopted for kmeans")
ap.add_argument("-bs", "--batch_size", type=int, default = 64, help="The batch size to run for each iteration")
args = ap.parse_args()
CLUSTERS = {'kmeans': MiniBatchKMeans(n_clusters=args.n_clusters, batch_size=100, verbose=1),
            'dbscan': DBSCAN(metric="euclidean",algorithm='kd_tree', n_jobs=args.jobs, leaf_size =60)}

def build_siftbowdict(xfeatures2d, dir_names, file_paths, dictionary_size):
  print('Computing descriptors from siftbow..')        
  desc_list = []
  num_files = len(file_paths)
  bar = progressbar.ProgressBar(maxval=num_files).start()
  for i in range(num_files):
    p = file_paths[i]
    image = cv2.imread(p)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    kp, dsc = xfeatures2d.detectAndCompute(gray, None)
    desc_list.extend(dsc)
    bar.update(i)
  bar.finish()
  print('Creating BoW dictionary using K-Means clustering with k={}..'.format(dictionary_size))
  dictionary = MiniBatchKMeans(n_clusters=dictionary_size, batch_size=100, verbose=1)
  dictionary.fit(np.array(desc_list))
  return dictionary

def emb_dsc(embeddings, dictionary_size = 8):
    with open(embeddings, 'rb') as fr:
        embeddings_dict = pickle.load(fr)
    embs, lb_ids, lb_cams, img_names = embeddings_dict['embeddings'], embeddings_dict['label_ids'], embeddings_dict['label_cams'], embeddings_dict['img_names']
    num_files =len(lb_ids)
    bar = progressbar.ProgressBar(maxval=num_files).start()
    embs_dict = {}
    dsc = []
    for index in range(num_files):
        ids = lb_ids[index]
        emb = embs[index]
        name = img_names[index].split('/')[-1].split('.')[0]
        embs_dict[name] = emb
        bar.update(index)
        dsc.extend(emb)
    return embs, lb_ids, img_names, dsc

def kmeans_clustering(emb_path, emb_name, image_dir, clustering_method='kmeans'):
    FORMAT = '%(levelname)s %(filename)s:%(lineno)d: %(message)s'
    logging.basicConfig(level=logging.INFO, format=FORMAT, stream=sys.stdout)
    logger = logging.getLogger(__name__)
    ## restore model
    logger.info('restoring model')
    clt = CLUSTERS[clustering_method]
    embeddings = emb_path + emb_name
    with open(embeddings, 'rb') as fr:
        embeddings_dict = pickle.load(fr)
    embs, lb_ids, lb_cams, img_names = embeddings_dict['embeddings'], embeddings_dict['label_ids'], embeddings_dict['label_cams'], embeddings_dict['img_names']
    num_files =len(lb_ids)
    bar = progressbar.ProgressBar(maxval=num_files).start()
    clt.fit(np.array(embs))
#    FEAT_SIZE = embs.shape[1]
    labelIDs = np.unique(clt.labels_)
    numUniqueIds = len(labelIDs)
    embeddings_dict['cluster_id'] = labelIDs
    print("[INFO] # of unique vehicles: {}".format(numUniqueIds))
    logger.info('dump clustering info')
    emb_file = emb_path + '/' + clustering_method + emb_name
    with open(emb_file, 'wb') as fw:
        pickle.dump(embeddings_dict, fw)
    logger.info('clustering finished')
    output_imgdir = root_dir + 'aic19-track2-reid/' + clustering_method + emb_name[:-4]
    if not os.stat(output_imgdir):
        os.makedirs(output_imgdir)
    
    for index,labelID in enumerate(labelIDs):
        idxs = np.where(clt.labels_ == labelID)[0]
        idxs = np.random.choice(idxs, size=min(25, len(idxs)), replace = False)
        vehicles = []
        
        for i in idxs:
            img_path = image_dir + img_names[i]
            logger.info('Reading image: {}'.format(img_path))
            image = cv2.imread(img_path)
            image = cv2.resize(image, (224,224))
            vehicles.append(image)
        montage = build_montages(vehicles, (224, 224), (5, 5))[0]
        # show the output montage
        title = "Cluster ID #{}".format(labelID)
        title = "Unknown Faces" if labelID == -1 else title
        cv2.imshow(title, montage)
        cv2.waitKey(100)
        output_path = output_imgdir +'/'+ title + '.jpg'
        cv2.imwrite(output_path, montage)
        bar.update(index)
    return clt

# usage example
if __name__ =='__main__':
#    sift = cv2.xfeatures2d.SIFT_create()
    root_dir = os.getcwd() + '/' #/media/f/2019AICity_carson/Track2/
    emb_path = root_dir + '/joint_aic_vid/embtest_resnet50/'
    emb_name = 'tencrop_embtest_32_color39_embnet.pkl'
    img_dir = root_dir + 'aic19-track2-reid/image_test/'
    #img_list = args.img_list
    dictionary = kmeans_clustering(emb_path, emb_name, img_dir, clustering_method='kmeans')